/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file ABpBA.hh

*/
#ifndef _ABPBA_HH
#define _ABPBA_HH
#include "linear_algebra.h"

namespace linear_algebra
{
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN>& ABpBA(const mat_sym_full<TN>& A,
			   const mat_asym_full<TN>& B,
			   mat_asym_full<TN>& r)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows() || B.cols() != A.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
    if(r.cols()!=A.cols())
      throw domain_error("ABpBA: A and output r don't agree on dimensions");
    try {
#endif
      long i,j,k,l;
      
      //for(i=0;i<r.size();i++) r[i]=(TN) 0;
      
      // upper triangle  upper triangle
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(j=0;j<=i;j++)
	    r[k*(k-1)/2+j]+=A[i*(i+1)/2+j]*B[k*(k-1)/2+i];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(j=0;j<i;j++)
	    r[k*(k-1)/2+j]+=B[i*(i-1)/2+j]*A[k*(k+1)/2+i];
      
      // lt lt is not needed because we are symmetric.
      
      // ut lt
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l-1)/2+j]-=A[k*(k+1)/2+j]*B[k*(k-1)/2+l];
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l-1)/2+j]+=B[k*(k-1)/2+j]*A[k*(k+1)/2+l];
      
      // lt ut 
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k-1)/2+i]+=A[i*(i+1)/2+l]*B[k*(k-1)/2+l];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k-1)/2+i]-=B[i*(i-1)/2+l]*A[k*(k+1)/2+l];
      
#ifdef DEBUG
    }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::operator*(matrix& b) const");
      }
#endif
    return r;
  }
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN> ABpBA(const mat_sym_full<TN>& A,
			  const mat_asym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
    mat_asym_full<TN> r(A.cols());
    return ABpBA(A,B,r);
  }

  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN>& ABpBA(const mat_asym_full<TN>& A,
			   const mat_sym_full<TN>& B,
			   mat_asym_full<TN>& r)
  {
    ABpBA(B,A,r)*=(TN) (-1);
    return r;
  }
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN> ABpBA(const mat_asym_full<TN>& A,
			  const mat_sym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
    mat_asym_full<TN> r(A.cols());
    ABpBA(B,A,r)*=(TN) (-1);
    return r;
  }
  
  //! Computes the anti-commutator AB+BA.\todo Need optimization taking advantage of sparse structure.
  template<class TN>
  mat_asym_full<TN>& ABpBA(const mat_sym_full<TN>& A,
			   const mat_asym_sparse<TN>& B,
			   mat_asym_full<TN>& r)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows() || B.cols() != A.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
    if(r.cols()!=A.cols())
      throw domain_error("ABpBA: A and output r don't agree on dimensions");
    try {
#endif
      long i,j,k,l;
      
      //for(i=0;i<r.size();i++) r[i]=(TN) 0;
      
      // upper triangle  upper triangle
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(j=0;j<=i;j++)
	    r[k*(k-1)/2+j]+=A[i*(i+1)/2+j]*B(k,i);
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(j=0;j<i;j++)
	    r[k*(k-1)/2+j]+=B(i,j)*A[k*(k+1)/2+i];
            
      // lt lt is not needed because we are symmetric.
      
      // ut lt
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l-1)/2+j]-=A[k*(k+1)/2+j]*B(k,l);
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l-1)/2+j]+=B(k,j)*A[k*(k+1)/2+l];
      
      // lt ut 
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k-1)/2+i]+=A[i*(i+1)/2+l]*B(k,l);
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k-1)/2+i]-=B(i,l)*A[k*(k+1)/2+l];
      
#ifdef DEBUG
    }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::operator*(matrix& b) const");
      }
#endif
    return r;
  }
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN> ABpBA(const mat_sym_full<TN>& A,
			  const mat_asym_sparse<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
    mat_asym_full<TN> r(A.cols());
    return ABpBA(A,B,r);
  }

  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN>& ABpBA(const mat_asym_sparse<TN>& A,
			   const mat_sym_full<TN>& B,
			   mat_asym_full<TN>& r)
  {
    ABpBA(B,A,r)*=(TN) (-1);
    return r;
  }
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_asym_full<TN> ABpBA(const mat_asym_sparse<TN>& A,
			  const mat_sym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
    mat_asym_full<TN> r(A.cols());
    ABpBA(B,A,r)*=(TN) (-1);
    return r;
  }
  

//! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_sym_full<TN>& ABpBA(const mat_sym_full<TN>& A,
			  const mat_sym_full<TN>& B,
			  mat_sym_full<TN>& r,
			  mat_full<TN>& I2)
  {
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
  if(I2.cols() != B.cols() || I2.rows() !=A.rows())
    throw domain_error("ABpBA: I2 has dimensional problems");
#endif
  long i,j,ij;
  A.multiply(B,I2);
  for(i=0,ij=0;i<I2.cols();i++)
    for(j=0;j<=i;j++,ij++)
      r[ij]+=I2[i][j];
  for(i=0,ij=0;i<I2.cols();i++)
    for(j=0;j<=i;j++,ij++)
      r[ij]+=I2[j][i];
  return r;
}

//! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_sym_full<TN>& ABpBA(const mat_sym_full<TN>& A,
			  const mat_sym_full<TN>& B,
			  mat_sym_full<TN>& r)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
    if(r.cols()!=A.cols())
      throw domain_error("ABpBA: A and output r don't agree on dimensions");
#endif
    mat_full<TN> I2(B.cols(),A.rows());
    return ABpBA(A,B,r,I2);
  }
  
//! Computes the anti-commutator AB+BA.
template<class TN>
mat_sym_full<TN> ABpBA(const mat_sym_full<TN>& A,
                       const mat_sym_full<TN>& B)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
  mat_sym_full<TN> r(A.cols());
  mat_full<TN> I2(B.cols(),A.rows());
  return ABpBA(A,B,r,I2);
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_sym_full<TN>& ABpBA(const mat_sym_full<TN>& A,
			const mat_sym_sparse<TN>& B,
			mat_sym_full<TN>& r,
			mat_full<TN>& I2)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
  if(I2.cols() != B.cols() || I2.rows() !=A.rows())
    throw domain_error("ABpBA: I2 has dimensional problems");
#endif
  long i,j;
  I2=A.multiply(B,I2);
  for(i=0;i<I2.cols();i++)
    for(j=0;j<=i;j++)
	r[i*(i+1)/2+j]+=I2[i][j];

  for(i=0;i<I2.rows();i++)
    for(j=0;j<=i;j++)
      r[i*(i+1)/2+j]+=I2[j][i];
  return r;
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_sym_full<TN>& ABpBA(const mat_sym_full<TN>& A,
			const mat_sym_sparse<TN>& B,
			mat_sym_full<TN>& r)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
#endif
  mat_full<TN> I(r.cols(),r.rows());
  return ABpBA(A,B,r,I);
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_sym_full<TN> ABpBA(const mat_sym_full<TN>& A,
		       const mat_sym_sparse<TN>& B)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
  mat_sym_full<TN> r(A.cols());
  return ABpBA(A,B,r);
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_asym_full<TN>& ABpBA(const mat_asym_full<TN>& A,
			 const mat_sym_sparse<TN>& B,
			 mat_asym_full<TN>& r,
			 mat_full<TN>& I2)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
  if(I2.cols() != B.cols() || I2.rows() !=A.rows())
    throw domain_error("ABpBA: I2 has dimensional problems");
#endif
  long i,j;
  I2=A.multiply(B,I2);
  
  //  for(i=0;i<r.size();i++) r[i]=(TN) 0;
  for(i=0;i<I2.cols();i++)
    {
      for(j=0;j<i;j++)
	r[i*(i-1)/2+j]=I2[i][j];
    }
  for(i=0;i<I2.rows();i++)
    for(j=0;j<i;j++)
      r[i*(i-1)/2+j]-=I2[j][i];
  return r;
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_asym_sparse<TN>& ABpBA(const mat_asym_full<TN>& A,
			   const mat_sym_sparse<TN>& B,
			   mat_asym_sparse<TN>& r,
			   mat_sparse<TN>& I2)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
  if(I2.cols() != B.cols() || I2.rows() !=A.rows())
    throw domain_error("ABpBA: I2 has dimensional problems");
  try {
#endif
    long i,j,k,l;
    I2=A.multiply(B,I2);
    //    r.zero();
    
    for(i=0;i<I2.size();i++)
      {
	long id=I2.index(i);
	for(j=0;j<I2[i].size() && I2[i].index(j)<id;j++)
	  {
	    long jd=I2[i].index(j);
	    r.add(id,jd,I2[i][j]);
	  }
      }
    for(i=0;i<I2.size();i++)
      {
	long id=I2.index(i);
	for(j=0;j<I2[i].size() && I2[i].index(j)<id;j++);
	for(j++;j<I2[i].size();j++)
	  {
	    long jd=I2[i].index(j);
	    r.add(jd,id,-I2[i][j]);
	  }
      }
    r.prune();
    return r;
#ifdef DEBUG
  } catch(domain_error e) {
    cerr << e.what() << endl;
    throw domain_error("called from ABpBA(const mat_asym_full<TN>& A, const mat_sym_sparse<TN>& B, mat_asym_sparse<TN>& r, mat_sparse<TN>& I2)");
  }
#endif
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_asym_full<TN>& ABpBA(const mat_asym_full<TN>& A,
			 const mat_sym_sparse<TN>& B,
			 mat_asym_full<TN>& r,
			 mat_sparse<TN>& I2)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
  if(I2.cols() != B.cols() || I2.rows() !=A.rows())
    throw domain_error("ABpBA: I2 has dimensional problems");
#endif
  long i,j,k,l;
  I2=A.multiply(B,I2);
  
  //  for(i=0;i<r.size();i++) r[i]=(TN) 0;
  for(i=0;i<I2.size();i++)
    {
      long id=I2.index(i);
      for(j=0;j<I2[i].size() && I2[i].index(j)<=id;j++)
	{
	  long jd=I2[i].index(j);
	  r[id*(id+1)/2+jd]=I2[i][j];
	}
    }
  for(i=0;i<I2.size();i++)
    {
      long id=I2.index(i);
      for(j=0;j<I2[i].size() && I2[i].index(j)<id;j++);
      for(;j<I2[i].size();j++)
	{
	  long jd=I2[i].index(j);
	  r[jd*(jd+1)/2+id]-=I2[i][j];
	}
    }
  return r;
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_asym_full<TN>& ABpBA(const mat_asym_full<TN>& A,
			 const mat_sym_sparse<TN>& B,
			 mat_asym_full<TN>& r)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
#endif
  mat_full<TN> I(r.cols(),r.rows());
  return ABpBA(A,B,r,I);
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_asym_sparse<TN>& ABpBA(const mat_asym_full<TN>& A,
			   const mat_sym_sparse<TN>& B,
			   mat_asym_sparse<TN>& r)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
#endif
  mat_sparse<TN> I(r.cols(),r.rows());
  return ABpBA(A,B,r,I);
}

//! Computes the anti-commutator AB+BA.
template<class TN>
mat_asym_full<TN> ABpBA(const mat_asym_full<TN>& A,
			const mat_sym_sparse<TN>& B)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
  mat_asym_full<TN> r(A.cols());
  return ABpBA(A,B,r);
}


template<class T>
mat_asym_full<T> ABpBA(const mat_full<T>& A,
		       const mat_asym_full<T>& B,
		       mat_asym_full<T>& r,
		       refvector<T>& I)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
  if(r.cols()!=A.cols())
    throw domain_error("ABpBA: A and output r don't agree on dimensions");
  if(I.dim() != B.rows())
    throw domain_error("ABpBA: I has dimensional problems");
#endif
  long i,j,k;
  for(i=0;i<r.size();i++) r[i]=(const T) 0.0;
  for(i=0;i<A.cols();i++)
    {
      I=B.multiply(A[i],I);
      for(j=0;j<i;j++)
	r[i*(i-1)/2+j]-=I[j];
      for(j=i+1;j<A.cols();j++)
	r[j*(j-1)/2+i]+=I[j];
    }
  return r;
}

template<class T>
mat_asym_full<T> ABpBA(const mat_full<T>& A,
		       const mat_asym_full<T>& B)
{
#ifdef DEBUG
  if(A.cols()!=B.rows())
    throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
  mat_asym_full<T> r(A.cols());
  refvector<T> I(A.rows());
  return ABpBA(A,B,r,I);
}
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_sym_full<TN>& ABpBA(const mat_asym_full<TN>& A,
                          const mat_asym_full<TN>& B,
                          mat_sym_full<TN>& r)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows() || B.cols() != A.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
    if(r.cols()!=A.cols())
      throw domain_error("ABpBA: A and output r don't agree on dimensions");
    try {
#endif
      long i,j,k,l;
      
      //      for(i=0;i<r.size();i++) r[i]=(TN) 0;
      
      // upper triangle  upper triangle
      
      for(k=0;k<B.cols();k++) {
	TN* RP=&r[k*(k+1)/2];
	for(i=0;i<k;i++) {
	  const TN* AP=&A[i*(i-1)/2];
	  const double prefactor=B[k*(k-1)/2+i];
	  for(j=0;j<i;j++)
	    RP[j]+=AP[j]*prefactor;
	  //r[k*(k+1)/2+j]+=A[i*(i-1)/2+j]*B[k*(k-1)/2+i];
	}
      }
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(j=0;j<i;j++)
	    r[k*(k+1)/2+j]+=B[i*(i-1)/2+j]*A[k*(k-1)/2+i];
      
      // lt lt is not needed because we are anti-symmetric.
      
      // ut lt
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<=l;j++)
	    r[l*(l+1)/2+j]-=A[k*(k-1)/2+j]*B[k*(k-1)/2+l];
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<=l;j++)
	    r[l*(l+1)/2+j]-=B[k*(k-1)/2+j]*A[k*(k-1)/2+l];
      
      // lt ut 
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k+1)/2+i]-=A[i*(i-1)/2+l]*B[k*(k-1)/2+l];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k+1)/2+i]-=B[i*(i-1)/2+l]*A[k*(k-1)/2+l];
      
#ifdef DEBUG
    }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::operator*(matrix& b) const");
      }
#endif
    return r;
  }
  
  //! Computes the anti-commutator AB+BA.
  template<class TN>
  mat_sym_full<TN> ABpBA(const mat_asym_full<TN>& A,
                         const mat_asym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABpBA: A and B don't agree on dimensions");
#endif
    mat_sym_full<TN> r(A.cols());
    return ABpBA(A,B,r);
  }

}; // namespace

#endif
/*! @}*/

