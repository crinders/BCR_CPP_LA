/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{ */
/*! @file matrix.h
  @brief Describes fundamentals of matrices.

*/
#ifndef _MATRIX_H
#define _MATRIX_H
#include <vector>
#include <iostream>
#include "linear_algebra.decl"
#include "matrix.decl"

//! Linear algebra Namespace
namespace linear_algebra
{

  //! matrix is a basic class which covers all kinds of matrices.
  //! Default constructor.
  template <class TN> matrix<TN>::matrix() {_rows=0; _cols=0;}
  
      
      //! Returns number of rows.
  template <class TN> long matrix<TN>::rows() const { return _rows;}
      
      //! Returns number of columns.
  template <class TN> long matrix<TN>::cols() const { return _cols;}
  
  // Allows to write to and read from an element.
  //virtual TN& operator()(long x, long y) { return 0;}
  
  //! Returns the number of entries.
  template <class TN> long matrix<TN>::size() { return _size;}
  
  template <class TN> matrix<TN>& matrix<TN>::operator=(const matrix<TN>& a) { copy(a); return (*this); }

  //! We demand that there is a transpose function.
  /*template <class TN> matrix<TN>& matrix<TN>::transpose()
    {
      throw domain_error("There is no transpose() function defined for this type!");
    }
  */
  //! We demand that there is a zero function.
  /*template <class TN> matrix<TN>& matrix<TN>::zero()
    {
      throw domain_error("There is no zero() function defined for this type!");
    }
  */
  //! We demand that there is a zero function.
  /*template <class TN> matrix<TN>& matrix<TN>::copy(const matrix<TN>& m)
    {
      throw domain_error("There is no copy()/1 function defined for this type!");
    }
  */
  //! We demand that there is a zero function.
  /*  template <class TN> matrix<TN>& matrix<TN>::copy(matrix<TN>& m, long i)
    {
      throw domain_error("There is no copy()/2 function defined for this type!");
      }*/

  template <class TN> TN matrix<TN>::trace() const
    {
      long i;
      TN r=0;
      for(i=0; i<cols() && i<rows(); i++)
	r+=(*this)(i,i);
      return r;
    }
  
  //! Scalar product of two full vectors.
  template<class TN>
    TN operator*( const vector<TN>& a, const vector<TN>& b)
    {
      if(a.size()!=b.size())
	{
	  cerr<< "Differing dimensions in template<class TN> TN operator*(vector<TN>& a, vector<TN> b)." << endl;
	  exit(1);
	}
      TN r=a[0]*b[0];
      long i;
      for(i=1; i<(long) b.size(); i++) r+=a[i]*b[i];
      return r;
    };
  
}
#endif
/*! @}*/
