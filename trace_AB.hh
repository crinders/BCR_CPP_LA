/*! \addtogroup LA */
/*! @{ */
/*! \file trace_AB.hh
  \brief get the trace of matrix products.
*/
#ifndef _TRACE_AB_H
#define _TRACE_AB_H
#include "linear_algebra.h"

#include <cassert>

namespace linear_algebra
{

  template<class T>
  T trace_AB(const mat_sym_full<T>& A,
	     const mat_full<T>& B)
  {
    assert( B.cols() == B.rows() );
    assert( A.cols() == B.cols() );
    long i,j;
    T r=(T) 0.0;
    for(i=0;i<A.cols();i++)
      for(j=0;j<=i;j++)
	r+=A[i*(i+1)/2+j]*B[i][j];
    for(i=0;i<A.cols();i++)
      for(j=0;j<i;j++)
	r+=A[i*(i+1)/2+j]*B[j][i];
    return r;
  }
  template<class T>
  T trace_AB(const mat_full<T>& A,
	     const mat_sym_full<T>& B)
  {
    return trace_AB(B,A);
  }

  //! Compute Trace of matrix product
  template<class TN>
  TN trace_AB(const mat_sym_full<TN>& A, const mat_sym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.rows() != B.cols()) 
      throw domain_error("trace_AB: A.rows!= B.cols");
    try {
#endif
    TN r=(TN) 0;
    long i;
    const double* AP=&A[0];
    const double* BP=&B[0];
    const long N=A.cols()*(A.cols()+1)/2;
    for(i=0;i<N;i++)
      r+=AP[i]*BP[i];
    r*=(TN) 2;
    for(i=0;i<B.cols();i++)
      {
	r-=A[i*(i+3)/2]*B[i*(i+3)/2];
      }
    return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from trace_AB");
    }
#endif
  }

  template<class TN>
  TN trace_AB(const mat_sym_full<TN>& A, const mat_sym_sparse<TN>& B)
  {
#ifdef DEBUG
    if(A.rows() != B.cols()) 
      throw domain_error("trace_AB: A.rows!= B.cols");
    try {
#endif
    TN r=(TN) 0;
    TN e=(TN) 0;
    long i,j;

    // ut ut
    for(i=0;i<B.size();i++)
      {
	long id=B.index(i);
	for(j=0;j<B[i].size() && B[i].index(j)<id;j++)
	  r+=A[id*(id+1)/2+B[i].index(j)]*B[i][j];
	e+=A(id,id)*B[i](id);
      }
    r*=(TN) 2;
    r+=e;
    return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from trace_AB");
    }
#endif
  }

  //! Compute Trace of matrix product
  template<class TN>
  TN trace_AB(const mat_asym_full<TN>& A, const mat_asym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.rows() != B.cols()) 
      throw domain_error("trace_AB: A.rows!= B.cols");
    try {
#endif
      TN r=(TN) 0;
      long i,j;
      const long length=B.size();
      for(i=0;i < length;i++)
	r+=B[i]*A[i];
      r*=(TN) -2;
      return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from trace_AB");
    }
#endif
  }

  //! Compute Trace of matrix product
  template<class TN>
  TN trace_AB(const mat_full<TN>& A, const mat_full<TN>& B)
  {
    TN r=(TN) 0;
#ifdef DEBUG
    if(A.rows() != B.cols()) 
      throw domain_error("trace_AB: A.rows!= B.cols");
    if(A.cols() !=B.rows())
      throw domain_error("trace_AB: A.cols!= B.rows");
    try {
#endif
      long i,j;
      for(i=0;i<A.cols();i++)
        for(j=0;j<A.rows();j++)
          r+=B[i][j]*A[j][i];
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from trace_AB");
    }
#endif
    return r;
  }
  //! Compute Trace of matrix product of A with transpose of B
  template<class TN>
  TN trace_AB_t(const mat_full<TN>& A, const mat_full<TN>& B)
  {
    TN r=(TN) 0;
#ifdef DEBUG
    if(A.rows() != B.rows()) 
      throw domain_error("trace_AB: A.rows!= B.cols");
    if(A.cols() !=B.cols())
      throw domain_error("trace_AB: A.cols!= B.rows");
    try {
#endif
      long i,j;
      for(i=0;i<A.cols();i++)
        for(j=0;j<A.rows();j++)
          r+=B[i][j]*A[i][j];
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from trace_AB");
    }
#endif
    return r;
  }
}
#endif
/*! @} */
