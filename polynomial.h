/** \addtogroup LA */
/** @{ \file polynomial.h 
    \brief Implementations for a class of polynomials
*/
#ifndef _POLYNOMIAL_H
#define _POLYNOMIAL_H
#include "sparse_vector_infty.h"
#include "polynomial.decl"
#include "linear_algebra.h"
#include "class_extensions.h"

namespace linear_algebra {
  
  //! Standard constructor.
  template<class C> polynomial<C>::polynomial() {
    sparse_vector_infty<C> r;
    _p=r;
  };
  
  //! Multiply two polynomials.
  template<class C> polynomial<C>& polynomial<C>::multiply(const polynomial<C>& p,
							   polynomial<C>& r) const
  {
    long i,j;
    for(i=0;i<r._p.size();i++) linear_algebra::zero(r._p[i]);
    for(i=0;i<_p.size();i++)
      for(j=0;j<p._p.size();j++)
	r._p.add(_p.index(i)+p._p.index(j),_p[i]*p._p[j]);
    return r;
  }
  
  //! Multiply two polynomials.
  template<class C> polynomial<C> polynomial<C>::operator*(const polynomial<C>& p) const
  {
    polynomial<C> r;
    multiply(p,r);
    return r;
  }
  
  //! Multiply two polynomials.
  template<class C> polynomial<C> polynomial<C>::operator*=(const polynomial<C>& p)
  {
    polynomial<C> r;
    multiply(p,r);
    _p=r._p;
    return (*this);
  }


  //! Add two polynomials.
  template<class C> polynomial<C> polynomial<C>::operator+(const polynomial<C>& p) const
  {
    polynomial<C> r;
    long i;
    r._p.set(_p.index(i),_p[i]);
    r._p+=p._p;
    return r;
  }

  //! Add two polynomials.
  template<class C> polynomial<C>& polynomial<C>::operator+=(const polynomial<C>& p)
  {
    long i;
    _p+=p._p;
    return (*this);
  }

  //! Acess an element.
  template<class C> const C& polynomial<C>::operator()(const long i) const
  {
    return (const C) _p(i);
  }

  //! Acess an element.
  template<class C> C& polynomial<C>::operator()(const long i)
  {
    return _p(i);
  }
  //! The ith element to a.
  template<class C> void polynomial<C>::set(const long i, const C& a)
  {
    _p.set(i,a);
  }
  //! The ith element to a.
  template<class C> void polynomial<C>::set(const long i, C& a)
  {
    return _p.set(i,a);
  }

  //! Assignment operator
  template<class C> polynomial<C>& polynomial<C>::operator=(const polynomial<C>& a)
  {
    _p=(const sparse_vector_infty<C>) a._p;
  }

  //! Assignment operator
  template<class C> polynomial<C>& polynomial<C>::operator=(polynomial<C>& a)
  {
    _p=a._p;
  }

  //! Define a zero.
  template<class C> polynomial<C>& polynomial<C>::zero()
  {
    _p.zero();
    return (*this);
  }


  template<class C> long int polynomial<C>::index(const long i) const { return _p.index(i); }
  template<class C> long int polynomial<C>::size() const { return _p.size(); }
  template<class C> C polynomial<C>::operator[](const long i) const {return _p[i]; }
  template<class C> C& polynomial<C>::operator[](const long i) { return _p[i]; }
  

  //! Copy assignment.
  template<class C> polynomial<C>& polynomial<C>::copy(const polynomial<C>& a)
  {
    _p.copy(a._p);
    return (*this);
  }

  //! Copy assignment.
  template<class C> polynomial<C>& polynomial<C>::copy(polynomial<C>& a)
  {
    _p.copy(a._p);
    return (*this);
  }

  //! Copy assignment.
  template<class C> polynomial<C>& polynomial<C>::copy(const polynomial<C>& a, const long i)
  {
    _p.copy(a._p,i);
    return (*this);
  }

  //! Copy assignment.
  template<class C> polynomial<C>& polynomial<C>::copy(polynomial<C>& a, const long i)
  {
    _p.copy(a._p,i);
    return (*this);
  }

  
} // end linear_algebra
//!@}
#endif
