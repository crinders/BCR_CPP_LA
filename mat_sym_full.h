/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file mat_sym_full.h
  \brief Provide manipulatins for symmetric full matrices.
  \todo throw exceptions.  
*/
#ifndef _MAT_SYM_FULL_H
#define _MAT_SYM_FULL_H
#include <vector>
#include <stdexcept>
#include "linear_algebra.decl"
#include "refcount.decl"
#include "sparse_vector.decl"
#include "mat_full.decl"
#include "mat_asym_full.decl"
#include "mat_sym_full.decl"

namespace linear_algebra
{
  // Constructors
  

  //! Default constructor.
  template <class TN> mat_sym_full<TN>::mat_sym_full() {_rows=_cols=_size=0; };
  
  //! Constructor to read a stored matrix from a stream. Aimed at retrieving information from a binary save with mat_sym_full::operator>>().
  template <class TN> mat_sym_full<TN>::mat_sym_full(istream& IN)
  {
    long rs,i,j;
#ifdef DEBUG
    const char* string("Full symmetric matrix:\n\x0");
    char a;
    i=0;
    IN.read((char*) &a,sizeof(char));
    while(string[i]==a && string[i]!='\n') 
      {
	i++;
	IN.read((char*) &a,sizeof(char));
      }
    if(string[i]!='\n')
      throw domain_error("This is not a full, symmetric matrix");
#else
    IN.seekg(23,ios::cur);
#endif
    
    IN.read((char*) &rs,sizeof(long)/sizeof(char));
    IN.read((char*) &_cols,sizeof(long)/sizeof(char));
#ifdef DEBUG
    if (rs*sizeof(char)!=_cols*(_cols+1)*sizeof(TN)/2+sizeof(long))
      throw domain_error("Dimension of Matrix and saved data do not agree in mat_sym_full(istream& IN)");
#endif
    
    mat_sym_full<TN> M(_cols);

    M._size=j=_cols*(_cols+1)*sizeof(TN)/sizeof(char)/2;
    
    refvector<char> buffer(j);
    IN.read(&buffer[0],j);
    
    TN b;
    char *pa=(char *) &b;

    for(j=0;j<_cols*(_cols+1)/2;j++)
      {
	for(i=0;i<sizeof(TN)/sizeof(char);i++)
	  pa[i]=buffer[j*sizeof(TN)/sizeof(char)+i];
	M[j]=b;
      }
    *this=M;
  }

  //! Constructor of dimension n with uninitialised values.
  template <class TN> mat_sym_full<TN>::mat_sym_full(long n)
  {
    _rows=_cols=n;
    _size=n*(n+1)/2;
    vector<TN> vals(_size);
    _vals=vals;
  }
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_sym_full<TN>::mat_sym_full(long n, const vector<TN> &vals)
  {
    _rows=_cols=n; 
    _vals=vals;
    _size=n*(n+1)/2;
#ifdef DEBUG
    if (_size!=_vals.size()) throw domain_error("inconsistent sizes mat_sym_full::mat_sym_full(long n, const vector<TN> &vals)");
#endif
  }

  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_sym_full<TN>::mat_sym_full(long n,refvector<TN> &vals)
  {
    _rows=_cols=n; 
    _vals=vals;
    _size=n*(n+1)/2;
#ifdef DEBUG
    if (_size!=_vals->size()) throw domain_error("inconsistent sizes mat_sym_full(long n, const vector<TN> &vals)");
#endif
  }
  
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_sym_full<TN>::mat_sym_full(const mat_full<TN>& A)
  {
    _rows=_cols=A.cols();
    _size=_rows*(_rows+1)/2;
    _vals.resize(_size);
    long i,j;
    for(i=0;i<_cols;i++)
      for(j=0;j<=i;j++)
        _vals[i*(i+1)/2+j]=A[i][j];
    for(i=0;i<_cols;i++)
      for(j=0;j<=i;j++)
        _vals[i*(i+1)/2+j]+=A[j][i];
    _vals*=(TN) 0.5;    
  }
  
  // Operators
  
  //! Assignement operator for non-const matrices.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::operator=(mat_sym_full<TN>& a)
  {
    _rows=a._rows;
    _cols=a._cols;
    _size=a._size;
    _vals=a._vals;
    return *this;
  }
  
  //! Assignement operator for non-const matrices.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::operator=(const mat_sym_full<TN>& a)
  {
    _rows=a._rows;
    _cols=a._cols;
    _size=a._size;
    _vals.copy(a._vals);
    return *this;
  }
  
  //! Output matrix in binary format into a stream. Aimed at file storage.
  template <class TN> void mat_sym_full<TN>::operator>>(ostream& OUT) const
    /*!
      The default is to output a full (column) matrix.
    */
  {
    OUT << "Full symmetric matrix:\n";
    long i,j;
    i=_vals->size()*sizeof(TN)+sizeof(long);
    i/=sizeof(char);
    OUT.write((char*) &i,sizeof(long)/sizeof(char));
    OUT.write((char*) &_cols,sizeof(long)/sizeof(char));
    TN a;
    for(i=0;i<_cols;i++)
      for(j=0;j<=i;j++)
	{
	  a=(*this)(i,j);
	  OUT.write((char*) &a, sizeof(TN)/sizeof(char));
	}
  }

  //! Access operator which checks for integrity in debug mode.
  template <class TN> const TN& mat_sym_full<TN>::operator()(const long x, const long y) const
  {
#ifdef DEBUG
    if( 0<=x && x <_cols && 0<=y && y<_rows)
#endif
      if(x>y)
	return _vals[x*(x+1)/2+y];
      else
	return _vals[y*(y+1)/2+x];
#ifdef DEBUG
    char v[100];
    snprintf(v,100,"illegal access to element in full matrix in const TN& mat_sym_full::operator()(const long x, const long y) const (%li,%li)",x,y);
    throw domain_error(v);
#endif
  }

  //! Access operator which checks for integrity in debug mode.  
  template <class TN> TN& mat_sym_full<TN>::operator()(const long x, const long y)
  {
#ifdef DEBUG
    if( 0<=x && x<_cols && 0<=y && y<_rows)
#endif
      if(x>y)
	return _vals[x*(x+1)/2+y];
      else
	return _vals[y*(y+1)/2+x];
#ifdef DEBUG
    throw domain_error( "illegal access to element in full matrix  in TN& mat_sym_full::operator()(const long x, const long y)");
#endif
  }

  //! Direct access operator which checks for integrity in debug mode.
  template <class TN> const TN& mat_sym_full<TN>::operator[](const long x) const
  {
#ifdef DEBUG
    if( 0<=x && x <_vals.size())
#endif
      return _vals[x];
#ifdef DEBUG
    else
      throw domain_error("illegal access to element in full matrix in const TN& mat_sym_full::operator[](const long x) const");
#endif
  }

  //! Direct access operator which checks for integrity in debug mode.  
  template <class TN> TN& mat_sym_full<TN>::operator[](const long x)
  {
#ifdef DEBUG
    if( 0<=x && x<_vals.size())
#endif
      return _vals[x];
#ifdef DEBUG
    else
      throw domain_error( "illegal access to element in full matrix  in TN& mat_sym_full::operator[](const long x)");
#endif
  }

  //! Extracts a block from a column/row
  template <class TN> refvector<TN> mat_sym_full<TN>::operator()(const long column, const long xmin, const long xmax) const
  {
#ifdef DEBUG
    if(xmin>xmax) {
      throw domain_error( "xmin > xmax in mat_sym_sparse::extract_col(const long column, const long xmin, const long xmax) const" );
      exit(1);
    }
    if(xmin<0 || xmax>_rows) 
      {
	throw domain_error("Matrix dimensions exceeded in mat_sym_sparse::extract_col(const long column, const long xmin, const long xmax) const" );
	exit(1);
      }
#endif
    try {
      refvector<TN> r(xmax-xmin);
      long i,j;
      for(i=xmin; i<=column && i<xmax; i++) r[i-xmin]=_vals[column*(column+1)/2+i];
      for(i=column+1;i<xmax;i++) r[i-xmin]=_vals[i*(i+1)/2+column];
      return r;
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by  refvector<TN> mat_sym_sparse::operator()(const long column, const long xmin, const long xmax) const");
    }
  }
  
  //! Extracts a column/row from the matrix.
  template <class TN> refvector<TN> mat_sym_full<TN>::operator()(const long column) const
  {
    return((*this)(column,0,_cols));
  }
  
  // Normal Member functions
  
  
  //! Addition operation for compatibility with mat_sym_sparse.
  template <class TN> mat_sym_full<TN>&  mat_sym_full<TN>::multiply(const long x, const long y, TN value)
  {
    (*this)(x,y)*=value;
  }
  
  //! Multiplies a symmetric full matrix with a full matrix.
  template <class TN> mat_full<TN>& mat_sym_full<TN>::multiply(const mat_full<TN>& b,mat_full<TN>& r) const
  {
#ifdef DEBUG
    if(_cols != b.rows())
      throw domain_error("Incompatible dimensions in friend  mat_full<TN> multiply(mat_sym_full<TN> a, mat_full<TN> b)");
    if(_cols != r.rows() || r.cols()!=b.cols())
      throw domain_error("Incompatible dimensions for output in friend  mat_full<TN> multiply(mat_sym_full<TN> a, mat_full<TN> b)");
#endif
    long i;
    for(i=0;i<b.cols();i++) multiply(b[i],r[i]);
    return r;
  }
  
  template <class T> mat_full<T> mat_sym_full<T>::operator*(const mat_full<T>& b) const
  {
    mat_full<T> r(b.cols(),_rows);
    return multiply(b,r);    
  }
  
  //! Multiply a symmetric full matrix with a symmetric full matrix.
  template <class TN> mat_full<TN>& mat_sym_full<TN>::multiply(const mat_sym_full<TN>& b,mat_full<TN>& r) const
  {
#ifdef DEBUG
    if((*this).cols() != b.rows())
      throw domain_error("Incompatible dimensions in matrix::operator*( matrix& b)");
    if(r.cols() != b.cols() || r.rows() != _rows)
      throw domain_error("Incompatible dimensions for output in matrix::operator*( matrix& b)");
    try
      {
#endif
	long i,j,k,l;
	
	for(i=0;i<r.cols();i++)
	  for(j=0;j<r.rows();j++) r[i][j]=(TN) 0;
	
	// upper triangle  upper triangle
	
	for(k=0;k<b.cols();k++)
	  for(i=0;i<=k;i++)
	    for(j=0;j<=i;j++)
	      r[k][j]+=(*this)[i*(i+1)/2+j]*b[k*(k+1)/2+i];
	
	// lt lt
	
	for(i=2;i<_cols;i++)
	  for(k=1;k<i;k++)
	    for(j=0;j<k;j++)
	      r[j][i]+=(*this)[i*(i+1)/2+k]*b[k*(k+1)/2+j];
	
	// ut lt
	
	for(k=0;k<_cols;k++)
	  for(l=0;l<k;l++)
	    for(j=0;j<=k;j++)
	      r[l][j]+=(*this)[k*(k+1)/2+j]*b[k*(k+1)/2+l];
	
	// lt ut 
	
	for(k=0;k<b.cols();k++)
	  {
	    for(i=0;i<=k;i++)
	      for(l=0;l<i;l++)
		r[k][i]+=(*this)[i*(i+1)/2+l]*b[k*(k+1)/2+l];
	    for(i=k+1;i<_cols;i++)
	      for(l=0;l<=k;l++)
		r[k][i]+=(*this)[i*(i+1)/2+l]*b[k*(k+1)/2+l];
	  }
	
	/*
	  for(i=0; i< b.rows(); i++)
	  for(j=0;j<(*this).rows();j++)
	  r[i]=(*this)*b(i);
	*/
#ifdef DEBUG
      }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::operator*(matrix& b) const");
      }
#endif
    return r;
  }
  
  //! Multiply a symmetric full matrix with a symmetric full matrix.
  template <class TN> mat_full<TN> mat_sym_full<TN>::operator*(const mat_sym_full<TN>& b) const
  {
    mat_full<TN> r(b.cols(),_rows);
    return multiply(b,r);
  }
  
  //! Multiply a symmetric full matrix with an antisymmetric full matrix
  template <class TN> mat_full<TN>& mat_sym_full<TN>::multiply(const mat_asym_full<TN>& b, mat_full<TN>& r) const
  {
#ifdef DEBUG
    if((*this).cols() != b.rows())
      throw domain_error("Incompatible dimensions in mat_sym_full::operator*( mat_asym_full& b)");
    if(r.cols() != b.cols() || r.rows()!=_rows)
      throw domain_error("Incompatible dimensions for output in mat_sym_full::operator*( mat_asym_full& b)");
    try
      {
#endif
	long i,j,k;
	for(i=0; i< b.rows(); i++)
	  for(j=0;j<(*this).rows();j++)
	    multiply(b.extract_col(i),r[i]);
#ifdef DEBUG
      }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::operator*(matrix& b) const");
      }
#endif
    return r;
  }
  
  //! Multiply a symmetric full matrix with an antisymmetric full matrix
  template <class TN> mat_full<TN> mat_sym_full<TN>::operator*(const mat_asym_full<TN>& b) const
  {
    mat_full<TN> r(b.cols(),_rows);
    return multiply(b,r);
  }
  
  //! Multiply a symmetric full matrix with a sparse vector.
  template <class TN> template<class TN2>
  refvector<TN>& mat_sym_full<TN>::multiply(const sparse_vector<TN2>& v, refvector<TN>& r) const
  {
#ifdef DEBUG
    if(v.dim()!=_cols)
      throw("Incompatible dimensions refvector<TN> mat_sym_full::operator*(const sparse_vector<TN>& v)");
    if(_rows!=r.dim())
      throw("Incompatible dimensions for output refvector<TN> mat_sym_full::operator*(const sparse_vector<TN>& v)");
#endif
    long i,j;
    
    // We will split A into a lower and an upper matrix.
    // Lower matrix
    for(i=0;i<_rows;i++)
      {
	r[i]=0;
	for(j=0; j<v._index.size() && v._index[j]<=i; j++) r[i]+=_vals[i*(i+1)/2+v._index[j]]*v._vals[j];
      }
    // Upper matrix
    for(i=0;i<v._index.size() && v._index[i]<_rows;i++)
      {
	long idx=v._index[i]*(v._index[i]+1)/2;
	for(j=0;j<v._index[i] ; j++) r[j]+=_vals[idx+j]*v._vals[i];
      }
    return r;
  }
  
  //! Multiply a symmetric full matrix with a sparse vector.
  template <class TN> template<class TN2>
  refvector<TN> mat_sym_full<TN>::operator*(const sparse_vector<TN2>& v) const
  {
    refvector<TN> r(_rows);
    return multiply(v,r);
  }
  
  //! Multiply a symmetric full matrix with a full vector.
  template <class TN> template<class TN2>
  refvector<TN>& mat_sym_full<TN>::multiply(const refvector<TN2>& v,refvector<TN>& r) const
  {
#ifdef DEBUG
    if(v.size()!=_cols)
      throw("Incompatible dimensions refvector<TN> operator*(const mat_sym_full& A, const refvector<TN>& v)");
    if(r.dim()!=_rows)
      throw("Incompatible dimensions for output in refvector<TN> operator*(const mat_sym_full& A, const refvector<TN>& v)");
    if(v.same(r))
      throw domain_error("r and v may not be the same: use refvector::operator*=(const mat_sym_full instead.");
#endif
    long i,j;
    
    // We will split A into a lower and an upper matrix.
    // Lower matrix
    for(i=0;i<_rows;i++)
      {
	r[i]=_vals[i*(i+1)/2]*v[0];
	for(j=1; j<=i; j++) r[i]+=_vals[i*(i+1)/2+j]*v[j];
	
      }
    // Upper matrix
    for(i=0;i<_rows;i++)
      for(j=0;j<i; j++) r[j]+=_vals[i*(i+1)/2+j]*v[i];
    return r;
  }
  
  //! Multiply a symmetric full matrix with a full vector.
  template <class TN> template<class TN2>
  refvector<TN> mat_sym_full<TN>::operator*(const refvector<TN2>& v) const
  {
    refvector<TN> r(v.dim());
    return multiply(v,r);
  }
  
  //! Multiply a symmetric full matrix with an element.
  template <class TN> template<class TN2>
  mat_sym_full<TN> mat_sym_full<TN>::operator*=(const TN2& a)
  {
    for(long i=0; i<_cols*(_cols+1)/2;i++)
      _vals[i]*=a;
    return *this;
  }
  
  //! Divide a symmetric full matrix by an element.
  template <class TN> template<class TN2>
  mat_sym_full<TN>& mat_sym_full<TN>::operator/=(const TN2& a)
  {
    const TN b=(TN) 1/a;
    for(long i=0; i<_cols*(_cols+1)/2;i++)
      _vals[i]*=b;
    return *this;
  }
  
  //! Multiply a symmetric full matrix with an element.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::multiply(const TN& a, mat_sym_full<TN>& r) const
  {
#ifdef DEBUG
    if(r.cols()!=_cols)
      throw domain_error("output dimension in mat_sym_full::multiply(const TN& a,...).");
#endif
    long i,j;
    for(i=0; i<_cols;i++)
      for(j=0; j<=i; j++)
	r[i*(i+1)/2+j]=_vals[i*(i+1)/2+j]*a;
    return r;
  }
  
  //! Multiply a symmetric full matrix with an element.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::operator*(const TN& a) const
  {
    mat_sym_full<TN> r(_cols);
    return multiply(a,r);
  }
  
  //! Divide a symmetric full matrix by an element.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::operator/(const TN& a) const
  {
    const TN b=(TN) 1/a;
    return (*this)*b;
  }
  
  //! Add two symmetric matrices.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::operator+=(const mat_sym_full<TN>& a)
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator+=(const mat_sym_full<TN>& a)");
#endif
    
    for(long i=0; i<_cols*(_cols+1)/2; i++)
      _vals[i]+=a[i];
    return *this;
  }
  
  //! Add two symmetric matrices.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::operator+=(const mat_sym_sparse<TN>& a)
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator+=(const mat_sym_sparse<TN>& a)");
    try {
#endif
      long i,j;
      for(i=0;i<a.size();i++)
	{
	  long id=a.index(i);
	  for(j=0;j<a[i].size();j++)
	    {
	      long jd=a[i].index(j);
	      (*this)[id*(id+1)/2+jd]+=a[i][j];
	    }
	}
      return (*this);
#ifdef DEBUG
    } catch (domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called by mat_sym_full::operator+=(const mat_sym_sparse<TN>& a)");
    }
#endif
  }

  
  //! Subtract two symmetric matrices.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::operator-=(const mat_sym_full<TN>& a)
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator-=(const mat_sym_full<TN>& a)");
#endif
    
    for(long i=0; i<_cols*(_cols+1)/2; i++)
      _vals[i]-=a[i];
    return *this;
  }
  
  //! Subtract two symmetric matrices.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::operator-=(const mat_sym_sparse<TN>& a)
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator+=(const mat_sym_sparse<TN>& a)");
    try {
#endif
      long i,j;
      for(i=0;i<a.size();i++)
	{
	  long id=a.index(i);
	  for(j=0;j<a[i].size();j++)
	    {
	      long jd=a[i].index(j);
	      (*this)[id*(id+1)/2+jd]-=a[i][j];
	    }
	}
      return (*this);
#ifdef DEBUG
    } catch (domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called by mat_sym_full::operator-=(const mat_sym_sparse<TN>& a)");
    }
#endif
  }
  
  //! Add two symmetric matrices.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::operator+(const mat_sym_full<TN>& a) const
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator+(const mat_sym_full<TN>& a) const");
#endif
    mat_sym_full<TN> M(_cols);
    for(long i=0; i<_cols*(_cols+1)/2; i++)
      M._vals[i]=_vals[i]+a[i];
    return M;
  }
  
  //! Subtract two symmetric matrices.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::operator-(const mat_sym_full<TN>& a) const
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator-(const mat_sym_full<TN>& a) const");
#endif
    mat_sym_full<TN> M(_cols);
    for(long i=0; i<_cols*(_cols+1)/2; i++)
      M._vals[i]=_vals[i]-a[i];
    return M;
  }
  
  //! Subtract two symmetric matrices, one full one sparse.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::operator-(const mat_sym_sparse<TN>& a) const
  {
#ifdef DEBUG
    if(_cols!=a.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::operator-(const mat_sym_full<TN>& a) const");
#endif
    mat_sym_full<TN> M(_cols);
    M.copy((*this));
    for(long i=0; i<a.size(); i++)
      for(long j=0; j<a[i].size();j++)
	M[a.index(i)*(a.index(i)+1)/2+a[i].index(j)]-=a[i][j];
    return M;
  }
  
  //! Simple display of matrix
  template <class TN> bool mat_sym_full<TN>::display() const
  {
#ifdef DEBUG
    try {
#endif
      long i,j,l;
      for(i=0;i<_cols;i++)
	cout << i << "        ";
      cout << endl;
      for(i=0,l=0; i<_rows; i++)
	{
	  cout << i << "   ";
	  for(j=0; j<=i; j++,l++) cout << _vals[l] << " ";
	  cout << endl;
	}
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from mat_sym_full::display");
    }
#endif
    return true;
  }
  
  //! Assignment operation for compatibility with mat_sym_sparse.
  template <class TN> void mat_sym_full<TN>::set(const long x, const long y, TN value)
  {
    (*this)(x,y)=value;
  }
  
  //! Addition operation for compatibility with mat_sym_sparse.
  template <class TN> bool mat_sym_full<TN>::add( long x, long y, const TN value)
  {
    (*this)(x,y)+=value;
    return true;
  }
  
  //! Produce the necessary \f$ij^\ast+ji^\ast\f$.
  template <class TN> void mat_sym_full<TN>::gen_mat(const refvector<TN>& a, const refvector<TN>& b, TN correction)
    /*!
      the result is \latexonly $-=ab^\ast + ba^\ast$\endlatexonly (-=ab^\ast + ba^\ast).
    */
  {
    long i,j;
    TN ai;
    if(a->size() != b->size()) throw domain_error("mat_sym_full::gen_mat error in dimensions");
    for(i=0; i<a.size(); i++)
      {
	ai=a[i]*correction;
	for(j=0; j<=i; j++)
	  {
	    _vals[i*(i+1)/2+j]-=ai*b[j];
	    //	  add(b.index(j),k,-ai*b[j]);
	  }
      }
    for(i=0; i<b.size(); i++)
      {
	ai=b[i]*correction;
	for(j=0; j<=i; j++)
	  {
	    _vals[i*(i+1)/2+j]-=ai*a[j];
	  }
      }
  }
  
  //! Transforms a symmetric full matrix by a unitary matrix. O(n^4) UAU^t
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::unitary_transform(const mat_full<TN> U) const
    /*!
      This transform is only suggested if memory management is of importance.
      Otherwise using two matrix mulitplications is faster O(n^3).
    */
  {
#ifdef DEBUG
    if(_cols!=U.cols() || _rows != U.cols())
      throw domain_error("Incompatble dimensions in mat_sym_full::unitary_transform(const mat_sym_full<TN>& a) const");
#endif
    const long n=U.rows();
    mat_sym_full<TN> R(n);
    
    long i,k,l,j;
    for(i=0;i<n*(n+1)/2;i++) R[i]=0;
    
    for(k=0;k<n;k++)
      for(i=0;i<n;i++)
	{
	  for(l=0;l<k;l++)
	    for(j=0;j<=i;j++)
	      R[i*(i+1)/2+j]+=_vals[k*(k+1)/2+l]*(U[l][j]*U[k][i]+U[k][j]*U[l][i]);
	  for(j=0;j<=i;j++)
	    R[i*(i+1)/2+j]+=_vals[k*(k+3)/2]*U[k][j]*U[k][i];
	}
    
    return R;
  }
  
  //! This routine produces the diagonal, sub/superdiagonal and transformation matrix for the Householder transformation of a sparse symmetric matrix.
  template <class TN> mat_full<TN> mat_sym_full<TN>::householder(refvector<TN>& diagonal, refvector<TN>& subdiagonal)
    /*!
      The aspired transformation is a product of \f$(I-v_iv_i^\ast)\f$ where \f$v_i\f$ is
      \f$(\pm e_1\|a_{12}\| + a_{12})/\sqrt{\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2}\f$.
      Notice that we begin counting at 0.
      \n The matrix is split up according to \f$A=\left(\begin{array}{cc} a_{11} & a_{12}^\ast \\ a_{12} & A_{22} \end{array}\right)\f$
      We use preconditioning such that the matrix is ordered with lowest off-diagonal value in the low right=hand corner.
      \f$\left(\begin{array}{ccccc} a_{11} & \cdots & & \\ & \ddots & & \\ & & \ddots & 0 \\ & & & a_{nn}\end{array}\right)\f$
      \n The returned matrix U will transfrom S to tridiagonal form via \f$U^\ast SU\f$.
    */
  {
#ifdef DEBUG
    try {
#endif
      diagonal.resize(_cols);
      subdiagonal.resize(_cols);
      if (diagonal.size()==1) {
	diagonal[0]=(*this)(0,0);
	mat_full<TN> T(1,1);
	T(0,0)=1;
	return T;
      }
      
      refvector<refvector<TN> > T(_cols);

      long i,j;
      TN absval;     // absolute value of current iteration generator
      TN absval2;    // square of the absolute value
      refvector<TN> u2;
      
      TN d;  // value of the current diagonal
      TN sd; // value of the current subdiagonal
      
      // The aspired transformation is a multiplication by (I-vv^\ast) where v is
      // (pm e_1\|a_{12}\| + a_{12})/\sqrt{\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2}
      // notice that we begin counting at 0.
      
      for(i=_cols-1; i> 1; i--)
	{
	  	  
	  refvector<TN> u(_rows);

	  // The following reduces accesses to the values.

	  for(j=0;j<i;j++)
	    u[j]=_vals[i*(i+1)/2+j]; // Let u be the ith column above the diagonal u = a_{12}
	  for(j=i;j<_rows;j++) u[j]=0;
	  sd=u[i-1];
	  d=_vals[i*(i+3)/2];
	  
	  // The transformation vector is zero in the first element.
	  
	  diagonal[i]=d;          // d=a_{11}
	  absval2=u*u;            // absval2 = \|a_{12}\|^2	    
	  absval=sqrt(absval2);   // absval = \|a_{12}\|
	  if (absval!=0) {     // Precautions for a_{12} \approx 0
	    
	    if(sd>0) subdiagonal[i]=absval;
	    else subdiagonal[i]=0-absval;
	    
	    /*
	      Store the transformation generator u.
	      The denominator will not be included in u.
	      Instead, to avoid taking the root and remultiplying,
	      the contributions will be added explicitly
	    */
	    
	    absval2=(TN) 1/(absval2+subdiagonal[i]*sd);// \pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2
	    //	    absval=(TN) 1/absval;
	    
	    u[i-1]+=subdiagonal[i];   // u = a_{12}\pm \|a_{12}\|e_1
	    u*=sqrt(absval2);         // u = (a_{12}\pm a_{12,1}e_1)/(\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2)
	    subdiagonal[i]*=(-1);     // = \mp \|a_{12}\| = a_{12}-uu^\ast a_{12}
	    
	    // Now we do the transformation for the remainder of A
	    
	    /* u2=Au */
	    
	    u2=(*this)*u;
	    
	    /* u^TAu = u^Tu2 */
	    
	    TN dummy=u*u2;
	    
	    /* u2 = Au - (u^TAu)/2 u = u2-dummy/2*u */
	    
	    //dummy/=2; // Our transformation matrix is I-2uu^T/\|u\|^2
	    dummy*=0.5;
	    
	    u2-=u*dummy;
	    
	    /*
		A' = A - uu2^T - u2u^T
		   = A - (uu^tA - u^TAu/2 uu^T)
		       - (Auu^t - u^TAu/2 uu^T)
		   = A - uu^TA - Auu^T + uu^TAuu^T
	    */
	    
	    gen_mat(u,u2, 1.0);
	    
	    //display();
	  }
	  else
	    for(j=0;j<=i;j++) u[j]=0;
	  
	  /* Store u in T */
	  
	  T[i]=u;
	} // Matches For(i=col-1;i>1; i--)
      
      diagonal[1]=(*this)(1,1);
      diagonal[0]=(*this)(0,0);
      subdiagonal[1]=(*this)(1,0);
      
      // Generate transformation matrix.
      
      mat_full<TN> r(_cols,_rows);

      for(i=0;i<_cols;i++) diagonal[i]=(*this)(i,i);
      for(i=1;i<_cols;i++) subdiagonal[i]=(*this)(i,i-1);
      
      for(i=_cols-1; i>=0; i--) r[i][i]=1; // we start with the identity.
      
      refvector<TN> u;/*
      for( i=2; i<T->size(); i++)
	{
	  //T[i].display();
	  cout << " " << T[i]*T[i] << endl;
	  }*/

      for(i=2; i<_cols; i++)
	{
	  u.copy(T[i]);
	  u2=r*u;
	  for(j=0; j<u.size(); j++)
	    r[j]-=u2*u[j]; // No danger here because there are no empty columns.
	}
      return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from mat_sym_full::householder");
    }
#endif
  } 
  
  //! Copy matrix with full depth.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::copy(const mat_sym_full<TN>& a)
  {
    _vals.copy(a._vals);
    _rows=a._rows;
    _cols=a._cols;
    return *this;
  }  
  
  //! Copy matrix with depth i.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::copy(const mat_sym_full<TN>& a, const long i)
  {
    _vals.copy(a._vals,i);
    _rows=a._rows;
    _cols=a._cols;
    return *this;
  }
  
  //! Copy matrix with depth i.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::copy(mat_sym_full<TN>& a, const long i)
  {
    _vals.copy(a._vals,i);
    _rows=a._rows;
    _cols=a._cols;
    return *this;
  }

  //! Copy matrix with depth 1.
  template <class TN> symmetric<TN>& mat_sym_full<TN>::copy(const symmetric<TN>& a) { return copy(a); }
  
  //! Copy matrix with depth i.
  template <class TN> symmetric<TN>& mat_sym_full<TN>::copy(const symmetric<TN>& a, const long i) { return copy(a,i); }
  
  //! Copy matrix with depth i.
  template <class TN> symmetric<TN>& mat_sym_full<TN>::copy(symmetric<TN>& a, const long i) { return copy(a,i); }
  
  
  //! Release the memory associated with the matrix
  template <class TN> void mat_sym_full<TN>::clear()
  {
    refvector<TN> a;
    _vals=a;
    _rows=_cols=0;
    _size=0;
  }
  
  //! This is the unitary transform \f$UAU^\ast\f$.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::uut(const mat_full<TN>& U, mat_sym_full<TN>& R, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(_cols!= U.cols() || _rows != U.cols())
      throw domain_error("Incompatble dimensions in symmetric::uut(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
    if(R.rows()!=U.rows())
      throw domain_error("Incompatble dimensions for R in symmetric::uut(const mat_sym_full<TN>& U, mat_sym_full<TN>& R, mat_full<TN>& I) const");
    if(I.cols() !=_cols || I.rows() !=U.rows())
      throw domain_error("Incompatble dimensions for I in symmetric::uut(const mat_sym_full<TN>& U, mat_sym_full<TN>& R, mat_full<TN>& I) const");
#endif
    U.multiply((*this),I);
    long i,j,k;
    if(I.cols()>0)
      for(i=0;i<U.rows();i++)
	for(j=0;j<=i;j++)
	  R[i*(i+1)/2+j]=U[0][i]*I[0][j];
    for(k=1;k<I.cols();k++)
      for(i=0;i<U.rows();i++)
	for(j=0;j<=i;j++)
	  R[i*(i+1)/2+j]+=U[k][i]*I[k][j];
    return R;
  }
  
  //! This is the unitary transform \f$UAU^\ast\f$.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::uut(const mat_full<TN>& U,mat_sym_full<TN>& R) const
  {
    mat_full<TN> I(_cols,U.rows());
    return uut(U,R,I);
  }
  
  //! This is the unitary transform \f$UAU^\ast\f$.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::uut(const mat_full<TN>& U) const
  {
    mat_sym_full<TN> R(U.rows());
    mat_full<TN> I(_cols,U.rows());
    return uut(U,R,I);
  }
  
  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::utu(const mat_sym_full<TN>& a, mat_sym_full<TN>& R, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=R.cols())
      throw domain_error("Incompatible dimensions for output in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=I.cols() || I.rows() !=_cols)
      throw domain_error("Incompatible dimensions for intermediate in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    multiply(a,I);
    long i,j;
    refvector<TN> v(_rows);
    for(i=0;i<R.cols();i++)
      {
	a.multiply(I[i],v);
	for(j=0;j<=i;j++)
	  R[i*(i+1)/2+j]=v[j];
      }
    return R;
  }
  
  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::utu(const mat_sym_full<TN>& a, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=I.cols() || I.rows() !=_cols)
      throw domain_error("Incompatible dimensions for intermediate in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    mat_sym_full<TN> R(_cols);
    multiply(a,I);
    long i,j;
    refvector<TN> v(_rows);
    for(i=0;i<R.cols();i++)
      {
	a.multiply(I[i],v);
	for(j=0;j<=i;j++)
	  R[i*(i+1)/2+j]=v[j];
      }
    return R;
  }
  
  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::utu(const mat_sym_full<TN>& a, mat_sym_full<TN>& R) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=R.cols())
      throw domain_error("Incompatible dimensions for output in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    mat_full<TN> I(a.cols(),_rows);
    return utu(a,R,I);
  }
  
  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::utu(const mat_sym_full<TN>& a) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    mat_full<TN> R(a.cols(),_rows);
    return utu(a,R);
  }
  
  //! This is the unitary transform \f$U^\ast AU\f$.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::utu(const mat_full<TN>& U, mat_sym_full<TN>& R, refvector<TN>& r) const
  {
#ifdef DEBUG
    if(_cols!= U.rows() || _rows != U.rows())
      throw domain_error("Incompatble dimensions in symmetric::utu(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
    if(R.cols()!=U.cols())
      throw domain_error("Incompatble dimensions for output in symmetric::utu(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
    if(r.dim()!=_rows)
      throw domain_error("Incompatble dimensions for output in symmetric::utu(const mat_full<TN>& U, mat_sym_full<TN>& R, refvector<TN>&) const");
#endif
    const long n=U.cols();
    
    /* This is also order n^3*/
    long i,j;
    for(i=0;i<n;i++)
      {
	multiply(U[i],r);
	for(j=0;j<=i;j++) R[i*(i+1)/2+j]=U[j]*r;
      }
    return R;
  }
  
  //! This is the unitary transform \f$U^\ast AU\f$.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::utu(const mat_full<TN>& U, mat_sym_full<TN>& R) const
  {
#ifdef DEBUG
    if(_cols!= U.rows() || _rows != U.rows())
      throw domain_error("Incompatble dimensions in symmetric::utu(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
    if(R.cols()!=U.cols())
      throw domain_error("Incompatble dimensions for output in symmetric::utu(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
#endif
    const long n=U.cols();
    
    /* This is also order n^3*/
    long i,j;
    refvector<TN> r(_rows);
    utu(U,R,r);
    return R;
  }
  
  //! This is the unitary transform U^tAU.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::utu(const mat_full<TN>& U) const
  {
    mat_sym_full<TN> R(U.cols());
    utu(U,R);
    return R;
  }

  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::utu(const mat_asym_full<TN>& a, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=I.cols() || I.rows() !=_cols)
      throw domain_error("Incompatible dimensions for intermediate in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    mat_sym_full<TN> R(_cols);
    multiply(a,I);
    long i,j;
    refvector<TN> v(_rows);
    for(i=0;i<R.cols();i++)
      {
	a.multiply(I[i],v);
	for(j=0;j<=i;j++)
	  R[i*(i+1)/2+j]=-v[j];
      }
    return R;
  }
  
  //! This is the unitary transform U^tAU.
  template <class TN> mat_sym_full<TN> mat_sym_full<TN>::utu(const mat_asym_full<TN>& U) const
  {
    mat_sym_full<TN> R(U.cols());
    utu(U,R);
    return R;
  }
  
  //! Prune the matrix of negligible values.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::prune(const TN tol)
  {
    long i;
    for(i=0;i<_vals.dim();i++)
      if(_vals[i]<=tol && _vals[i] >= -tol) _vals[i]=(TN) 0;
    return *this;
  }
  //! Set the matrix to zero.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::zero()
  {
    _vals.zero();
    return (*this);
  }

  //! Constructor of dimension n with uninitialised values.
  template <class TN> mat_sym_full<TN>& mat_sym_full<TN>::resize(long n)
  {
    _rows=_cols=n;
    _size=n*(n+1)/2;
    _vals.resize(_size);
    return *this;
  }

} // linear_algebra::

#include "refcount.h"
#include "sparse_vector.h"
#include "mat_full.h"
#include "mat_asym_full.h"
#endif
/*! @}*/
