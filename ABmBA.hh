/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file ABmBA.hh
*/
#ifndef _ABMBA_HH
#define _ABMBA_HH
#include "linear_algebra.h"

namespace linear_algebra
{
  
  //! Computes the commutator AB-BA.
  template<class TN>
  mat_asym_full<TN>& ABmBA(const mat_asym_full<TN>& A,
			   const mat_asym_full<TN>& B,
			   mat_asym_full<TN>& r)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows() || B.cols() != A.rows())
      throw domain_error("ABmBA: A and B don't agree on dimensions");
    if(r.cols()!=A.cols())
      throw domain_error("ABmBA: A and output r don't agree on dimensions");
    try {
#endif
      long i,j,k,l;
      long count;
      //      r.zero();      
      // upper triangle  upper triangle
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(j=0;j<i;j++)
	    r[k*(k-1)/2+j]+=A[i*(i-1)/2+j]*B[k*(k-1)/2+i];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(j=0;j<i;j++)
	    r[k*(k-1)/2+j]-=B[i*(i-1)/2+j]*A[k*(k-1)/2+i];
      
      // lt lt is not needed because we are anti-symmetric.
      
      // ut lt
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l-1)/2+j]-=A[k*(k-1)/2+j]*B[k*(k-1)/2+l];
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l-1)/2+j]+=B[k*(k-1)/2+j]*A[k*(k-1)/2+l];
      
      // lt ut 
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k-1)/2+i]-=A[i*(i-1)/2+l]*B[k*(k-1)/2+l];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k-1)/2+i]+=B[i*(i-1)/2+l]*A[k*(k-1)/2+l];

#ifdef DEBUG
    }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::ABmBA*(mat_asym_full,mat_asym_full) const");
      }
#endif
    //r*=(TN) -1;
    return r;
  }
  
  //! Computes the commutator AB-BA.
  template<class TN>
  mat_asym_full<TN> ABmBA(const mat_asym_full<TN>& A,
			  const mat_asym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABmBA: A and B don't agree on dimensions");
#endif
    mat_asym_full<TN> r(A.cols());
    return ABmBA(A,B,r);
  }
  //! Computes the commutator AB-BA.
  template<class TN>
  mat_sym_full<TN>& ABmBA(const mat_sym_full<TN>& A,
			  const mat_asym_full<TN>& B,
			  mat_sym_full<TN>& r)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows() || B.cols() != A.rows())
      throw domain_error("ABmBA: A and B don't agree on dimensions");
    if(r.cols()!=A.cols())
      throw domain_error("ABmBA: A and output r don't agree on dimensions");
    try {
#endif
      long i,j,k,l;
      
      //for(i=0;i<r.size();i++) r[i]=(TN) 0;
      
      // upper triangle  upper triangle
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(j=0;j<=i;j++)
	    r[k*(k+1)/2+j]+=A[i*(i+1)/2+j]*B[k*(k+1)/2+i];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(j=0;j<=i;j++)
	    r[k*(k+1)/2+j]-=B[i*(i+1)/2+j]*A[k*(k+1)/2+i];
      
      // lt lt is not needed because we are symmetric.
      
      // ut lt
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l+1)/2+j]-=A[k*(k+1)/2+j]*B[k*(k+1)/2+l];
      
      for(k=0;k<A.cols();k++)
	for(l=0;l<k;l++)
	  for(j=0;j<l;j++)
	    r[l*(l+1)/2+j]-=B[k*(k+1)/2+j]*A[k*(k+1)/2+l];
      
      // lt ut 
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k+1)/2+i]+=A[i*(i+1)/2+l]*B[k*(k+1)/2+l];
      
      for(k=0;k<B.cols();k++)
	for(i=0;i<=k;i++)
	  for(l=0;l<i;l++)
	    r[k*(k+1)/2+i]+=B[i*(i+1)/2+l]*A[k*(k+1)/2+l];
      
#ifdef DEBUG
    }
    catch (domain_error e) 
      {
	cerr << e.what();
	throw domain_error("called from matrix::operator*(matrix& b) const");
      }
#endif
    return r;
  }
  
  //! Computes the commutator AB-BA.
  template<class TN>
  mat_sym_full<TN> ABmBA(const mat_sym_full<TN>& A,
			 const mat_asym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABmBA: A and B don't agree on dimensions");
#endif
    mat_sym_full<TN> r(A.cols());
    return ABmBA(A,B,r);
  }

  //! Computes the commutator AB-BA.
  template<class TN>
  mat_sym_full<TN>& ABmBA(const mat_asym_full<TN>& A,
			  const mat_sym_full<TN>& B,
			  mat_sym_full<TN>& r)
  {
    return (ABmBA(B,A,r)*=(TN) (-1));
    //    return r;
  }
  
  //! Computes the commutator AB-BA.
  template<class TN>
  mat_sym_full<TN> ABmBA(const mat_asym_full<TN>& A,
			 const mat_sym_full<TN>& B)
  {
#ifdef DEBUG
    if(A.cols()!=B.rows())
      throw domain_error("ABmBA: A and B don't agree on dimensions");
#endif
    mat_sym_full<TN> r(A.cols());
    return (ABmBA(B,A,r)*=(TN) (-1));
    //    return r;
  }
}; // namespace

#endif
/*! @}*/

