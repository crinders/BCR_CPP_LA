/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file mat_asym_full.h
  \brief Provide manipulations for antisymmetric full matrices.
  \todo throw exceptions.

*/
#ifndef _MAT_ASYM_FULL_H
#define _MAT_ASYM_FULL_H
#include <vector>
#include "linear_algebra.decl"
#include "refcount.decl"
#include "sparse_vector.decl"
#include "mat_full.decl"
#include "mat_sym_full.decl"
#include "mat_sym_sparse.decl"
#include "mat_asym_full.decl"
#include <cmath>
#include <cassert>
#include <cstdio>
#include <cstdlib>

namespace linear_algebra
{

  //! mat_asym_full uses packing for full antisymmetric matrices
  //! Default constructor.

  template <class TN> mat_asym_full<TN>::mat_asym_full():
    _vals(), _vals_r(_vals) { _rows=_cols=_size=0; };

  //! Copy constructor.
  template <class TN> mat_asym_full<TN>::mat_asym_full(const mat_asym_full<TN>& a):
    _vals_r(_vals) {
    _rows=a._rows;
    _cols=a._cols;
    _size=a._size;
    _vals=a._vals_r;
  }  
  
  //! Constructor of dimension n with uninitialised values.
  template <class TN> mat_asym_full<TN>::mat_asym_full(long n):
    _vals_r(_vals)
  {
    _rows=_cols=n;
    _size=n*(n-1)/2;
    vector<TN> vals(_size);
    _vals=vals;
  }
  
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_full<TN>::mat_asym_full(long m, const TN* vals):
    _vals_r(_vals)
  {
    const long n=(long) (sqrt(2.0*(double) m+0.25)+0.5);
    assert( n*(n-1)/2 == m);
    _rows=_cols=n;
    refvector<TN> a(m);
    for(long i=0;i <m;i++) a[i]=vals[i];
    _vals=a;
  }
  
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_full<TN>::mat_asym_full(long n, const vector<TN> &vals):
    _vals_r(_vals)
  {
    _rows=_cols=n; 
    _vals=vals;
    _size=n*(n-1)/2;
#ifdef DEBUG
    if (_size!=_vals.size()) throw domain_error("inconsistent sizes mat_asym_full::mat_asym_full(long n, const vector<TN> &vals)");
#endif
  }
  
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_full<TN>::mat_asym_full(long n,refvector<TN> &vals):
    _vals_r(_vals)
  {
    _rows=_cols=n; 
    _vals=vals;
    _size=n*(n-1)/2;
#ifdef DEBUG
    if (_size!=_vals->size()) throw domain_error("inconsistent sizes mat_asym_full(long n, const vector<TN> &vals)");
#endif
  }

  //! Constructor to read a stored matrix from a stream. Aimed at retrieving information from a binary save with mat_full::operator>>().
  template <class TN> mat_asym_full<TN>::mat_asym_full(istream& IN):
    _vals_r(_vals)
  {
    long rs,j;
#ifdef DEBUG
    const char* string("Full asymmetric matrix:\n\x0");
    char a;
    long i=0;
    IN.read((char*) &a,sizeof(char));
    while(string[i]==a && string[i]!='\n') 
      {
	i++;
	IN.read((char*) &a,sizeof(char));
      }
    if(string[i]!='\n')
      throw domain_error("This is not a full asymmetric matrix");
#else
    IN.seekg(24,ios::cur);
#endif
    
    IN.read((char*) &rs,sizeof(long));
    IN.read((char*) &_cols,sizeof(long));
#ifdef DEBUG
    if (rs!=_cols*(_cols+1)/2*sizeof(TN)+sizeof(long))
      throw domain_error("Dimension of Matrix and saved data do not agree in mat_asym_full(istream& IN)");
#endif
    _rows=_cols;
    _size=_cols*(_cols+1)/2;
    refvector<TN> M(_size);
    
    for(j=0;j<M.dim();j++)
      IN.read((char*) &(M[j]),sizeof(TN));
    _vals=M;
  }

  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_full<TN>::mat_asym_full(const mat_full<TN>& A):_vals_r(_vals)
  {
    _rows=_cols=A.cols();
    _size=_rows*(_rows-1)/2;
    _vals.resize(_size);
    long i,j;
    for(i=1;i<_cols;i++)
      for(j=0;j<i;j++)
        _vals[i*(i-1)/2+j]=A[i][j];
    for(i=1;i<_cols;i++)
      for(j=0;j<i;j++)
        _vals[i*(i-1)/2+j]-=A[j][i];
    _vals*=(TN) 0.5;    
  }
  
  // Operators
  
  //! Output an asymmetric, full matrix in binary format into a stream. Aimed at file storage.
  template <class TN> void mat_asym_full<TN>::operator>>(ostream& OUT) const
    /*!
      The default is to output a full (column) matrix.
    */
  {
    OUT << "Full asymmetric matrix:\n";
    long i;
    i=_cols*(_cols+1)/2*sizeof(TN)+sizeof(long);
    OUT.write((char*) &i,sizeof(long));
    OUT.write((char*) &_cols,sizeof(long));
    OUT.write((const char*) &(_vals[0]),i-sizeof(long));
  }


    //! Assignment operator
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::operator=(mat_asym_full<TN>& a)
  {
    _rows=a._rows;
    _cols=a._cols;
    _size=a._size;
    _vals=a._vals_r;
    return *this;
  }
  //! Assignment operator
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::operator=(const mat_asym_full<TN>& a)
  {
    _rows=a._rows;
    _cols=a._cols;
    _size=a._size;
    _vals=a._vals_r;
    return *this;
  }

  /*
  //! Access operator which checks for integrity in debug mode.
  template <class TN> const TN& mat_asym_full<TN>::operator()(const long x, const long y) const
  {
#ifdef DEBUG
    if( 0<=x && x <_cols && 0<=y && y<_rows)
      {
#endif
	if(x>y)
	  return _vals[x*(x+1)/2+y];
	else
	  return -_vals[y*(y+1)/2+x];
#ifdef DEBUG
      }
    else 
      {
	char v[100];
	snprintf(v,100,"illegal access to element in full matrix in const TN& mat_asym_full::operator()(const long x, const long y) const (%li,%li)",x,y);
	throw domain_error(v);
      }
#endif
  }
  */
  //! Access operator which checks for integrity in debug mode.  
  template <class TN> const TN& mat_asym_full<TN>::operator()(const long x, const long y) const
  {
#ifdef DEBUG
    if( 0<=x && x<_cols && 0<=y && y<_rows)
      {
#endif
	if(x>y)
	  return _vals[x*(x-1)/2+y];
	else if(x<y)
	  return -_vals[y*(y-1)/2+x];
        else
          return (TN) 0.0;
#ifdef DEBUG
      }
    else
      {
	char v[100];
	snprintf(v,100,"illegal access to element in full matrix in const TN& mat_asym_full::operator()(const long x, const long y) const (%li,%li)",x,y);
	throw domain_error(v);
      }
#endif
  }

  //! Direct access operator which checks for integrity in debug mode.
  template <class TN> const TN& mat_asym_full<TN>::operator[](const long x) const
  {
#ifdef DEBUG
    if( 0<=x && (unsigned) x <_vals->size())
#endif
      return _vals[x];
#ifdef DEBUG
    else
      throw domain_error("illegal access to element in full matrix in const TN& mat_asym_full::operator[](const long x) const");
#endif
  }

  //! Direct access operator which checks for integrity in debug mode.  
  template <class TN> TN& mat_asym_full<TN>::operator[](const long x)
  {
#ifdef DEBUG
    if( 0<=x && x<_vals.size())
#endif
      return _vals[x];
#ifdef DEBUG
    else
      throw domain_error( "illegal access to element in full matrix  in TN& mat_asym_full::operator[](const long x)");
#endif
  }

  // Normal Member functions

  //! Multiply by a full matrix.
  template <class TN> mat_full<TN>& mat_asym_full<TN>::multiply(const mat_full<TN>& a, mat_full<TN>& r) const
  {
#ifdef DEBUG
    if(a.cols() !=_rows)
      throw domain_error("Incompatible dimensions in mat_asym_full<TN> multiply(mat_full<TN> a, mat_full r)");
    if( r.cols()!=a.cols() || r.rows()!=_cols)
      throw domain_error("Incompatible dimensions for output in mat_asym_full<TN> multiply(mat_full<TN> a, mat_full r)");
#endif
    long i;
    for(i=0;i<a.cols();i++) multiply(a[i],r[i]);
    return r;
  }

  //! Multiply by a full matrix.
  template <class TN> mat_full<TN> mat_asym_full<TN>::operator*(const mat_full<TN>& a) const
  {
#ifdef DEBUG
    if(a.cols() !=_rows)
      throw domain_error("Incompatible dimensions in mat_asym_full<TN> operator*(mat_full<TN> a)");
#endif
    mat_full<TN> r(a.cols(), _rows);
    return multiply(a,r);
  }
  
  //! Multiply by a number back into the matrix.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::operator*=(TN a)
  {
    _vals*=a;
    return (*this);
  }
  
  //! Multiply by a number.
  template <class TN> mat_asym_full<TN> mat_asym_full<TN>::operator*(TN a) const
  {
    mat_asym_full<TN> r(_cols);
    r._vals=_vals*a;
    return r;
  }
  
  //! Multiply an asymmetric full matrix with a full vector.
  template <class TN> refvector<TN>& mat_asym_full<TN>::multiply(const refvector<TN>& v, refvector<TN>& r) const
  {
#ifdef DEBUG
    if(v.dim()!=_rows)
      throw("Incompatible dimensions friend vector<TN> operator*(const mat_asym_full& A, const vector<TN>& v)");
    if(r.dim()!=_rows)
      throw("Incompatible dimensions friend vector<TN> operator*(const mat_asym_full& A, const vector<TN>& v)");
#endif
    long i,j;
    for(i=0;i<r.dim();i++) r[i]=(TN) 0;
    // We will split A into a lower and an upper matrix.
    // Lower matrix
    for(i=1;i<_rows;i++)
      {
	for(j=0; j<i; j++) r[i]-=_vals[i*(i-1)/2+j]*v[j];
      }
    // Upper matrix
    for(i=1;i<_rows;i++)
      for(j=0;j<i; j++) r[j]+=_vals[i*(i-1)/2+j]*v[i];
    return r;
  }
  
  //! Multiply an asymmetric full matrix with a full vector.
  template <class TN> refvector<TN> mat_asym_full<TN>::operator*(const refvector<TN>& v) const
  {
#ifdef DEBUG
    if(v.dim()!=_rows)
      {
	throw("Incompatible dimensions refvector<TN> mat_asym_full::operator*(const refvector<TN>& v)");
      }
#endif
    refvector<TN> r(_rows);
    
    return multiply(v,r);
  }
  
  //! Multiply an asymmetric full matrix with a sparse vector.\todo clean up sparse matrix to ensure correct behavior for all cases.
  template <class TN> sparse_vector<TN>& mat_asym_full<TN>::multiply(const sparse_vector<TN>& v, sparse_vector<TN>& r) const
  {
#ifdef DEBUG
    if(v.dim()!=_rows)
      throw("Incompatible dimensions refvector<TN> mat_asym_full<TN>::multiply(const sparse_vector<TN>&, refvector<TN>& v)");
    if(r.dim()!=_rows)
      throw("Incompatible dimensions refvector<TN> mat_asym_full<TN>::multiply(const sparse_vector<TN>&, refvector<TN>& v)");
    try {
#endif
    long i,j;

    // We will split A into a lower and an upper matrix.
    // ut

    r.zero();
    for(i=0;i<v.size();i++)
      {
	long id=v.index(i);
	for(j=0;j<id;j++)
	  r(j)+=(*this)[id*(id-1)/2+j]*v[i];
	r.prune();
      }
    
    // lt
    
    for(i=1;i<_rows;i++)
      {
	for(j=0;j<v.size() && v.index(j)<i;j++)
	  r(i)-=v[j]*(*this)[i*(i-1)/2+v.index(j)];
	r.prune();
      }
    
    return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from mat_asym_full::multiply(const sparse_vector&, refvector)");
    }
#endif
  }
  //! Multiply an asymmetric full matrix with a sparse vector.
  template <class TN> refvector<TN>& mat_asym_full<TN>::multiply(const sparse_vector<TN>& v, refvector<TN>& r) const
  {
#ifdef DEBUG
    if(v.dim()!=_rows)
      throw("Incompatible dimensions refvector<TN> mat_asym_full<TN>::multiply(const sparse_vector<TN>&, refvector<TN>& v)");
    if(r.dim()!=_rows)
      throw("Incompatible dimensions refvector<TN> mat_asym_full<TN>::multiply(const sparse_vector<TN>&, refvector<TN>& v)");
    try {
#endif
    long i,j;
    for(i=0;i<r.dim();i++) r[i]=(TN) 0;
    // We will split A into a lower and an upper matrix.
    // ut
    
    for(i=0;i<v.size();i++)
      {
	long id=v.index(i);
	for(j=0;j<id;j++)
	  r[j]+=(*this)[id*(id-1)/2+j]*v[i];
      }
    
    // lt
    
    for(i=1;i<_rows;i++)
      for(j=0;j<v.size() && v.index(j)<i;j++)
	r[i]-=v[j]*(*this)[i*(i-1)/2+v.index(j)];
    
    return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("called from mat_asym_full::multiply(const sparse_vector&, refvector)");
    }
#endif
  }
  
  //! Multiply an asymmetric full matrix with a sparse vector.
  template <class TN> refvector<TN> mat_asym_full<TN>::operator*(const sparse_vector<TN>& v) const
  {
#ifdef DEBUG
    if(v.dim()!=_rows)
      {
	throw("Incompatible dimensions refvector<TN> mat_asym_full::operator*(const sparse_vector<TN>& v)");
      }
#endif
    refvector<TN> r(_rows);
    
    return multiply(v,r);
  }
  
  //! Add an antisymmetric full matrix to an existing one.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::operator+=(const mat_asym_full<TN>& a)
  { 
#ifdef DEBUG
    if(a._cols!=_cols)
      throw domain_error("Incompatible dimensions in mat_asym_full<TN> mat_asym_full::operator+=(const mat_asym_full<TN> a) const");
#endif
    long i;
    for(i=0;i<_cols*(_cols-1)/2;i++)
      _vals[i]+=a._vals[i];
    return (*this);
  }

  //! Add an antisymmetric full matrix to an existing one.
  template <class TN> mat_asym_full<TN> mat_asym_full<TN>::operator+(const mat_asym_full<TN>& a) const
  {
#ifdef DEBUG
    if(a._cols!=_cols)
      throw domain_error("Incompatible dimensions in mat_asym_full<TN> mat_asym_full::operator+(const mat_asym_full<TN> a) const");
#endif
    mat_asym_full<TN> r(*this);
    r+=a;
    return r;
  }
  
  //! Subtract an antisymmetric full matrix to an existing one.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::operator-=(const mat_asym_full<TN>& a)
  {
#ifdef DEBUG
    if(a._cols!=_cols)
      throw domain_error("Incompatible dimensions in mat_asym_full<TN> mat_asym_full::operator-=(const mat_asym_full<TN> a) const");
#endif
    long i;
    for(i=0;i<_cols*(_cols-1)/2;i++)
      _vals[i]-=a._vals[i];
    return (*this);
  }
  
  //! Extract a column from an antisymmetric matyrix.
  template <class TN> refvector<TN> mat_asym_full<TN>::extract_col(const long x) const
  {
#ifdef DEBUG
    if(x<0 || x>=_cols)
      throw domain_error("Incompatible dimensions in refvector<TN> mat_asym_full::extract_col(const long x) const");
#endif
    refvector<TN> r(_rows);
    long i;
    const long j=_rows-x-1;
    for(i=0;i<x;i++) r[i]=_vals[x*(x-1)/2+i];
    for(i=x+1;i<_rows; i++) r[i]=-_vals[i*(i-1)/2+x];
    return r;
  }
  
  //! Mulitply an antisymmetric full matrix with an antisymmetric full matrix.
  template <class TN> mat_full<TN>& mat_asym_full<TN>::multiply(const mat_asym_full<TN> B,mat_full<TN>& r) const
  {
#ifdef DEBUG
    if(B._cols != _cols)
      throw domain_error(" Incompatible dimensions in mat_full<TN> mat_asym_full::multiply(const mat_asym_full<TN> B) const, mat_full r");
    if(r.cols()!=_cols || r.rows()!=_rows)
      throw domain_error(" Incompatible dimensions for output in mat_full<TN> mat_asym_full::multiply(const mat_asym_full<TN> B,mat_full r) const");
#endif
    
    // Lower triangle
    long i, j, k;
    r.zero();
    for(i=2;i<B.cols();i++)
      for(j=1;j<i;j++)
	for(k=0;k<j;k++)
	  r[i][k]+=_vals[j*(j-1)/2+k]*B[i*(i-1)/2+j];
    for(i=1;i<B.cols();i++)
      for(j=0;j<i;j++)
	for(k=j+1;k<_cols;k++)
	  r[i][k]-=_vals[k*(k-1)/2+j]*B[i*(i-1)/2+j];

    for(i=0;i<B.cols();i++)
      for(j=i+1;j<B.cols();j++)
	for(k=0;k<j;k++)
	  r[i][k]-=_vals[j*(j-1)/2+k]*B[j*(j-1)/2+i];
    for(i=0;i<B.cols();i++)
      for(j=i+1;j<B.cols();j++)
	for(k=j+1;k<_cols;k++)
	  r[i][k]+=_vals[k*(k-1)/2+j]*B[j*(j-1)/2+i];
    /*
    r[0].zero();
    for(i=1; i<B.cols(); i++)
      {
        r[i]=(*this).extract_col(0)*(-B[i*(i-1)/2]);
	for(j=1; j<i; j++) r[i]-=(*this).extract_col(j)*B[i*(i-1)/2+j];
      }
    // Upper triangle
    for (i=1; i< B.cols(); i++)
      for(j=0; j<i; j++)
      r[j]+=(*this).extract_col(i)*B[i*(i-1)/2+j];*/
    return r;
  }
  
  //! Mulitply an antisymmetric full matrix with an antisymmetric full matrix.
  template <class TN> mat_full<TN> mat_asym_full<TN>::operator*(const mat_asym_full<TN>& B) const
  {
#ifdef DEBUG
    if(B._cols != _cols)
      throw domain_error(" Incompatible dimensions in mat_full<TN> mat_asym_full::operator*(const mat_asym_full<TN> B) const");
#endif
    mat_full<TN> r(_cols,_rows);
    return multiply(B,r);
  }
  
  //! Mulitply an antisymmetric full matrix with a symmetric full matrix.
  template <class TN> mat_full<TN> mat_asym_full<TN>::operator*(const mat_sym_full<TN>& B) const
  {
#ifdef DEBUG
    if(B.rows() != _cols)
      throw domain_error(" Incompatible dimensions in mat_full<TN> mat_asym_full::operator*(const mat_sym_full<TN> B) const");
#endif
    mat_full<TN> r(B.cols(),_rows);
    
    // Lower triangle
    long i, j;
    for(i=0; i<B.cols(); i++)
      {
	r[i]=(*this).extract_col(0)*B[i*(i-1)/2];
	for(j=1; j<=i; j++) r[i]+=(*this).extract_col(j)*B[i*(i-1)/2+j];
      }
    // Upper triangle
    for (i=0; i< B.cols(); i++)
      for(j=0; j<i; j++)
	r[j]+=(*this).extract_col(i)*B[i*(i-1)/2+j];
    return r;
  }



  //! Multiply two sparse matrices.
  template <class TN> mat_sparse<TN>& mat_asym_full<TN>::multiply(const  mat_sym_sparse<TN>& b, mat_sparse<TN>& r) const
  {
#ifdef DEBUG
    if( matrix<TN>::_cols != b.rows())
      {
	throw domain_error( "Incompatible dimensions in mat_sparse mat_asym_full::operator*(const mat_sym_sparse& a, const mat_sparse& b)");
      }
    try {
#endif
      refvector<sparse_vector<TN> > rv(b.size());
      
      long i,j,k;
      for(i=0; i<b.size();i++)
	{
	  sparse_vector<TN> v(r.rows());
	  multiply(b[i],v);
	  rv[i]=v;
	}
      
      mat_sparse<TN> r2(b.cols(),matrix<TN>::_rows,b._scols.vec(),rv);
      // lt lt
      
      for(i=0;i<b.size();i++)
	{
	  long id=b.index(i);
	  for(j=0;j<b[i].size() && b[i].index(j)<id; j++)
	    {
	      long jd=b[i].index(j);
	      for(k=id+1;k<_rows;k++)
		r2.add(jd,k,-(*this)[k*(k+1)/2+id]*b[i][j]);
	    }
	}
      r2.prune();

      // ut lt
      
      for(i=0;i<b.size();i++)
	{
	  long id=b.index(i);
	  for(j=0;j<b[i].size() && b[i].index(j)<id;j++)
	    {
	      long jd=b[i].index(j);
	      for(k=0;k<id;k++)
		r2.add(jd,k,(*this)[id*(id+1)/2+k]*b[i][j]);
	    }
	}
      
      r2.prune();
      r=r2;
      return(r);
#ifdef DEBUG
    } catch(domain_error e) { 
      cerr << e.what() << endl;
      throw domain_error(" called from mat_sparse<TN> mat_sym_full::operator*(const  mat_sym_sparse<TN>& b, mat_sparse&) const");
    }
#endif
  }

  
  //! Mulitply an antisymmetric full matrix with a symmetric full matrix.
  template <class TN> mat_full<TN>& mat_asym_full<TN>::multiply(const mat_sym_sparse<TN>& B, mat_full<TN>& r) const
  {
#ifdef DEBUG
    if(B.rows() != _cols)
      throw domain_error(" Incompatible dimensions for B in mat_full<TN> mat_asym_full::multiply(const mat_sym_sparse<TN>& B,mat_full<TN>& r) const");
    if(_rows != r.rows() || B.cols() != r.cols())
      throw domain_error(" Incompatible dimensions for r in mat_full<TN> mat_asym_full::multiply(const mat_sym_sparse<TN>& B,mat_full<TN>& r) const");
    try {
#endif
      
      long i,j,k;
      
      for(i=0;i<r.cols();i++)
	for(j=0;j<r.rows();j++) r[i][j]=(TN) 0;
      // This takes care of A* ut
      for(k=0;k<B.size();k++)
	r[B.index(k)]=(*this).multiply(B[k],r[B.index(k)]);
      /*
      // upper triangle  upper triangle
      
      for(k=0;k<B.size();k++)
	{
	  long ki=B.index(k);
	  for(i=0;i<=ki;i++)
	    for(j=0;j<B[k].size() && B[k].index(j) <i;j++)
	      r[ki][B[k].index(j)]+=(*this)[i*(i-1)/2+B[k].index(j)]*B[k][j];
	}
      
	// lt ut 
      
      for(k=0;k<B.size();k++)
	{
	  long ki=B.index(k);
	  for(i=0;i<=ki;i++)
	    for(l=0;l<B[k].size() && B[k].index(l)<i;l++)
	      r[ki][i]-=(*this)[i*(i-1)/2+B[k].index(l)]*B[k][l];
	  for(i=ki+1;i<_cols;i++)
	    for(l=0;l<B[k].size() && B[k].index(l)<ki;l++)
	      r[ki][i]-=(*this)[i*(i-1)/2+B[k].index(l)]*B[k][l];
	}
      */
      // lt lt
      
      for(i=0;i<B.size();i++)
	{
	  long id=B.index(i);
	  for(j=0;j<B[i].size() && B[i].index(j)<id; j++)
	    {
	      long jd=B[i].index(j);
	      for(k=id+1;k<_rows;k++)
		r[jd][k]-=(*this)[k*(k+1)/2+id]*B[i][j];
	    }
	}

      // ut lt
      
      for(i=0;i<B.size();i++)
	{
	  long id=B.index(i);
	  for(j=0;j<B[i].size() && B[i].index(j)<id;j++)
	    {
	      long jd=B[i].index(j);
	      for(k=0;k<id;k++)
		r[jd][k]+=(*this)[id*(id+1)/2+k]*B[i][j];
	    }
	}
      
      return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error("Called from mat_asym_full<TN>::multiply(const mat_sym_sparse<TN>&, mat_full<TN>&)");
    }
#endif
  }
  
  //! Mulitply an antisymmetric full matrix with a symmetric sparse matrix.
  template <class TN> mat_full<TN> mat_asym_full<TN>::operator*(const mat_sym_sparse<TN>& B) const
  {
#ifdef DEBUG
    if(B.cols() != _cols)
      throw domain_error(" Incompatible dimensions in mat_full<TN> mat_asym_full::operator*(const mat_sym_sparse<TN>& B) const");
#endif
    mat_full<TN> r(_cols,_rows);
    return multiply(B,r);
  }
    
  //! Get the product \f$AA^\ast\f$.\test
  template <class TN> mat_sym_full<TN> mat_asym_full<TN>::aa_t() const
  {
    mat_sym_full<TN> r(_cols);
    long i,k,j;
    for(i=0;i<_cols;i++)
      for(k=0;k<=i;k++)
	for(j=0;j<k; j++) r[i*(i-1)/2+k]+=_vals[i*(i-1)/2+j]*_vals[k*(k+1)/2+j];
    
    for(i=0;i<_cols;i++)
      for(j=0;j<i; j++)
	for(k=0;k<j;k++)
	  r[i*(i-1)/2+k]-=_vals[i*(i-1)/2+j]*_vals[j*(j+1)/2+k];
    
    for(j=0;j<_cols;j++)
      for(i=0;i<j;i++)
	for(k=0;k<=i;k++)
	  r[i*(i-1)/2+k]+=_vals[j*(j+1)/2+i]*_vals[j*(j+1)/2+k];
    
    return r;
  }
  
  template <class TN> void mat_asym_full<TN>::add(const long x, const long y, TN a)
  {
#ifdef DEBUG
    if( 0<=x && x<_cols && 0<=y && y<_rows)
      {
#endif
	if(x>y)
	  _vals[x*(x+1)/2+y]+=a;
	else
	  _vals[y*(y+1)/2+x]-=a;
#ifdef DEBUG
      }
    else
      {
	char v[100];
	snprintf(v,100,"illegal access to element in full, asymmetric matrix in const TN& mat_asym_full::add(const long x, const long y, TN a) (%li,%li)",x,y);
	throw domain_error(v);
      }
#endif
  }

  //! Compute the density of non-zeros.
  template<class T> long mat_asym_full<T>::nonzeros() const
  {
    const double r=sqrt(_vals*_vals);
    long count=0;
    long i;
    for(i=0;i<_vals.size();i++)
      if(fabs(_vals[i])>1e-16*r)
	count++;
    return count;
  }

  //! Simple display of matrix.
  template <class TN> bool mat_asym_full<TN>::display(ostream& out) const
  {
    long i,j,l;
    for(i=0,l=0; i<_rows; i++)
      {
	for(j=0; j<i; j++,l++) out << _vals[l] <<" ";
	out << endl;
      }
    return true;
  }

  //! Assignment operation for compatibility with mat_sym_sparse.
  template <class TN> void mat_asym_full<TN>::set(const long x, const long y, TN value)
  {
    
#ifdef DEBUG
    if(!( 0<=x && x<_cols && 0<=y && y<_rows))
      throw domain_error( "illegal access to element in full matrix  in TN& mat_asym_full::set()(const long x, const long y, TN value)");
#endif
    
    if(x>y)
      _vals[x*(x+1)/2+y]=value;
    else
      _vals[y*(y+1)/2+x]=-value;
    
  }

  //! Copy matrix with depth 1.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::copy(const mat_asym_full<TN> a)
  {
    _vals.copy(a._vals);
    _rows=a._rows;
    _cols=a._cols;
    return *this;
  }
  
  //! Copy matrix with depth i.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::copy(const mat_asym_full<TN> a, const long i)
  {
    _vals.copy(a._vals,i);
    _rows=a._rows;
    _cols=a._cols;
    return *this;
  }
  
  //! Reset matrix releasing all associated memory.
  template <class TN> void mat_asym_full<TN>::clear()
  {
    _rows=_cols=_size=0;
    refvector<TN> a;
    _vals=a;
  }

  //! Set matrix to 0, i.e., all elements.
  template <class TN> mat_asym_full<TN>&  mat_asym_full<TN>::zero()
  {
    _vals.zero();
    return *this;
  }


  //! Returns the size of the vector containing the matrix data.
  template <class TN> long mat_asym_full<TN>::size() const { return _vals.dim();}

  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::utu(const mat_asym_full<TN>& a, mat_asym_full<TN>& R, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=R.cols())
      throw domain_error("Incompatible dimensions for output in mat_sym_full::utu(const mat_sym_full<TN>& a)");
    if(a.cols()!=I.cols() || I.rows() !=_cols)
      throw domain_error("Incompatible dimensions for intermediate in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    multiply(a,I);
    long i,j;
    refvector<TN> v(_rows);
    for(i=0;i<R.cols();i++)
      {
	a.multiply(I[i],v);
	for(j=0;j<=i;j++)
	  R[i*(i-1)/2+j]=v[j];
      }
    return R;
  }

  //! This an antisymmetric transform \f$ SAS\f$.
  template <class TN> mat_asym_full<TN> mat_asym_full<TN>::utu(const mat_asym_full<TN>& a, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_asym_full::utu(const mat_asym_full<TN>& a)");
    if(a.cols()!=I.cols() || I.rows() !=_cols)
      throw domain_error("Incompatible dimensions for intermediate in mat_asym_full::utu(const mat_asym_full<TN>& a)");
#endif
    mat_asym_full<TN> R(_cols);
    multiply(a,I);
    long i,j;
    refvector<TN> v(_rows);
    for(i=0;i<R.cols();i++)
      {
	a.multiply(I[i],v);
	for(j=0;j<=i;j++)
	  R[i*(i-1)/2+j]=v[j];
      }
    return R;
  }
  
  //! This an antisymmetric transform \f$ SAS\f$.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::utu(const mat_asym_full<TN>& a, mat_asym_full<TN>& R) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_asym_full::utu(const mat_asym_full<TN>& a)");
    if(a.cols()!=R.cols())
      throw domain_error("Incompatible dimensions for output in mat_asym_full::utu(const mat_asym_full<TN>& a)");
#endif
    mat_full<TN> I(a.cols(),_rows);
    return utu(a,R,I);
  }
  
  //! This a symmetric transform \f$ SAS\f$.
  template <class TN> mat_asym_full<TN> mat_asym_full<TN>::utu(const mat_asym_full<TN>& a) const
  {
#ifdef DEBUG
    if(a.rows()!=_cols)
      throw domain_error("Incompatible dimensions in mat_sym_full::utu(const mat_sym_full<TN>& a)");
#endif
    mat_full<TN> R(a.cols(),_rows);
    return utu(a,R);
  }
  
  //! This is the unitary transform \f$U^\ast AU\f$.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::utu(const mat_full<TN>& U, mat_asym_full<TN>& R) const
  {
#ifdef DEBUG
    if(_cols!= U.rows() || _rows != U.rows())
      throw domain_error("Incompatble dimensions in symmetric::utu(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
    if(R.cols()!=U.cols())
      throw domain_error("Incompatble dimensions for output in symmetric::utu(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
#endif
    const long n=U.cols();
    
    /* This is also order n^3*/
    long i,j;
    refvector<TN> r(_rows);
    for(i=0;i<n;i++)
      {
	multiply(U[i],r);
	for(j=0;j<=i;j++) R[i*(i-1)/2+j]=U[j]*r;
      }
    return R;
  }
  
  //! This is the unitary transform U^tAU.
  template <class TN> mat_asym_full<TN> mat_asym_full<TN>::utu(const mat_full<TN>& U) const
  {
    mat_asym_full<TN> R(U.cols());
    utu(U,R);
    return R;
  }
  
  //! This is the unitary transform \f$UAU^\ast\f$.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::uut(const mat_full<TN>& U, mat_asym_full<TN>& R, mat_full<TN>& I) const
  {
#ifdef DEBUG
    if(_cols!= U.rows() || _rows != U.rows())
      throw domain_error("Incompatble dimensions in symmetric::uut(const mat_sym_full<TN>& U, mat_sym_full<TN>& R) const");
    if(R.rows()!=U.rows())
      throw domain_error("Incompatble dimensions for R in symmetric::uut(const mat_sym_full<TN>& U, mat_sym_full<TN>& R, mat_full<TN>& I) const");
    if(I.cols() !=_cols || I.rows() !=U.rows())
      throw domain_error("Incompatble dimensions for I in symmetric::uut(const mat_sym_full<TN>& U, mat_sym_full<TN>& R, mat_full<TN>& I) const");
#endif
    U.multiply((*this),I);
    long i,j,k;
    if(I.cols()>0)
      for(i=0;i<U.rows();i++)
	for(j=0;j<i;j++)
	  R[i*(i-1)/2+j]=U[0][i]*I[0][j];
    for(k=1;k<I.cols();k++)
      for(i=0;i<U.rows();i++)
	for(j=0;j<i;j++)
	  R[i*(i-1)/2+j]+=U[k][i]*I[k][j];
    return R;
  }
  
  //! This is the unitary transform \f$UAU^\ast\f$.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::uut(const mat_full<TN>& U,mat_asym_full<TN>& R) const
  {
    mat_full<TN> I(_cols,U.rows());
    return uut(U,R,I);
  }
  
  //! This is the unitary transform \f$UAU^\ast\f$.
  template <class TN> mat_asym_full<TN> mat_asym_full<TN>::uut(const mat_full<TN>& U) const
  {
    mat_asym_full<TN> R(U.rows());
    mat_full<TN> I(_cols,U.rows());
    return uut(U,R,I);
  }
  
  
  //! Produce the necessary \f$ij^\ast-ji^\ast\f$.
  template <class TN> void mat_asym_full<TN>::gen_mat(const refvector<TN>& a, const refvector<TN>& b)
    /*!
      the result is \f$+=ab^\ast - ba^\ast\f$. 
    */
  {
    long i,j;

#ifdef DEBUG
    if(a.dim() != b.dim()) throw domain_error("mat_sym_full::gen_mat error in dimensions");
#endif
    for(i=0; i<a.dim(); i++)
      for(j=0; j<i; j++)
	_vals[i*(i-1)/2+j]-=a[i]*b[j]; // -ba^\ast
    for(i=0; i<b.dim(); i++)
      for(j=0; j<i; j++)
	_vals[i*(i-1)/2+j]+=b[i]*a[j]; // ab^\ast
  }
  
  //! This routine produces the diagonal, sub/superdiagonal and transformation matrix for the Householder transformation of a full anti-symmetric matrix.
  template <class TN> mat_full<TN> mat_asym_full<TN>::householder(refvector<TN>& subdiagonal)
    /*!
      The aspired transformation is a multiplication by \f$(I-vv^\ast)\f$ where v is
      \f$(\pm e_1\|a_{12}\| + a_{12})/\sqrt{\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2}\f$.
      Notice that we begin counting at 0.
      \n The matrix is split up according to \f$A=\left(\begin{array}{cc} a_{11} & a_{12}^\ast \\ a_{12} & A_{22} \end{array}\right)\f$
      We use preconditioning such that the matrix is ordered with lowest off-diagonal value in the low right=hand corner.
      \f$\left(\begin{array}{ccccc} a_{11} & \cdots & & \\ & \ddots & & \\ & & \ddots & 0 \\ & & & a_{nn}\end{array}\right)\f$
    */
  {
#ifdef DEBUG
    try 
      {
#endif
	if (subdiagonal->size()==1) {
	  subdiagonal[0]=(TN) 0;
	  mat_full<TN> T(1,1);
	  T(0,0)=1;
	  return T;
	}
	
	refvector<refvector<TN> > T(_cols);
	
	long i,j;
	TN absval;     // absolute value of current iteration generator
	TN absval2;    // square of the absolute value
	refvector<TN> u2;
	
	TN sd; // value of the current subdiagonal
	
	// The aspired transformation is a multiplication by (I-vv^\ast) where v is
	// (pm e_1\|a_{12}\| + a_{12})/\sqrt{\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2}
	// notice that we begin counting at 0.
	
	for(i=_cols-1; i> 1; i--)
	  {
	    
	    refvector<TN> u(_rows);
	    
	    // The following reduces accesses to the values.
	    
	    for(j=0;j<i;j++)
	      u[j]=-_vals[i*(i-1)/2+j]; // Let u be the ith column above the diagonal u = a_{12}
	    for(j=i;j<_rows;j++) u[j]=0;
	    sd=u[i-1];
	    
	    // The transformation vector is zero in the first element.
	    
	    absval2=u*u;            // absval2 = \|a_{12}\|^2	    
	    absval=sqrt(absval2);   // absval = \|a_{12}\|
	    if (absval!=0) {     // Precautions for a_{12} \approx 0
	      
	      if(sd>0) subdiagonal[i]=absval;
	      else subdiagonal[i]=0-absval;
	      
	      /*
		Store the transformation generator u.
		The denominator will not be included in u.
		Instead, to avoid taking the root and remultiplying,
		the contributions will be added explicitly
	      */
	      
	      absval2=(TN) 1/(absval2+subdiagonal[i]*sd);// \pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2
	      //	    absval=(TN) 1/absval;
	      
	      u[i-1]+=subdiagonal[i];   // u = a_{12}\pm \|a_{12}\|e_1
	      u*=sqrt(absval2);         // u = (a_{12}\pm a_{12,1}e_1)/(\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2)
	      subdiagonal[i]*=(-1);     // = \mp \|a_{12}\| = a_{12}-uu^\ast a_{12}
	      
	      // Now we do the transformation for the remainder of A
	      
	      /* u2=Au */
	      
	      u2=(*this)*u;
	      //	    u2*=(double) 2;
	      
	      /*
		A' = A + uu2^T - u2u^T
		   = A + 2uu^tA^t
		       - 2Auu^t
		   = A - 2uu^TA - 2Auu^T (+ 4uu^TAuu^T=0)
	      */
	    
	      gen_mat(u,u2);
	      
	      //display();
	    }
	    else
	      for(j=0;j<=i;j++) u[j]=0;
	    
	    /* Store u in T */
	    
	    T[i]=u;
	  } // Matches For(i=col-1;i>1; i--)
	
	subdiagonal[1]=(*this)(1,0);
	
	// Generate transformation matrix.
	
	mat_full<TN> r(_cols,_rows);
	
	for(i=1;i<_cols;i++) subdiagonal[i]=(*this)(i,i-1);
	
	for(i=_cols-1; i>=0; i--) r[i][i]=1; // we start with the identity.
	
	refvector<TN> u;/*
			  for( i=2; i<T->size(); i++)
			  {
			  //T[i].display();
			  cout << " " << T[i]*T[i] << endl;
			  }*/
	
	for(i=2; i<_cols; i++)
	  {
	    u.copy(T[i]);
	    u2=r*u;
	    for(j=0; j<u.size(); j++)
	      r[j]-=u2*u[j]; // No danger here because there are no empty columns.
	  }
	return r;
#ifdef DEBUG
      } catch(domain_error e) {
	cerr << e.what() << endl;
	throw domain_error("called from mat_sym_full::householder");
      }
#endif
  }

  //! Prune the matrix of negligible values.
  template <class TN> mat_asym_full<TN>& mat_asym_full<TN>::prune(const TN tol)
  {
    long i;
    for(i=0;i<_vals.dim();i++)
      if(_vals[i]<=tol && _vals[i] >= -tol) _vals[i]=(TN) 0;
    return *this;
  }
  
} // linear_algebra::

#include "refcount.h"
#include "sparse_vector.h"
#include "mat_full.h"
#include "mat_sym_full.h"
#include "mat_sym_sparse.h"
#endif
/*! @}*/
