/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{ */
/*! @file symmetric.h
  @brief Describes fundamentals of symmetric matrices.

*/
#ifndef _SYMMETRIC_H
#define _SYMMETRIC_H
#include <vector>
#include <iostream>
#include "square.h"
#include "symmetric.decl"

//! Linear algebra Namespace
namespace linear_algebra
{

  //! The symmetric class contains all symmetric matrices.
  template <class TN> matrix<TN>& symmetric<TN>::transpose(matrix<TN>& r) const { r=(*this); return r;}
  template <class TN> matrix<TN>& symmetric<TN>::transpose(matrix<TN>& r) { r=(*this); return r;}
  template <class TN> symmetric<TN>& symmetric<TN>::transpose(symmetric<TN>& r) const { r=(*this); return r;}
  template <class TN> symmetric<TN>& symmetric<TN>::transpose(symmetric<TN>& r) { r=(*this); return r;}
  template <class TN> symmetric<TN>& symmetric<TN>::transpose() const { return (*this);}
  template <class TN> symmetric<TN>& symmetric<TN>::transpose() { return (*this);}

  template <class TN> matrix<TN>& symmetric<TN>::copy(const matrix<TN>& a) { return copy(a); }
  template <class TN> matrix<TN>& symmetric<TN>::copy(const matrix<TN>& a, const long i) {return copy(a); }
  template <class TN> matrix<TN>& symmetric<TN>::copy(matrix<TN>& a, const long i) { return copy(a); }

}
#endif
/*! @}*/
