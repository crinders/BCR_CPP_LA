/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file sparse_vector_infty.h
  \brief Provide functionality for sparse vectors of infinite dimension.
*/
#ifndef _SPARSE_VECTOR_INFTY_H
#define _SPARSE_VECTOR_INFTY_H
#include <vector>
#include <stdexcept>
#include "sparse_vector_infty.decl"
#include "refcount.h"
#include "class_extensions.h"

namespace linear_algebra
{

  //! Default constructor. Dimension 0.
  template <class TN> sparse_vector_infty<TN>::sparse_vector_infty() {
    _size=0; 
    linear_algebra::zero(_prune_tol);
    refvector<long> i;
    refvector<TN> v;
    _index=i;
    _vals=v;
  }
  
  //! Constructs a sparse vector of infinite dimension with initialisation.
  template <class TN> sparse_vector_infty<TN>::sparse_vector_infty(const vector<long>& a, const vector<TN>& b)
    /*!
      \param a Vector indices.
      \param b Vector values.
    */
    : _size(a.size()), _index(a), _vals(b), _prune_tol(0)
    {
      if(a.size()!=b.size())
	{
	  throw domain_error("Construction of sparse_vector_infty due to different initialisation sizes failed. " );
	  exit(1);
	}
    }
  
  
  //! Constructs a sparse vector of infinite dimension with initialisation.
  template <class TN> sparse_vector_infty<TN>::sparse_vector_infty(const refvector<long>& a, const vector<TN>& b)
    /*!
      \param a Reference counted vector indices.
      \param b Vector values.
    */
    : _size(a.size()), _index(a), _vals(b), _prune_tol(0)
    {
      if(a.size()!=b.size())
	{
	  throw domain_error("Construction of sparse_vector_infty due to different initialisation sizes failed. " );
	  exit(1);
	}
    }
  
  //! Constructs a sparse vector of infinite dimension with initialisation.
  template <class TN> sparse_vector_infty<TN>::sparse_vector_infty(const refvector<long>& a, const refvector<TN>& b)
    /*!
      \param a Reference counted vector indices.
      \param b Reference counted ector values.
    */
    : _size(a.size()), _index(a), _vals(b), _prune_tol(0)
    {
      if(a.size()!=b.size())
	{
	  throw domain_error("Construction of sparse_vector_infty due to different initialisation sizes failed. " );
	  exit(1);
	}
    }
  
  //! Constructs a sparse vector of infinite dimension with initialisation.
  template <class TN> sparse_vector_infty<TN>::sparse_vector_infty(const vector<long>& a, const refvector<TN>& b)
    /*!
      \param a Vector indices.
      \param b Reference counted vector values.
    */
    : _size(b.size()), _index(a), _vals(b), _prune_tol(0)
    {
      if(a.size()!=b->size())
	{
	  throw domain_error("Construction of sparse_vector_infty due to different initialisation sizes failed. " );
	  exit(1);
	}
    }
  
  //! Copy constructor.
  template <class TN> sparse_vector_infty<TN>::sparse_vector_infty(const sparse_vector_infty& a) :
    _size(a._size), _prune_tol(a._prune_tol) {
    _index.copy(a._index);
    _vals.copy(a._vals);
  }
  
  //! Multiply by a sparse vector.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator*=(const sparse_vector_infty<TN>& a)
    /*!
      This is an elementwise multiplication which results in a new vector. \f$c_i=c_i\cdot a_i.\f$
    */
    {
      long ii=0;
      long iv=0;
      long ai=0;
      long av=0;
      while( ii<_index->size())
	{
	  while(  ai<a._index->size() && _index[ii]>a._index[ai]) {ai++; av++;}
	  if(ai<a._index->size() && a._index[ai] == _index[ii])
	    {
	      _vals[iv]*=a._vals[av];
	      ai++;
	      av++;
	      ii++;
	      iv++;
	    }
	  else
	    {
	      _index->erase(_index->begin()+ii);
	      _vals->erase(_vals->begin()+iv);
	    }
	}
      return *this;
    }
  
  //! Scalar product of sparse vectors.
  template <class TN> TN sparse_vector_infty<TN>::operator*(const sparse_vector_infty& a) const
    {
      long ii=0;
      long iv=0;
      long ai=0;
      long av=0;
      TN r;
      r=0;
      while( ii<_index->size())
	{
	  while(ai<a._index->size() && _index[ii]>a._index[ai]) {ai++; av++;}
	  if(ai<a._index->size() && a._index[ai] == _index[ii])
	    {
	      r+=_vals[iv]*a._vals[av];
	      av++;
	      ai++;
	    }
	  ii++;
	  iv++;
	}
      return r;
    }
  
  //! is (x) non zero
  template <class TN> bool sparse_vector_infty<TN>::is_nonzero(const long x, long &i) const
    {
      i=0;
      while(i<_index.size() && _index[i]<x) i++;
      if(i<_index.size() && _index[i]==x) return true;
      else return false;
    }
  
  //! Direct access operator.
  template <class TN> TN& sparse_vector_infty<TN>::operator[](const long x) {
#ifdef DEBUG
    if(x<0 || x>=_vals.dim()) throw domain_error(" Access error sparse_vector_infty::operator[](const long x)");
#endif
    return _vals[x];}
  
  //! Direct access operator.
  template <class TN> TN sparse_vector_infty<TN>::operator[](const long x) const {
#ifdef DEBIUG
    if(x<0 || x>=_vals.dim()) throw domain_error(" Access error sparse_vector_infty::operator[](const long x) const");
#endif
    return _vals[x];}
  
  // Direct access operator.
  //const TN& operator[](const long x) const {return _vals[x];}
  
  //! Access by element operator.
  template <class TN> TN sparse_vector_infty<TN>::operator()(const long x) const 
    {
      if(x>=_index.size()) return zero();
      long ii=0;
      long iv=0;
      
      while(ii<_index->size() && _index[ii]<x) {ii++; iv++;}
      if(ii<_index->size() && _index[ii]==x) return _vals[iv];
      return 0;
    }
  
  //! Access by element operator.
  template <class TN> TN& sparse_vector_infty<TN>::operator()(const long x)
    /*!
      One of the pitfalls of using this for assignments is that if _size is set incorrectly
      the throw argument will result in an SIGABORT if not caught.
    */
    {
      long ii=0;
      long iv=0;
      
      while(ii<_index.size() && _index[ii]<x) {ii++; iv++;}
      if(ii<_index.size() && _index[ii]==x) return _vals[iv];
      _index->insert(_index->begin()+ii,x);
      TN intermediate;
      linear_algebra::zero(intermediate);
      _vals->insert(_vals->begin()+iv,intermediate);
      return _vals[iv];
    }
  
  //! Add an element for consistency.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::add(const long x, TN z)
    /*!
      One of the pitfalls of using this for assignments is that if _size is set incorrectly
      the throw argument will result in an SIGABORT if not caught.
    */
    {
      long ii=0;
      long iv=0;
      
      while(ii<_index.size() && _index[ii]<x) {ii++; iv++;}
      if(ii<_index.size() && _index[ii]==x) _vals[iv]+=z;
      else
	{
	  _index->insert(_index->begin()+ii,x);
	  _vals->insert(_vals->begin()+iv,z);
	}
      return (*this);
    }
  
  //! Multiply an element for consistency.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::multiply(const long x, TN z)
    /*!
      One of the pitfalls of using this for assignments is that if _size is set incorrectly
      the throw argument will result in an SIGABORT if not caught.
    */
    {
      long ii=0;
      long iv=0;
      
      while(ii<_index.size() && _index[ii]<x) {ii++; iv++;}
      if(ii<_index.size() && _index[ii]==x) _vals[iv]*=z;
      return (*this);
    }
  
  //! Add an infinite sparse vector.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator+=(const sparse_vector_infty<TN>& B)
    {
      long irow=0;
      long ival=0;
      long brow=0;
      long bval=0;
      
      while(brow<B._index.size() && irow<_index.size())
	{
	  while(brow<B._index.size() &&
		B._index[brow]<_index[irow])
	    {
	      _index->insert(_index->begin()+irow,B._index[brow]);
	      brow++;
	      irow++;
	      _vals->insert(_vals->begin()+ival,B._vals[bval]);
	      bval++;
	      ival++;
	    }
	  if(brow<B._index.size() && B._index[brow]==_index[irow])
	    {
	      _vals[ival]+=B._vals[bval];
	      bval++;
	      brow++;
	    }
	  irow++;ival++;
	}
      if(irow>=_index.size())
	while(brow<B._index.size())
	  {
	    _index->push_back(B._index[brow]);
	    _vals->push_back(B._vals[bval]);
	    brow++;
	    bval++;
	  }
      return *this;
    }
  
  //! Subtract a sparse vector.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator-=(const sparse_vector_infty<TN>& B)
    {
      long irow=0;
      long ival=0;
      long brow=0;
      long bval=0;
      
      while(brow<B._index->size() && irow<_index->size())
	{
	  while(brow<B._index->size() &&
		B._index[brow]<_index[irow])
	    {
	      _index->insert(_index->begin()+irow,B._index[brow]);
	      brow++;
	      irow++;
	      _vals->insert(_vals->begin()+ival,-B._vals[bval]);
	      bval++;
	      ival++;
	    }
	  if(brow<B._index->size() && B._index[brow]==_index[irow])
	    {
	      _vals[ival]-=B._vals[bval];
	      bval++;
	      brow++;
	    }
	  irow++;ival++;
	}
      if(irow>=_index->size())
	while(brow<B._index->size())
	  {
	    _index->push_back(B._index[brow]);
	    _vals->push_back(-(B._vals[bval]));
	    brow++;
	    bval++;
	  }
      return *this;
    }
  
  //! Subtract a sparse vector.\test
  template <class TN> sparse_vector_infty<TN> sparse_vector_infty<TN>::operator-() const
    {
      sparse_vector_infty<TN> r(*this);
      long i;
      for(i=0 ; i<_vals->size(); i++) r._vals[i]=0-r._vals[i];
      
      return r;
    }
  
  //! Multiply by an element.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator*=(const TN x)
    {
      const long i=_vals->size();
      long j;
      for(j=0; j<i; j++) _vals[j]*=x;
      return *this;
    }
  
  //! Multiply by an element.
  template <class TN> sparse_vector_infty<TN> sparse_vector_infty<TN>::operator*(const TN x) const
    {
      const long i=_vals->size();
      long j;
      sparse_vector_infty<TN> a;
      a.copy(*this);
      for(j=0; j<i; j++) a._vals[j]*=x;
      return a;
    }
  
  //! Divide by an element.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator/=(const TN x)
    {
      const long i=_vals->size();
      long j;
      const TN y=(TN) 1/x;
      for(j=0; j<i; j++) _vals[j]*=y;
      return *this;
    }
  
  //! Divide by an element.
  template <class TN> sparse_vector_infty<TN> sparse_vector_infty<TN>::operator/(const TN x) const
    {
      const long i=_vals->size();
      long j;
      sparse_vector_infty<TN> a;
      a.copy(*this);
      const TN y=(TN) 1/x;
      for(j=0; j<i; j++) a._vals[j]*=y;
      return a;
    }
  
  //! Copy const sparse_vector_infty
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator=(const sparse_vector_infty<TN>& b)
  {
    _vals.copy(b._vals);
    _size=b._size;
    _index=b._index;
    _prune_tol=b._prune_tol;
    return *this;
  }
  
  //! Copy const sparse_vector_infty
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator=(sparse_vector_infty<TN>& b)
  {
    _vals=b._vals;
    _size=b._size;
    _index=b._index;
    _prune_tol=b._prune_tol;
    return *this;
  }
  
  //! Copy a const vector into a sparse vector.
  template <class TN> sparse_vector_infty<TN>& sparse_vector_infty<TN>::operator=(const vector<TN> b)
    {
      _size=b.size();
      _vals=b;
      refvector<long> index(b.size());
      for(long i=0;i<(long) b.size(); i++) index[i]=i;
      _index=index;
      _prune_tol=0;
      prune();
      return *this;
    }
  
  //! Copy sparse vector with copy of depth i.
  template <class TN> sparse_vector_infty<TN> sparse_vector_infty<TN>::copy(const sparse_vector_infty<TN>& b, long i)
    {
      if(i=0) (*this)=b;
      else
	{
	  _vals.copy(b._vals,i-1);
	  _size=b._size;
	  _index=b._index.vec();
	  _prune_tol=b._prune_tol;
	}
      return *this;
    }
  //! Copy a sparse vector with depth=1.
  template <class TN> sparse_vector_infty<TN> sparse_vector_infty<TN>::copy(const sparse_vector_infty<TN>& b)
    {
      return copy(b,1);
    }
  
  template <class TN> void sparse_vector_infty<TN>::set(long i, const TN& value)
    {
      (*this)(i)=value;
    }
  
  template <class TN> void sparse_vector_infty<TN>::set(long i, TN& value)
    {
      (*this)(i)=value;
    }
  
  //! Prune a sparse vector of negligible values.
  template <class TN> void sparse_vector_infty<TN>::prune(const TN x)
    {
      long i=0;
      long v=0;
      while( i<_index.size())
	{
	  if(_vals[v]>x || _vals[v]<-x) {i++;v++;}
	  else 
	    {
	      _index->erase(_index->begin()+i);
	      _vals->erase(_vals->begin()+v);
	    }
	}
    }
  template <class TN> void sparse_vector_infty<TN>::prune()
    {
      prune(_prune_tol);
    }
  
  //! Simple display of sparse vector.
  template <class TN> bool sparse_vector_infty<TN>::display() const
    {
      long row=0;
      long val=0;
      while(val < _vals.dim())
	cout << "(" << _index[row++] <<") = " << _vals[val++] << endl;
      return true;
    }
  
  //! Returns the maximum element.
  template <class TN> TN sparse_vector_infty<TN>::max() const
    {
      TN max;
      if(_vals->size() == 0) return 0;
      long i=0;
      max=_vals[i];i++;
      while(i<_vals->size())
	{
	  if(max<_vals[i]) max=_vals[i];
	  i++;
	}
      if(_vals->size()<_size && max<0) max=0;
      return max;
    }
  
  //! Normalise the sparse vector in accordance with the \f$\|\cdot\|_2 \f$ 2-norm.
  template <class TN> void sparse_vector_infty<TN>::normalise()
    {
      TN a=0;
      long i;
      for(i=0; i<_vals->size(); i++) a+=_vals[i]*_vals[i];
      a=(TN) 1/sqrt(a);
      for(long i=0; i<_vals->size(); i++) _vals[i]*=a;
    }
  
  
  //! Returns the number of nonzero elements.
  template <class TN> long sparse_vector_infty<TN>::size() const { return _index->size(); }
  //! Returns the index of the ith nonzero element.
  template <class TN> long sparse_vector_infty<TN>::index(long i) const { return _index[i];}
  //! Releases the associated memory, i.e., sets the vector to 0.
  template <class TN> void sparse_vector_infty<TN>::clear()
    {
      refvector<long> i;
      refvector<TN> a;
      _index=i;
      _vals=a;
      _size=0;
    }

  //! Retains the dimension but releases the associated memory, i.e., sets the vector to 0.
  template <class TN> void sparse_vector_infty<TN>::zero()
    {
      refvector<long> i;
      refvector<TN> a;
      _index=i;
      _vals=a;
    }
  
} // linear_algebra::
#endif
/*! @}*/
