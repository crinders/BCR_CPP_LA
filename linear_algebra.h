#ifndef _LINEAR_ALGEBRA_H
#define _LINEAR_ALGEBRA_H

#include "linear_algebra.decl"
#include "refcount.h"
#include "matrix.h"
#include "square.h"
#include "symmetric.h"
#include "asymmetric.h"
#include "sparse_vector.h"
#include "mat_sparse.h"
#include "mat_sym_full.h"
#include "mat_full.h"
#include "mat_sym_sparse.h"
#include "mat_asym_sparse.h"
#include "mat_asym_full.h"
#include "class_extensions.hh"
#endif
