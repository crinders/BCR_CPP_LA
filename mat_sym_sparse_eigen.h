/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file mat_sym_sparse_eigen.h
  \brief Eigenvalue related routines. 
  \todo throw exceptions.

*/
#ifndef _MAT_SYM_SPARSE_EIGEN_H
#define _MAT_SYM_SPARSE_EIGEN_H


//! Produce the necessary \f$ij^\ast+ji^\ast\f$.
template <class TN> void mat_sym_sparse<TN>::gen_mat(const sparse_vector<TN>& a, const sparse_vector<TN>& b, TN correction)
  /*!
    the result is \f$-=ab^\ast + ba^\ast\f$.
  */
{
  long i,j,k,l;
  TN ai;
  if(a.size() == 0 || b.size() == 0) return;
  for(i=0; i<a.size(); i++)
    {
      k=a.index(i);
      ai=a[i]*correction;
      for(j=0; j<b.size() && b.index(j)<k; j++)
	{
	  add(k,b.index(j),-ai*b[j]);
	  //	  add(b.index(j),k,-ai*b[j]);
	}
      if(j<b.size() && b.index(j) == k)
	{
	  add(k,k,-2*ai*b[j]);
	  j++;
	}
      while(j<b.size())
	{
	  add(b.index(j),k,-ai*b[j]);
	  j++;
	}
    }
  prune();
}

//! This routine produces a the diagonal, sub/superdiagonal and transformation matrix for the Householder transformation of a sparse symmetric matrix.
template <class TN> mat_sparse<TN> mat_sym_sparse<TN>::householder(vector<TN>& diagonal, vector<TN>& subdiagonal)
  /*!
    The aspired transformation is a multiplication by \f$(I-vv^\ast)\f$ where v is
    \f$(\pm e_1\|a_{12}\| + a_{12})/\sqrt{\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2}\f$.
    Notice that we begin counting at 0. Also see mat_sym_full::householder on the transformstions.
  */
{

  if (_cols==1) {
    diagonal[0]=(*this)(0,0);
    mat_sparse<TN> T(1,1);
    T.set(0,0,1);
    return T;
  }

  long i,j,k;
  vector<sparse_vector<TN> > T(_cols);
  TN absval;     // absolute value of current iteration generator
  TN absval2;    // square of the absolute value
  sparse_vector<TN> u(_rows);
  vector<long>::iterator ui;
  typename vector<TN>::iterator uv;
  sparse_vector<TN> u2;

  TN d;
  TN sd;

  // The aspired transformation is a multiplication by (I-vv^\ast) where v is
  // (pm e_1\|a_{12}\| + a_{12})/\sqrt{\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2}
  // notice that we begin counting at 0.

  for(i=_cols-1; i> 1; i--)
    {

      // The following reduces accesses to the values.

      if(is_col_nonzero(i,j))
	{
	  u.copy(_svals[j]); // u = a_1
	  if(u.is_nonzero(i-1,k))
	    {
	      sd=u[k];                 // sd = a_{12,1}
	      if(k+2 <= u.size())
		{
		  d=u[k+1];

		  // The transformation vector is zero in the first element.

		  ui=u._index->end();
		  ui--;
		  u._index->erase(ui);
		  uv=u._vals->end();
		  uv--;
		  u._vals->erase(uv);  // u = a_{12}
		}
	      else d=0;
	    }
	  else
	    {
	      sd=0;                    // sd = a_{12,1}
	      if(k+1 == u.size())
		{
		  d=u[k];

		  // The transformation vector is zero in the first element.

		  ui=u._index->end();
		  ui--;
		  u._index->erase(ui);
		  uv=u._vals->end();
		  uv--;
		  u._vals->erase(uv); // u = a_{12}
		}
	      else d=0;
	    } // u=a_{12}, sd=a_{12,1}
	  diagonal[i]=d; // d= a_{11}
	  if(u.size()>0)
	    {
	      absval2=u*u;                    // absval2 = \|a_{12}\|^2
      
	      absval=sqrt(absval2);           // absval = \|a_{12}\|
	      if(absval>1e-16)               // Avoid NaN and such for a_{12}\approx 0
		{
		  if(sd>0) subdiagonal[i]=absval;
		  else subdiagonal[i]=0-absval;
		  
		  /*
		    Store the transformation generator u.
		    The denominator will not be included in u.
		    Instead, to avoid taking the root and remultiplying,
		    the contributions will be added explicitly
		  */
		  
		  absval2=(TN) 1/(absval2+subdiagonal[i]*sd);  // absval2^{-1} = \pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2
		  absval=(TN) 1/absval;
		  
		  if(sd!=0) // if sd != 0 it  must be the last entry in u since we made sure the diagonal value of d is zero.
		    u._vals[u._vals->size()-1]+=subdiagonal[i];
		  else
		    {
		      u._index->push_back(i-1);
		      u._vals->push_back(subdiagonal[i]);
		    }                                          // u = a_{12}\pm \|a_{12}\|e_1
		  u*=sqrt(absval2);                            // u = (a_{12}\pm a_{12,1}e_1)/(\pm a_{12,1}\|a_{12}\| + \|a_{12}\|^2)
		  subdiagonal[i]*=(-1);                        // = \mp \|a_{12}\| = a_{12}-uu^\ast a_{12}
		  
		  // Now we do the transformation for the remainder of A
		  
		  /* u2=Au */
		  
		  u2=(*this)*u;
		  
		  /* u^TAu = u^Tu2 */
		  
		  TN dummy=u*u2;
		  
		  /* u2 = Au - (u^TAu)/2 u = u2-dummy/2*u */
		  
		  //dummy/=2; // Our transformation matrix is I-2uu^T/\|u\|^2
		  dummy*=0.5;
		  
		  u2-=u*dummy;
		  
		  /*
		    A' = A - uu2^T - u2u^T
		    = A - (uu^tA - u^TAu/2 uu^T)
		    - (Auu^t - u^TAu/2 uu^T)
		    = A - uu^TA - Auu^T + uu^TAuu^T
		  */
		  
		  gen_mat(u,u2,1.0);
		}
	      else
		{
		  sparse_vector<TN> g(_rows);
		  u=g;
		}
	    } // u.size()>0
	  else
	    {
	      absval2=1;
	      subdiagonal[i]=0;
	    }
	  
	} // is_col_nonzero
      else
	{
	  absval2=1;
	  d=sd=0;
	  subdiagonal[i]=0;
	  diagonal[i]=0;
	  sparse_vector<TN> g(_rows);
	  u=g;
	  absval2=1;
	}

      /* Store u in T */
      
      T[i]=u;
    }
  
  diagonal[1]=(*this)(1,1);
  diagonal[0]=(*this)(0,0);
  subdiagonal[1]=(*this)(1,0);
  
  // Generate transformation matrix.
  
  mat_sparse<TN> r(_cols,_rows);
  
  for(i=_cols-1; i>=0; i--) r.add(i,i,1); // we start with the identity.

  for(i=2; i< T.size() ; i++)
    {
      u=T[i];
      u2=r*u;
      for(j=0; j<u._index->size(); j++)
	r[u._index[j]]-=u2*u[j]; // No danger here because there are no empty columns.
    }
  r.prune(_prune_tol);
  return r;
}

//! This routine produces a the diagonal, sub/superdiagonal and transformation matrix for the Householder transformation of a sparse symmetric matrix.
template <class TN> mat_sparse<TN> mat_sym_sparse<TN>::householder(refvector<TN>& diagonal, refvector<TN>& subdiagonal)
{
  return householder(diagonal.vec(), subdiagonal.vec());
}

#endif
/*! @}*/
