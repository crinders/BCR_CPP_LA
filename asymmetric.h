/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file asymmetric.h
  \brief Provide manipulations for antisymmetric full matrices.
  \todo throw exceptions.

*/
#ifndef _ASYMMETRIC_H
#define _ASYMMETRIC_H
#include <vector>
#include "square.h"
#include "asymmetric.decl"

namespace linear_algebra
{

  template <class TN> asymmetric<TN>& asymmetric<TN>::transpose() { return (*this)*((TN) -1); }
  template <class TN> asymmetric<TN>& asymmetric<TN>::operator*(const TN a) const
  {
    throw domain_error("You need a specific implementation of asymmetric operator*(TN a)");
  }
  
} // linear_algebra::
#endif
/*! @}*/
