/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file mat_sparse.h
  \brief Provide functionality for sparse matrix and vector interactions.
*/
#ifndef _MAT_SPARSE_H
#define _MAT_SPARSE_H
#include <vector>
#include <stdexcept>
#include "linear_algebra.decl"
#include "matrix.decl"
#include "mat_sparse.decl"

#include "refcount.decl"
#include "sparse_vector.decl"
#include "mat_full.decl"
#include "mat_sym_sparse.decl"

namespace linear_algebra
{
  
  /*******************************************************************************************************************************************/
  
  //! Class of sparse column matrices.
  
  // Constructors
  
  //! Constructor of n columns and m rows with uninitialised values.
  template <class TN> mat_sparse<TN>::mat_sparse(long n, long m): _prune_tol(0), _empty(true) {matrix<TN>::_size=0; matrix<TN>::_rows=m; matrix<TN>::_cols=n;}
  //! Constructor of n columns and m rows with initialisation of values.
  template <class TN> mat_sparse<TN>::mat_sparse(long n, long m, const vector<long>& scols, const vector<sparse_vector<TN> >& vals)
    /*!
      \param n Dimension of matrix.
      \param scols Vector of column indices.
      \param vals Vector of columns. Each column is a sparse vector.
      
      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
    */
    {
      matrix<TN>::_rows=m;
      matrix<TN>::_cols=n;
      if(scols.size()!=vals.size()) 
	{
	  throw domain_error("values and indices don't match up in   mat_sparse(long n, long m, vector<long> scols, vector<sparse_vector<TN>> vals) ");
	  exit(1);
	}
      _scols=vals;
      _colindex=scols;
      _prune_tol=0;
      _empty=false;
      matrix<TN>::_size=vals.size();
    }
  
  //! Constructor of n columns and m rows with initialisation of values.
  template <class TN> mat_sparse<TN>::mat_sparse(long n, long m, const vector<long>& scols, const refvector<sparse_vector<TN> >& vals)
    /*!
      \param n Dimension of matrix.
      \param scols Vector of column indices.
      \param vals Reference counted vector of columns. Each column is a sparse vector.
      
      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
    */
    {
      matrix<TN>::_rows=m;
      matrix<TN>::_cols=n;
      if(scols.size()!=vals->size()) 
	{
	  throw domain_error("values and indices don't match up in   mat_sparse(long n, long m, vector<long> scols, vector<sparse_vector<TN>> vals) ");
	  exit(1);
	}
      _scols=vals;
      _colindex=scols;
      _prune_tol=0;
      _empty=false;
      matrix<TN>::_size=vals->size();
    }
  
  //! Constructor of n columns and m rows with initialisation of values.
  template <class TN> mat_sparse<TN>::mat_sparse(long n, long m, const refvector<long>& scols, const refvector<sparse_vector<TN> >& vals)
    /*!
      \param n Dimension of matrix.
      \param scols Reference counted vector of column indices.
      \param vals Reference counted vector of columns. Each column is a sparse vector.
      
      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
    */
    {
      matrix<TN>::_rows=m;
      matrix<TN>::_cols=n;
      if(scols->size()!=vals->size()) 
	{
	  throw domain_error("values and indices don't match up in   mat_sparse(long n, long m, vector<long> scols, vector<sparse_vector<TN>> vals) ");
	  exit(1);
	}
      _scols=vals;
      _colindex=scols;
      _prune_tol=0;
      _empty=false;
      matrix<TN>::_size=vals->size();
    }
  
  //! Constructor of n columns and m rows with initialisation of values.
  template <class TN> mat_sparse<TN>::mat_sparse(long n, long m, const refvector<long>& scols, const vector<sparse_vector<TN> >& vals)
    /*!
      \param n Dimension of matrix.
      \param scols Reference counted Vector of column indices.
      \param vals Vector of columns. Each column is a sparse vector.
      
      Each value has an associated row and column index in srows and scols. 
	  Therefore they all have the same size.
    */
    {
      matrix<TN>::_rows=m;
      matrix<TN>::_cols=n;
      if(scols->size()!=vals.size()) 
	{
	  throw domain_error("values and indices don't match up in   mat_sparse(long n, long m, vector<long> scols, vector<sparse_vector<TN>> vals) ");
	  exit(1);
	}
      _scols=vals;
      _colindex=scols;
      _prune_tol=0;
      _empty=false;
      matrix<TN>::_size=vals.size();
    }
  
  template <class TN> mat_sparse<TN>::mat_sparse(): _empty(true), _prune_tol(0) {matrix<TN>::_size=0; matrix<TN>::_rows=0; matrix<TN>::_cols=0; }
  
  // Operators *********************************************************************************************************

  template <class TN> mat_sparse<TN>& mat_sparse<TN>::operator=(mat_sparse<TN>& a)
  {
    _prune_tol=a._prune_tol;
    matrix<TN>::_cols=a._cols;
    matrix<TN>::_rows=a._rows;
    matrix<TN>::_size=a._size;
    _empty=a._empty;
    _colindex=a._colindex;
    _scols=a._scols;
    return *this;
  }
  
  //! Return a column through direct access. No integrity checked.
  template <class TN> sparse_vector<TN>& mat_sparse<TN>::operator[](const long x) {
#ifdef DEBUG
    if(x<0 || x>_scols.size()) throw domain_error("mat_sparse::operator[](const long x): illegal acces.");
#endif
    return _scols[x]; }
  
  //! Return a column through direct access. No integrity checked.
  template <class TN> const sparse_vector<TN>& mat_sparse<TN>::operator[](const long x) const {
#ifdef DEBUG
    if(x<0 || x>_scols->size()) throw domain_error("mat_sparse::operator[](const long x) const: illegal acces.");
#endif
    return _scols[x];}
  
  //! Return value at (x,y).
  template <class TN> const TN& mat_sparse<TN>::operator()(const long x,const long y) const
    {
      long i,j;
      if( x<matrix<TN>::_cols && y<matrix<TN>::_rows)
	{
	  if(is_nonzero(x,y,i,j)) { 
#ifdef DEBUG
	    if(i>_scols->size() || i<0) throw domain_error("mat_sparse::operator()(const long x,const long y) const:Illegal access to vector");
	    if(j>_scols[i]._vals->size() || j<0) throw domain_error("mat_sparse::operator()(const long x,const long y) const:Illegal access in vector");
#endif
	    return _scols[i][j];}
	  else
	    return 0;
	}
      throw domain_error(" Illegal access outside of dimension in virtual TN mat_sparse::operator()(const long x,const long y) const " );
      exit(1);
    }
  
  //! Multiply by an object of TN into left side.
  template <class TN> mat_sparse<TN>& mat_sparse<TN>::operator*=(const TN& x)
    {
      if( !_empty)
	for(long i=0; i<_colindex.size(); i++)
	  {
	    _scols[i]*=x;
	  }
      return *this;
    }
  //! Divide by an object of TN into left side. This is very slow and not advisable.
  template <class TN> mat_sparse<TN>& mat_sparse<TN>::operator/=(const TN& x)
    {
      long i=0;
      if( !_empty)
	while(i<_colindex.size())
	  {
	    _scols[_colindex[i]]/=x;
	    i++;
	  }
      return *this;
    }
  
  //! Multiply two sparse matrices.
  template <class TN> mat_sparse<TN> mat_sparse<TN>::operator*(const  mat_sparse<TN>& b) const
    {
      if( matrix<TN>::_cols != b._rows)
	{
	  throw domain_error( "Incompatible dimensions in mat_sparse mat_sparse::operator*(const mat_sparse& a, const mat_sparse& b)");
	  exit(1);
	}
      try {
	refvector<sparse_vector<TN> > rv(b._colindex->size());
	
	long i,j;
	long ncol;
	for(i=0; i<b._colindex->size();i++)
	  {
	    ncol=_scols->size();
	    rv[i]._size=matrix<TN>::_rows;
	    rv[i]=(*this)*b._scols[i];
	  }
	
	mat_sparse<TN> r(b._cols,matrix<TN>::_rows,b._colindex.vec(),rv);
	r.prune();
	return(r);
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from mat_sparse<TN> mat_sparse::operator*(const  mat_sparse<TN>& b) const");
      }
    }
  
  //! Multiply a sparse matrix by a sparse vector.
  template <class TN> sparse_vector<TN> mat_sparse<TN>::operator*(const sparse_vector<TN>& a) const
    {
      if(a._size != matrix<TN>::_cols)
	{
	  throw domain_error("Incompatible dimensions in sparse_vector<TN> mat_sparse::operator*(const sparse_vector<TN>& a)" );
	  exit(1);
	}
      try {
	sparse_vector<TN> r(matrix<TN>::_rows);
	vector<long> thisindex; // We will produce one vector of mutual nonzeros. O(n) instead of O(n^2).
	vector<long> aindex;
	long j=0;
	long i=0;
	while( j<a._index.size())
	  {
	    while(i<_colindex.size() && _colindex[i]<a._index[j]) i++;
	    if(i<_colindex.size() && _colindex[i]==a._index[j])
	      {
		thisindex.push_back(i);
		aindex.push_back(j);
		i++;
	      }
	    j++;
	  };
	for(long i=0; i<(long) thisindex.size(); i++)
	  r+=_scols[thisindex[i]]*a[aindex[i]];
	r._prune_tol=_prune_tol;
	r.prune();
	return r;
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from sparse_vector<TN> mat_sparse::operator*(const sparse_vector<TN>& a) const");
      }
    }
  
  
  //! Multiply a sparse matrix by a sparse symmetric matrix.
  template <class TN> mat_sparse<TN> mat_sparse<TN>::operator*(const mat_sym_sparse<TN>& b) const
    /*!
      The result is automatically pruned for the default pruning value set in _prune_tol.
    */
    {
#ifdef DEBUG
      if(matrix<TN>::_cols!=b._rows)
	{
	  throw domain_error("Incompatible dimensions in  mat_sparse mat_sparse::operator*(const mat_sym_sparse<TN>& b)");
	  exit(1);
	}
#endif
      try {
	// Determine number of columns of the upper half.
	
	long i;
	
	refvector<sparse_vector<TN> > sv(b._scols->size());
	
	for(i=0;i<b._scols->size();i++)
	  sv[i]=(*this)*b._svals[i];
	
	mat_sparse<TN> r1(b._cols,matrix<TN>::_rows,b._scols.vec(),sv);
	
	// Let's get lower half now.
	
	long bv=0;
	long bi=0;
	long ii=0;
	long iv=0;
	vector<long>::iterator ci;
	typename vector<sparse_vector<TN> >::iterator svi;
	long k;
	
	for(i=0; i<b._svals->size();i++)
	  {
	    while(ii<_colindex->size() && _colindex[ii]<b._scols[i])
	      {
		ii++;
		iv++;
	      }
	    if(ii<_colindex->size())
	      {
		if(_colindex[ii]==b._scols[i])
		  {
		    bi=0;
		    bv=0;
		    while(bi<b._svals[i]._index->size() && b._svals[i]._index[bi]<b._scols[i])
		      {
			if(r1.is_col_nonzero(b._svals[i]._index[bi],k))
			  r1._scols[k]+=_scols[iv]*b._svals[i]._vals[bv];
			else
			  {
			    svi=r1._scols->begin();
			    svi+=k;
			    svi=r1._scols->insert(svi,matrix<TN>::_rows);
			    ci=r1._colindex->begin();
			    ci+=k;
			    ci=r1._colindex->insert(ci,b._svals[i]._index[bi]);
			    (*svi)=_scols[iv]*b._svals[i]._vals[bv];
			  }
			bi++;
			bv++;
		      }
		  }
	      }
	    else i=b._svals->size();
	  }
	r1.prune();
	return r1;
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from mat_sparse<TN> mat_sparse::operator*(const mat_sym_sparse<TN>& b) const");
      }
    }
  
  //! Multiplies the \b left hand side from the \underline{ left } by the \b right hand side.
  template <class TN> mat_sparse<TN>& mat_sparse<TN>::operator*=(const mat_sparse<TN>& b)
    {
      if( b._cols != matrix<TN>::_rows)
	{
	  throw domain_error( "Incompatible dimensions in mat_sparse mat_sparse::operator*=(const mat_sparse& b)");
	  exit(1);
	}
      try {
	long i;
	for(i=0; i<_colindex.size();i++)
	  {
	    _scols[i]=b*_scols[i];
	  }
	matrix<TN>::_rows=b._rows;
	prune();
	return *this;
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from mat_sparse<TN>& mat_sparse::operator*=(const mat_sparse<TN>& b)");
      }
    }
  
  //! Multiplies the \b left hand side from the \underline{left } by the \b right hand side.
  template <class TN> mat_sparse<TN>& mat_sparse<TN>::operator*=(const mat_full<TN>& b)
    {
#ifdef DEBUG
      if( b._cols != matrix<TN>::_rows)
	{
	  throw domain_error( "Incompatible dimensions in mat_sparse mat_sparse::operator*=(const mat_full& b)");
	  exit(1);
	}
      try {
#endif
	long i,j;
	long ncol;
	for(i=0; i<_colindex->size();i++)
	  {
	    _scols[i]=b*_scols[i];
	  }
	matrix<TN>::_rows=b._rows;
	prune();
	return *this;
#ifdef DEBUG
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from mat_sparse<TN>& mat_sparse::operator*=(const mat_full<TN>& b)");
      }
#endif
    }
  
  //! Multiply a sparse matrix with a full refvector.
  template <class TN> refvector<TN>& mat_sparse<TN>::multiply(const refvector<TN>& a, refvector<TN>& b) const
    {
#ifdef DEBUG
      if(a->size() != matrix<TN>::_cols)
	throw domain_error("Incompatible dimensions in refvector mat_sparse::operator*(const refvector) const");
      if(matrix<TN>::_rows != b.dim())
	throw domain_error("Incompatible dimensions for output in refvector mat_sparse::operator*(const refvector) const");
      
      try {
#endif
	long i,j;
	for(i=0;i<b.dim();i++) b[i]=(TN) 0;
	for(i=0;i<_colindex.size();i++)
	  for(j=0;j<_scols[i]._index.size();j++)
	    b[_scols[i]._index[j]]+=a[_colindex[i]]*_scols[i]._vals[j];
	return b;
#ifdef DEBUG
      } catch (domain_error e) {
	cerr << e.what() << endl;
	throw domain_error("Called from refvector mat_sparse::operator*(const refvector) const");
      }
#endif
    }
  
  //! Multiply a sparse matrix with a full refvector.
  template <class TN> refvector<TN> mat_sparse<TN>::operator*(const refvector<TN>& a) const
    {
      refvector<TN> r(matrix<TN>::_rows);
      return multiply(a,r);
    }
  
  //! Multiply a sparse matrix with a full matrix.
  template <class TN> mat_full<TN>& mat_sparse<TN>::multiply(const mat_full<TN>& a, mat_full<TN>& R) const
    {
#ifdef DEBUG
      if( a.cols() != matrix<TN>::_rows)
	throw domain_error( "Incompatible dimensions in mat_full mat_sparse::operator*(const mat_full& a)");
      if( R.rows() != matrix<TN>::_rows || R.cols() != a.cols())
	throw domain_error( "Incompatible dimensions for output in mat_full mat_sparse::operator*(const mat_full& a)");
      
      try {
#endif
	long i;
	for(i=0;i<a.cols();i++)
	  multiply(a[i],R[i]);
	return R;
	
#ifdef DEBUG
      } catch (domain_error e) {
	cerr << e.what() << endl;
	throw domain_error("called from mat_full operator*(mat_full )");
      }
#endif 
    }
  
  //! Multiply a sparse matrix with a full matrix.
  template <class TN> mat_full<TN> mat_sparse<TN>::operator*(const mat_full<TN>& a) const
    {
      mat_full<TN> R(a.cols(),matrix<TN>::_rows);
      return multiply(a,R);
    }
  
  // Normal member functions ****************************************************************************************************
  
  //! Copy matrix with deep copy.
  template <class TN> mat_sparse<TN>& mat_sparse<TN>::copy(const mat_sparse<TN>& a)
    {
      matrix<TN>::_size=a._size;
      _scols.copy(a._scols);
      _colindex.copy(a._colindex);
      matrix<TN>::_rows=a._rows;
      matrix<TN>::_cols=a._cols;
      _empty=a._empty;
      return *this;
    }
  
  //! Transpose the sparse matrix.
  template <class TN> mat_sparse<TN> mat_sparse<TN>::transpose() const
    {
      try {
	long i,j;
	vector<long> colindex;
	long ci;
	long svi;
	vector<sparse_vector<TN> > scols;
	for(i=0; i<_scols->size(); i++)
	  {
	    ci=0;
	    svi=0;
	    j=0;
	    while(j<_scols[i].size())
	      {
		while(ci<colindex.size() &&
		      colindex[ci]<_scols[i]._index[j])
		  {
		    ci++;
		    svi++;
		  }
		if(!(ci<colindex.size() &&
		     colindex[ci]==_scols[i]._index[j]))
		  {
		    colindex.insert(colindex.begin()+ci,_scols[i]._index[j]);
		    scols.insert(scols.begin()+svi,matrix<TN>::_cols);
		    scols[svi](_colindex[i])=_scols[i][j];
		    ci++;
		    svi++;
		  }
		else if(ci<colindex.size() && colindex[ci]==_scols[i]._index[j])
		  {
		    scols[svi](_colindex[i])=_scols[i][j];
		    svi++;
		    ci++;
		  }
		j++;
	      }
	  }
	mat_sparse<TN> r(matrix<TN>::_rows,matrix<TN>::_cols, colindex,scols);
	return r;
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from mat_sparse<TN> transpose() const");
      }
    }
  
  //! Prune  the matrix.
  template <class TN> void mat_sparse<TN>::prune(const TN x)
    {
      long i=0;
      long j=0;
      while(i<_scols.size())
	{
	  _scols[i]._prune_tol=_prune_tol;
	  _scols[i].prune(x);
	  if(_scols[i]._index->size() == 0) 
	    {
	      _scols->erase(_scols->begin()+i);
	      _colindex->erase(_colindex->begin()+j);
	    }
	  else
	    {
	      i++;
	      j++;
	    }
	}
      matrix<TN>::_size=_scols->size();
      if(matrix<TN>::_size == 0) _empty=true;
    }
  template <class TN> void mat_sparse<TN>::prune()
    {
      prune(_prune_tol);
    }
  
  //! Checks whether an access is non zero.
  template <class TN> bool mat_sparse<TN>::is_nonzero(long x, long y, long &j, long &k) const
    /*!
      \param j,k hold the position of x,y in _scols,_scols[j]. If (x,y) is 0. An insertion would happen here.
    */
    {
      j=0;
      k=0;
      if( _empty) return false;
      
      while( j<_colindex.size() &&
	     x>_colindex[j]) j++;
      if(j<_colindex.size() && x==_colindex[j]) return _scols[j].is_nonzero(y,k);
      else return false;
    }
  
  //! Checks whether a column is empty.
  template <class TN> bool mat_sparse<TN>::is_col_nonzero(const long x, long &i) const
    {
      i=0;
      if(_empty) return false;
      for(;i<_colindex.size() && _colindex[i]<x; i++);
      if(i<_colindex.size() && _colindex[i]==x) return true;
      else return false;
    }
  
  //! Adds a value.
  template <class TN> bool mat_sparse<TN>::add(const long x, const long y, const TN z)
    /*!
      \param z is added to (x,y).
      If the value is zero the associated vectors are expanded.
    */
    {
      long i;
      if(x<matrix<TN>::_cols && y<matrix<TN>::_rows)
	{
	  if(is_col_nonzero(x,i)) 
	    {
	      try { 
		(_scols[i])(y)+=z;
	      } catch(domain_error e) { 
		cerr << e.what() << endl;
		throw domain_error(" called from bool mat_sparse::add(const long x, const long y, const TN z)");
	      }
	    }
	  else
	    {
	      vector<long>::iterator s=_colindex->begin();
	      s+=i;
	      _colindex->insert(s,x);
	      typename vector<sparse_vector<TN> >::iterator v=_scols->begin();
	      v+=i;
	      sparse_vector<TN> a;
	      v=_scols->insert(v,a);
	      (*v)._size=matrix<TN>::_rows;
	      (*v)(y)+=z;
	      matrix<TN>::_size++;
	    }
	  _empty=false;
	  return true;
	}
      throw domain_error("Outside of dimension of mat_sparse::add " );
      exit(1);
    }
  //! Simple display of matrix.
  template <class TN> bool mat_sparse<TN>::display() const
    {
      long col=0;
      long val=0;
      long v=0;
      long i=0;
      while(val< _scols->size())
	{
	  v=0;
	  i=0;
	  while(v<_scols[val]._vals->size())
	    cout << "(" << _colindex[col] << "," << _scols[val]._index[i++] <<") = " << _scols[val]._vals[v++];
	  cout << endl;
	  val++;
	  col++;
	}
      return true;
    }
  
  //! Extracts a block from a column.
  template <class TN> vector<TN> mat_sparse<TN>::extract_full_col(const long column, const long xmin, const long xmax) const
    {
      if(xmin>xmax) {
	throw domain_error("xmin > xmax in mat_sparse::extract_full_col(const long column, const long xmin, const long xmax) const" );
	exit(1);
      }
      if(xmin<0 || xmax>matrix<TN>::_rows) 
	{
	  throw domain_error("Matrix dimensions exceeded in mat_sparse::extract_full_col(const long column, const long xmin, const long xmax) const");
	  exit(1);
	}
      try {
	vector<TN> r(xmax-xmin);
	long i,j;
	if(is_col_nonzero(column,i))
	  for(j=xmin; j<xmax; j++) r[j-xmin]=_scols[i](j);
	return r;
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from vector<TN> mat_sparse::extract_full_col(const long column, const long xmin, const long xmax) const");
      }
    }
  //! Extracts a block from a column with default max=matrix<TN>::_rows.
  template <class TN> vector<TN> mat_sparse<TN>::extract_full_col(const long column, const long xmin) const
  {
    return extract_full_col(column, xmin, matrix<TN>::_rows);
  }
  
  //! Extracts a block from a column with default max=matrix<TN>::_rows.
  template <class TN> sparse_vector<TN> mat_sparse<TN>::extract_sparse_col(const long column, const long xmin) const
    {
      return extract_sparse_col(column, xmin, matrix<TN>::_rows);
    }
  
  //! Extracts a sparse block from a column.
  template <class TN> sparse_vector<TN> mat_sparse<TN>::extract_sparse_col(const long column, const long xmin, const long xmax) const
    {
      if(xmin>xmax) {
	throw domain_error("xmin > xmax in mat_sparse::extract_sparse_col(const long column, const long xmin, const long xmax) const" );
	exit(1);
      }
      if(xmin<0) 
	{
	  throw domain_error("Matrix dimensions exceeded in mat_sparse::extract_sparse_col(const long column, const long xmin, const long xmax) const");
	  exit(1);
	}
      try {
	long i;
	if(is_col_nonzero(column,i))
	  {
	    if(xmin==0 && xmax==matrix<TN>::_rows) return _scols[i];
	    long cs=0;
	    long v=0;
	    refvector<long> indices;
	    refvector<TN> vals;
	    while(cs<_scols[i]._index->size() && _scols[i]._index[cs]<xmin) {cs++; v++;};
	    while(cs<_scols[i]._index->size() && _scols[i]._index[cs]<xmax)
	      {
		indices->push_back(_scols[i]._index[cs]-xmin);
		vals->push_back(_scols[i]._vals[v]);
		v++;
		cs++;
	      }
	    sparse_vector<TN> r(xmax-xmin, indices,vals);
	    return r;
	  }
	else
	  {
	    sparse_vector<TN> r(xmax-xmin);
	    return r;
	  }
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from sparse_vector<TN> mat_sparse::extract_sparse_col(const long column, const long xmin, const long xmax) const");
      }
    }
  
  //! Returns a mat_sym_sparse object of the upper triangle of a sparse matrix.\test compiles. Execution?
  template <class TN> mat_sym_sparse<TN> mat_sparse<TN>::sym_upper_triangle() const
    {
      try {
	long i;
	refvector<sparse_vector<TN> > sv;
	refvector<long> si;
	long m;
	if(matrix<TN>::_rows> matrix<TN>::_cols) m=matrix<TN>::_cols;
	else m=matrix<TN>::_rows;
	for(i=0; i<_scols->size() && _colindex[i]<m; i++)
	  {
	    si->push_back(_colindex[i]);
	    refvector<TN> svv;
	    refvector<long> svi;
	    long j;
	    for(j=0; j<_scols[i]._index->size() && _scols[i]._index[j]<=_colindex[i]; j++)
	      {
		svv->push_back(_scols[i][j]);
		svi->push_back(_scols[i]._index[j]);
	      }
	    sparse_vector<TN> b(m, svi,svv);
	    sv->push_back(b);
	  }
	mat_sym_sparse<TN> r(m, si, sv);
	return r;
      } catch(domain_error e) { 
	cerr << e.what() << endl;
	throw domain_error(" called from  mat_sym_sparse<TN> sym_upper_triangle() const");
      }
    }
  
  //! Deletes described column from the matrix. Direct access deletion.
  template <class TN> void mat_sparse<TN>::erase(long c)
    {
      if(c<_scols->size())
	{
	  _scols->erase(_scols->begin()+c);
	  _colindex->erase(_colindex->begin()+c);
	  matrix<TN>::_size--;
	}
      else
	throw domain_error("mat_sparse::erase() has encountered an illegal deletion request");
    }
  
  //! Return the density of non-zero elements.
  template <class TN> double mat_sparse<TN>::density() const 
    {
      long i,j;
      j=0;
      for(i=0; i<_scols->size(); i++)
	j+=_scols[i]._vals->size();
      return (double) j/(double) (matrix<TN>::_rows*matrix<TN>::_cols);
    }
  
  //! Returns the number of nonzero columns.
  template <class TN> long mat_sparse<TN>::size() const { return _scols->size(); }
  //! Returns the column of a given index.
  template <class TN> long mat_sparse<TN>::index(const long x) const { return _colindex[x];}
  
  //! Set a value.
  template <class TN> void mat_sparse<TN>::set(const long x, const long y, TN z)
    {
      long i=0;
      if(is_col_nonzero(x,i))
	(_scols[i])(y)=z;
      else
	{
	  vector<long>::iterator s=_colindex->begin();
	  s+=i;
	  _colindex->insert(s,x);
	  sparse_vector<TN> v((*this)._rows);
	  v(y)+=z;
	  _scols.insert(i,v);
	  //	  (*v)._size=matrix<TN>::_rows;
	  matrix<TN>::_size++;
	  _empty=false;
	}
    }
  
  //! Sets the matrix to zero releasing all associated memory.
  template <class TN> void mat_sparse<TN>::clear()
    {
      _empty=true;
      matrix<TN>::_rows=matrix<TN>::_cols=0;
      refvector<long> c;
      refvector<sparse_vector<TN> > a;
      _colindex=c;
      _scols=a;
    }
  
  //! Sets the matrix to zero releasing all associated memory.
  template <class TN> mat_sparse<TN>& mat_sparse<TN>::zero()
  {
    _empty=true;
      refvector<long> c;
      refvector<sparse_vector<TN> > a;
      _colindex=c;
      _scols=a;
      return (*this);
  }
  
} // linear_algebra::

#include "refcount.h"
#include "sparse_vector.h"
#include "mat_full.h"
#include "mat_sym_sparse.h"

#endif
/*! @}*/
