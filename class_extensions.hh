/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file class_extensions.hh
   \brief General expansions for LA
*/

#ifndef _CLASS_EXTENSIONS_HH
#define _CLASS_EXTENSIONS_HH
#include <stdexcept>
#include <sstream>
#include "linear_algebra.decl"
#include "class_extensions.decl"
#include "refcount.decl"
#include "matrix.decl"
#include "mat_full.decl"
#include "mat_asym_full.h"
#include <cmath>

namespace linear_algebra
{
  template<class TN>
  inline mat_sym_sparse<TN>& zero(mat_sym_sparse<TN>& r) { return r.zero();}
  template<class TN>
  inline mat_sym_full<TN>& zero(mat_sym_full<TN>& r) { return r.zero();}
  template<class TN>
  inline mat_sparse<TN>& zero(mat_sparse<TN>& r) { return r.zero();}
  template<class TN>
  inline mat_full<TN>& zero(mat_full<TN>& r) { return r.zero();}
  
  template<class TN>
  inline refvector<TN>& zero(refvector<TN>& r) { return r.zero();}

  inline double& zero(double &r) { r=0; return r; }

  template<typename TN>
  inline TN& zero(TN &r) { r=(TN) 0; return r; }
  
  inline double& one(double& r) { r=(double) 1; return r;}
  
  template<class TN>
  inline matrix<TN>& one(matrix<TN>& r) { return r.one();}
  
  inline void copy(double& a, const double& b, const long i) { a=b; }
  
  inline void copy(double& a, const double& b) { a=b; }
  
  inline void copy(long& a, const long& b, const long i) { a=b; }
  
  inline void copy(long& a, const long& b) { a=b; }
  
  inline void copy(bool& a, const bool& b) { a=b; }
  
  template<class TN>
  inline void copy(mat_full<TN>& a, const mat_full<TN>& b, const long i) { if(i>0) a.copy(b,i); else a=b;}
  template<class TN>
  inline void copy(mat_full<TN>& a, const mat_full<TN>& b) { a.copy(b); }
  template<class TN>
  inline void copy(mat_full<TN>& a, mat_full<TN>& b) { a.copy(b); }
  
  template<class TN>
  inline void copy(mat_sparse<TN>& a, const mat_sparse<TN>& b, const long i) { if(i>0) a.copy(b,i); else a=b;}
  template<class TN>
  inline void copy(mat_sparse<TN>& a, const mat_sparse<TN>& b) { a.copy(b); }
  template<class TN>
  inline void copy(mat_sparse<TN>& a, mat_sparse<TN>& b) { a.copy(b); }
  
  template<class TN>
  inline void copy(mat_sym_sparse<TN>& a, const mat_sym_sparse<TN>& b, const long i) { if(i>0) a.copy(b,i); else a=b;}
  template<class TN>
  inline void copy(mat_sym_sparse<TN>& a, const mat_sym_sparse<TN>& b) { a.copy(b); }
  template<class TN>
  inline void copy(mat_sym_sparse<TN>& a, mat_sym_sparse<TN>& b) { a.copy(b); }
  
  template<class TN>
  inline void copy(mat_sym_full<TN>& a, const mat_sym_full<TN>& b, const long i) { if(i>0) a.copy(b,i); else a=b;}
  template<class TN>
  inline void copy(mat_sym_full<TN>& a, const mat_sym_full<TN>& b) { a.copy(b); }
  template<class TN>
  inline void copy(mat_sym_full<TN>& a, mat_sym_full<TN>& b) { a.copy(b); }
  
  template<class TN>
  inline void copy(refvector<TN>& a, refvector<TN>& b, const long i) { if(i>0) a.copy(b,i); else a=b; }
  template<class TN>
  inline void copy(refvector<TN>& a, const refvector<TN>& b, const long i) { if(i>0) a.copy(b,i); else a=b; }
  template<class TN>
  inline void copy(refvector<TN>& a, const refvector<TN>& b) {a.copy(b);}
  
  template<class TN>
  inline void copy(sparse_vector<TN>& a, sparse_vector<TN>& b, const long i)
  {
    if(i>0)
      a.copy(b,i);
    else a=b;
  }
  
  template<class TN>
  inline void copy(sparse_vector<TN>& a, const sparse_vector<TN>& b)
  {
    a.copy(b);
  }

  template<class C>
  inline void copy(C& a, const C& b)
  {
    a=b;
  }

  template<class TN>
  inline void display(const matrix<TN>& a, ostream& outs) { return a.display(outs); }
  template<class TN>
  inline void display(const refvector<TN>& a, ostream& outs) { return a.display(outs); }

  inline void display(const double& a, ostream& outs) { 
    outs << a << " ";
    return;
  }
  
  template<class TNTN>
  mat_full<TNTN> exp(const mat_asym_full<TNTN>& A)
  {
    mat_full<TNTN> U(A.cols(),A.cols());
    double n=1;
    mat_full<TNTN> A_n(A.cols(),A.cols());
    long i;
    for(i=0;i<U.cols();i++) U[i][i]=1;
    for(i=0;i<A.cols();i++) A_n[i][i]=1;
    TNTN error=1;
    i=1;
    long j;
    while(error>1e-16)
      {
	A_n*=A;
	A_n*=(TNTN) 1/(TNTN) i;
	U+=A_n;
	i++;
	n=0;
	for(j=0;j<A_n.cols();j++) n+=A_n[j]*A_n[j];
	error=0;
	for(j=0;j<U.cols();j++) error+=U[j]*U[j];
	error=sqrt(n/error);
      }
    return U;
  }

  template <class TNTN>
  mat_full<TNTN> dexp(mat_asym_full<TNTN> A,const long k, const long l)
  {
    mat_full<double> A_n(A.cols(),A.cols());
    mat_full<double> dA_n(A.cols(),A.cols());
    mat_asym_full<double> dA_kl(A.cols());
    long n;
    for(n=0;n<A_n.cols();n++) A_n[n][n]=(double) 1;
    double error=1.0;
    n=0;
    mat_full<double> U(A.cols(),A.cols());
    dA_kl.set(k,l,(double) 1);
    long i;
    double e;
    while(error>1e-16)
      {
	n++;
	dA_n+=A*dA_n;
	dA_n+=(dA_kl*A_n);
	A_n*=A;
	dA_n*=(double) 1/n;
	U+=dA_n;
	A_n*=(double) 1/n;
	error=0;
	for(i=0;i<A.cols();i++) error+=A_n[i]*A_n[i];
	e=0;
	for(i=0;i<U.cols();i++) e+=U[i]*U[i];
	error=sqrt(error/e);
      }
    return U;
  }
}
#include "linear_algebra.h"
#endif
/*! @}*/
