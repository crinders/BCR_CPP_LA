/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{ */
/*! @file square.h
  @brief Describes fundamentals of matrices.

*/
#ifndef _SQUARE_H
#define _SQUARE_H
#include <vector>
#include <iostream>
#include "square.decl"
#include "matrix.h"

//! Linear algebra Namespace
namespace linear_algebra
{

  //! The square matrix class contains all n by n matrices.
  
  template <class TN> TN square<TN>::trace() const { 
    long i;
    TN trace=0;
    for(i=0;i<_cols;i++) trace+=(*this)(i,i);
    return trace;
  }
  
}
#endif
/*! @}*/
