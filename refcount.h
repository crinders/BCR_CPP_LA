/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file refcount.h
  \brief Contains a reference counted vector class as well as general reference counting.
*/
#ifndef _REFCOUNT_H
#define _REFCOUNT_H
using namespace std;
#include <vector>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <cstdlib>
//#include "linear_algebra.decl"
#include "refcount.decl"
#include "sparse_vector.decl"
#include "class_extensions.hh"
#include <cstdlib>
#include <cmath>


template <class TN>
void std::operator>>(const vector<TN>& a, ostream& OUT)
{
  typename vector<TN>::const_iterator i=a.begin();
  while(i!=a.end()) OUT << *(i++) << " ";
  OUT << endl;
}

/*template <typename TN>
void std::operator>>(const TN& a, ostream& OUT)
{
  OUT << a;
}*/

namespace linear_algebra
{

  //! Default constructor;
  template <class Type> refcount<Type>::refcount()
    {
      _refcount=new unsigned long(1);
      _P=new Type();
    }
  //! Constructor from a pointer.
  template <class Type> refcount<Type>::refcount(Type* l) { _P=l; _refcount=new unsigned long(1); }

  //! Copy constructor, but no deep copy.
  template <class Type> refcount<Type>::refcount(refcount<Type>& rhs)
    {
      _refcount=rhs._refcount;
      (*_refcount)++;
      _P=rhs._P;
    }

  //! Copy constructor, but no deep copy.
  template <class Type> refcount<Type>::refcount(const refcount<Type>& rhs)
  {
    _refcount=new unsigned long(1);
    *_refcount=1;
    _P=new Type();
    _P=rhs._P;
  }

  //! Destructor checks for reference count and deletes if necessary.
  template <class Type> refcount<Type>::~refcount()
    {
      (*_refcount)--;
      if(*_refcount==0)
	{
	  if(_P != 0x0)
	    delete _P;
	  delete _refcount;
	}
    }

  //! Assignment without deep copy. Old information is discarded if no references remain.
  template <class Type> refcount<Type>& refcount<Type>::operator=(refcount<Type>& rhs)
    {
      if(this!= &rhs)
	{
	  (*_refcount)--;
	  if(*_refcount==0)
	    {
	      if(_P!=0x0)
		delete _P;
	      delete _refcount;
	    };

	  _refcount=rhs._refcount;
	  (*_refcount)++;
	  _P=rhs._P;
	}
      return *this;
    }

  //! Access operator.
  //Type& operator[](long x) { return _P[x]; }
  //const Type& operator[](long x) const {return _P[x];}

  //! Let refcount behave like any other pointer.
  template <class Type> refcount<Type>::operator Type*() { return _P;}
  template <class Type> refcount<Type>::operator const Type*() const { return _P;}
  //! Dereferencing operator.
  //Type* operator->() { return _P;}

  //! Class refvector provides reference counted vectors.
  //! Default constructor;
  template <class TN> refvector<TN>::refvector(): _delim(' ')
    {
      _refcount=new unsigned long(1);
      _P=new vector<TN>();
    }

  //! Copy constructor, but no deep copy.
  template <class TN> refvector<TN>::refvector(refvector<TN>& rhs)
  {
    _refcount=rhs._refcount;
    _delim=rhs._delim;
    (*_refcount)++;
    _P=rhs._P;
  }

  //! Copy constructor necessarily deep to first level.
  template <class TN> refvector<TN>::refvector(const refvector<TN>& rhs)
  {
    _refcount=new unsigned long(1);
    _delim=rhs._delim;
    _P=new vector<TN>();
    *this=rhs;
  }

  //! Initialise the vector to have i entries.
  template <class TN> refvector<TN>::refvector(long i) : _delim(' ')
    {
      _refcount=new unsigned long(1);
      _P=new vector<TN>(i);
    }

  //! Do a deep copy of i into a referenced counter.
  template <class TN> refvector<TN>::refvector(const vector<TN>& i): _delim(' ')
    {
      _refcount=new unsigned long(1);
      _P=new vector<TN>(i);
    }

  //! Do a deep copy of i into a referenced counter.
  template <class TN> refvector<TN>::refvector(long j,const TN* i): _delim(' ')
    {
      _refcount=new unsigned long(1);
      _P=new vector<TN>(j);
      for(long k=0;k<j;k++)
	(*_P)[k]=i[k];
    }

  //! Read a refvector from a stream as written by refvector::write_to()
  template <class TN> refvector<TN>::refvector(istream& IN): _delim(' ')
    {
      const char* string("Full vector:\n\x0");
      char a;
      long i;
      i=0;
      IN.read((char*) &a,sizeof(char));
      while(string[i]==a && string[i]!='\n')
	{
	  i++;
	  IN.read((char*) &a,sizeof(char));
	}
      if(string[i]!='\n')
	throw domain_error("This is not a full vector");
      IN.read((char*) &i,sizeof(long));
      _refcount=new unsigned long(1);
      _P=new vector<TN>(i);
      IN.read((char*) &(*_P)[0],(i*sizeof(TN))/sizeof(char));
    }

  //! Destructor checks for reference count and deletes if necessary.
  template <class TN> refvector<TN>::~refvector()
    {
      (*_refcount)--;
      if(*_refcount==0)
	{
	  if(_P != NULL)
	    delete _P;
	  delete _refcount;
	}
    }

  template <class TN> void refvector<TN>::clear()
    {
      refvector<TN> a;
      (*this)=a;
    }

  //! Compares two refvectors.
  template <class TN> bool refvector<TN>::operator==(const refvector<TN>& a) const
  {
    if(dim()==a.dim())
      {
	long i;
	for(i=0;i<dim();i++)
	  if(!((*_P)[i]==a[i])) return false;

	return true;
      }
    else return false;
  }
  //! Compares two refvectors.
  template <class TN> bool refvector<TN>::operator<=(const refvector<TN>& a) const
  {
    if(dim()==a.dim())
      {
        long i;
        for(i=0;i<dim();i++)
          if(((*_P)[i]>a[i])) return false;

        return true;
      }
    else return false;
  }
  //! Compares two refvectors.
  template <class TN> bool refvector<TN>::operator>=(const refvector<TN>& a) const
  {
    if(dim()==a.dim())
      {
        long i;
        for(i=0;i<dim();i++)
          if(((*_P)[i]<a[i])) return false;

        return true;
      }
    else return false;
  }


  //! Determines if two refvectors are exactly the same.
  template <class TN> template <class T>
  bool refvector<TN>::same(const refvector<T>& a) const
    /*!
     */
    {
      if(_P==(vector<TN>*) &a.vec()) return true;
      else return false;
    }

  //! Assignment without deep copy. Old information is discarded if no references remain.
  template <class TN> refvector<TN>& refvector<TN>::operator=(refvector<TN>& rhs)
  {
    if(this!= &rhs)
      {
	(*_refcount)--;
	if(*_refcount==0)
	  {
	    if(_P!=0x0)
	      delete _P;
	    delete _refcount;
	  };

	_refcount=rhs._refcount;
	(*_refcount)++;
	_P=rhs._P;
      }
    return *this;
  }

  //! Assignment with necessary deep copy. Old information is discarded if no references remain.
  template <class TN> refvector<TN>& refvector<TN>::operator=(const refvector<TN>& rhs)
  {
    if(this!= &rhs)
      {
	(*_refcount)--;
	if(*_refcount==0)
	  {
	    *_refcount=1;
	    _P->resize(rhs->size(),TN());
	  }
	else {
	  _refcount=new unsigned long(1);
	  *_refcount=1;
	  _P=new vector<TN>(rhs->size());
	}
	copy(rhs);
      }
    return *this;
  }

  //! Assignment without deep copy. Old information is discarded if no references remain.
  template <class TN> const refvector<TN>& refvector<TN>::operator=(const refvector<TN>& rhs) const
  {
    if(this!= &rhs)
      {
	(*_refcount)--;
	if(*_refcount==0)
	  {
	    if(_P!=0x0)
	      delete _P;
	    delete _refcount;
	  };

	_refcount=rhs._refcount;
	(*_refcount)++;
	_P=rhs._P;
      }
    return *this;
  }

  template <class TN> refvector<TN>& refvector<TN>::operator=(const vector<TN>& rhs)
    {
      (*_refcount)--;
      if(*_refcount==0)
	{
	  if(_P!=0x0)
	    delete _P;
	  delete _refcount;
	};
      _refcount=new unsigned long(1);
      _P=new vector<TN>;
      (*_P)=rhs;
      return *this;
    }

  template <class TN> refvector<TN>& refvector<TN>::set(long i, TN& rhs)
  {
#ifdef DEBUG
    if(i<0 || i > size())
      throw domain_error("refvector::set(long,TN)");
#endif
    (*_P)[i]=rhs;
    return *this;
  }

  template <class TN> void refvector<TN>::operator>>(ostream& o) const
    {
      long i;
      for(i=0;i<size();i++) {
	o << _delim;
	(*_P)[i] >> o;
      }
    }


  //! Multiply by a refvector.
  template <class TN> refvector<TN>& refvector<TN>::operator*=(const refvector<TN>& a)
    /*!
      This is an elementwise multiplication which results in a new vector. \f$c_i=c_i\cdot a_i.\f$
      See also refvector::element_prod
    */
    {
#ifdef DEBUG
      if((*this)->size() != a->size()) throw domain_error("refvector::operator*=(const refvector<TN>& a) error in dimensions");
#endif
      if(size()<1) return *this;
      double* TP=&(*this)[0];
      const double* aP=&a[0];
      for(long i=0; i<size(); i++)
	TP[i]*=aP[i];
      // (*this)[i]*=a[i];
      return *this;
    }

  //! Multiply by a refvector.
  template <class TN> refvector<TN> refvector<TN>::element_prod(const refvector<TN>& a) const
    /*!
      This is an elementwise multiplication which results in a new vector. \f$c_i=c_i \cdot a_i.\f$
      See refvector::operator*=().
    */
    {
#ifdef DEBUG
      if((*this)->size() != a->size()) throw domain_error("refvector::operator*=(const refvector<TN>& a) error in dimensions");
#endif
      refvector<TN> r(a.dim());
      r.copy((*this));
      r*=a;
      return r;
    }

  //! Scalar product of refvectors.
  template <class TN> TN refvector<TN>::operator*(const refvector<TN>& b) const
    {
#ifdef DEBUG
	if(_P->size() != b->size()) throw domain_error("refvector::operator* error in dimensions");
#endif
	TN r;
	const long N=_P->size();
	if (N<1) return (TN) 0;
	const TN* PP=&((*_P)[0]);
	const TN* bP=&b[0];
	r=0;
	for(unsigned long i=0; i<N; i++) r+=PP[i]*bP[i];
	return r;
    }

  /*
  //! Scalar product of refvectors.
  template <class TN3, class TN2, class TN> TN3& refvector<TN>::operator*(const refvector<TN2>& b, TN3& r) const
  {
#ifdef DEBUG
	if(_P->size() != b->size()) throw domain_error("refvector::operator* error in dimensions");
#endif
	r=0;
	for(unsigned long i=0; i<_P->size(); i++) r+=(*_P)[i]*b[i];
	return r;
	}


  //! Scalar product of refvectors.
  template <class TN3, class TN2, class TN> TN3& refvector<TN>::operator*(const refvector<TN2>& b, TN3& r) const
  {
#ifdef DEBUG
	if(_P->size() != b->size()) throw domain_error("refvector::operator* error in dimensions");
#endif
	TN3 r
  }*/

  //! Add a refvector.
  template <class TN> refvector<TN>& refvector<TN>::operator+=(const refvector<TN>& B)
    {
#ifdef DEBUG
      if(_P->size()!=B->size())
	{
	  throw domain_error(" Incompatible dimensions in refvector& refvector::operator+=(refvector& B)" );
	  exit(1);
	}
#endif
      if(dim()>0) {
    	  double* TP=&(*this)[0];
    	  const double* BP=&B[0];
    	  const long N=size();
    	  for(long i=0;i < N; i++) TP[i]+=BP[i];
      }
      //(*this)[i]+=B[i];
      return *this;
    }

  //! Add two refvectors.
  template <class TN> refvector<TN> refvector<TN>::operator+(const refvector<TN>& B) const
    {
#ifdef DEBUG
      if(_P->size()!=B->size())
	{
	  throw domain_error(" Incompatible dimensions in refvector refvector::operator+(refvector& B)" );
	  exit(1);
	}
#endif
      refvector<TN> r(_P->size());
      if(dim()>0) {
    	  TN* RP=&r[0];
    	  const TN* VP=&(*this)[0];
    	  const TN* BP=&B[0];
    	  const long N=_P->size();
    	  for(long i=0;i < N; i++) RP[i]=VP[i]+BP[i];
    	  // r[i]=(*this)[i]+B[i];
      }
      return r;
    }

  //! Subtract a refvector.
  template <class TN> refvector<TN>& refvector<TN>::operator-=(const refvector<TN>& B)
    {
#ifdef DEBUG
      if(_P->size()!=B->size())
	{
	  throw domain_error(" Incompatible dimensions in refvector& refvector::operator-=(refvector& B)" );
	  exit(1);
	}
#endif
      if(dim()>0) {
    	  const TN* BP=&B[0];
    	  TN* TP=&(*this)[0];
    	  const long N=_P->size();
    	  for(unsigned long i=0;i < N; i++) TP[i]-=BP[i];
      }
      return *this;
    }

  //! Subtract two refvectors.
  template <class TN> refvector<TN> refvector<TN>::operator-(const refvector<TN>& B) const
    {
#ifdef DEBUG
      if(_P->size()!=B->size())
	{
	  throw domain_error(" Incompatible dimensions in refvector refvector::operator-(refvector& B)" );
	  exit(1);
	}
#endif
      refvector<TN> r(_P->size());
      const long N=_P->size();
      if(N>0) {
    	  TN* RP=&r[0];
    	  const TN* TP=&(*this)[0];
    	  const TN* BP=&B[0];
    	  for(unsigned long i=0;i < N; i++) RP[i]=TP[i]-BP[i];
    	  //r[i]=(*this)[i]-B[i];
      }
      return r;
    }

  //! Subtract two refvectors.
  template <class TN> refvector<TN>& refvector<TN>::subtract(const refvector<TN>& B,
							     refvector<TN>& r) const
  {
#ifdef DEBUG
    if(_P->size()!=B->size())
      throw domain_error(" Incompatible dimensions in refvector refvector::operator-(refvector& B)" );

    if(_P->size()!=r.size())
      throw domain_error(" Incompatible dimensions in refvector refvector::operator-(refvector& B)" );
#endif
    for(unsigned long i=0;i < _P->size(); i++) r[i]=(*this)[i]-B[i];
      return r;
  }

  //! Add a sparse vector.
  template <class TN> refvector<TN>& refvector<TN>::operator+=(const sparse_vector<TN>& a)
    {
#ifdef DEBUG
      if(a.dim() != _P->size())
	throw domain_error("Incompatible dimensions in refvector<TN> refvector::operator+=(const sparse_vector<TN>* a)");
      try {
#endif
	long i;
	for(i=0;i<a.size();i++)
	  (*_P)[a.index(i)]+=a[i];
	return (*this);
#ifdef DEBUG
      } catch(domain_error e) {
	cerr << e.what() << endl;
	throw domain_error("called from refvector<TN> refvector::operator+=(const sparse_vector<TN>* a)");
      }
#endif
    }

  //! Multiply by an element.
  template <class TN> refvector<TN>& refvector<TN>::operator*=(const TN x)
    {
      const long i=_P->size();
      if(i<1) return *this;
      long j;
      TN* TP=&(*this)[0];
      for(j=0; j<i; j++) TP[j]*=x;
      return *this;
    }

  //! Multiply by an element into the vector.
  template <class TN> template <class TN2>
  refvector<TN>& refvector<TN>::operator*=(const TN2& a)
    {
      const long N=_P->size();
      if(N<1) return *this;
      TN* TP=&((*this)[0]);
      for(long i=0; i<N;i++)
    	  TP[i]*=a;
      return *this;
    }

  //! Multiply by an element.
  template <class TN> template <class TN2>
    refvector<TN> refvector<TN>::operator*(const TN2 x) const
    {
      refvector<TN> r((*this).dim());
      return multiply(x,r);
    }

  //! Multiply by an element.
  template <class TN> template <class TN2>
    refvector<TN>& refvector<TN>::multiply(const TN2 x,refvector<TN>& a) const
    /*!
      This may pose problems in some settings when TN*TN2 is not defined.
    */
    {
#ifdef DEBUG
	  if(a.dim()!=dim())
    	  throw domain_error("refvector::multiply(const TN2 x, ...) called with incorrect output initialization.");
#endif
      const long i=_P->size();
      if(i>0) {
    	  const long remainder=i % 4;
    	  const long aligned=i-remainder;
    	  const TN* TP=&(*this)[0];
    	  TN* aP=&a[0];
    	  long j;

    	  for(j=0; j<i; j++) aP[j]+=TP[j]*x;
      }
      else
    	  a.copy(*this);
      return a;
    }
  //! Divide by a refvector.
  template <class TN> refvector<TN>& refvector<TN>::operator/=(const refvector<TN>& a)
    /*!
      This is an elementwise division which results in a new vector. \f$c_i=c_i\cdot a_i.\f$
      See also refvector::element_prod
    */
    {
#ifdef DEBUG
      if((*this)->size() != a->size()) throw domain_error("refvector::operator/=(const refvector<TN>& a) error in dimensions");
#endif
      if(size()<1) return *this;
      double* TP=&(*this)[0];
      const double* aP=&a[0];
      for(long i=0; i<size(); i++)
        TP[i]/=aP[i];
      return *this;
    }

  //! Divide by an element.
  template <class TN> template <class TN2>
    refvector<TN> refvector<TN>::operator/(const TN2 x) const
    /*!
      This may pose problems in some settings when TN*=TN2 is not defined.
    */
    {
      const TN2 divisor=(TN2) 1/x;
      return (*this)*divisor;
    }

  //! Simple display of refvector.
  template <class TN> void refvector<TN>::display(ostream& outs) const
    {
      long val=0;
      while(val < size()) {
	outs << "(" << val <<") = ";
	linear_algebra::display((*this)[val++],outs);
	outs << " ";//endl;
      }
      return;
    }

  //! Change all values to absolute value
  template <class TN> refvector<TN>& refvector<TN>::abs_inplace()
     {
        for(long i = 0; i<dim(); i++) (*this)[i]=std::abs((*this)[i]);
        return *this;
     }

  //! Change all values to absolute value
  template <class TN> refvector<TN> refvector<TN>::abs() const
     {
        refvector<TN> r(*this);
        return r.abs_inplace();
     }

  //! Returns the maximum element.
  template <class TN> TN refvector<TN>::max() const
    {
      TN max;
      if(_P->size() == 0) return 0;
      long i=0;
      max=(*this)[i];i++;
      while(i<_P->size())
	{
	  if(max<(*this)[i]) max=(*this)[i];
	  i++;
	}
      return max;
    }

  //! Returns the sum.
  template <class TN> TN refvector<TN>::sum(TN zero=0) const
    {
       if (dim()<1) return zero;
       TN a=(*this)[0];
       for(long i=1; i<dim(); i++) a+=(*this)[i];
       return a;
    }

  //! Normalise the vector in accordance with the \f$\|\cdot\|_2 \f$ 2-norm.
  template <class TN> void refvector<TN>::normalise()
    {
      TN a=(*this)*(*this);
      a=(TN) 1/sqrt(a);
      *this*=a;
    }

  //! Access operator.
  //TN& operator[](long x) { return _P[x]; }
  //const TN& operator[](long x) const {return _P[x];}

  /*
    vector<TN>::iterator begin() { return _P->begin(); }
    vector<TN>::const_iterator begin() const { return _P->begin(); }
    vector<TN>::iterator end() { return _P->end(); }
    vector<TN>::const_iterator end() const { return _P->end(); }
  template <class TN>
    typename vector<TN>::iterator refvector<TN>::erase(typename vector<TN>::iterator a) { return _P->erase(a); }
    vector<TN>::iterator insert(vector<TN>::iterator a, TN b) { return _P->insert(a,b); }
    void push_back(TN a) { _P->push_back(a); }*/

  //! Direct access operator.
  template <class TN> TN& refvector<TN>::operator[](long i) {
#ifdef DEBUG
    if(i<0 || i>=(long) _P->size()) {
      stringstream ss;
      ss.str("");
      ss << " Access error refvector::operator[](const long " << i << " > " << _P->size() << " )";
      throw domain_error(ss.str());
    }
#endif
    return (*_P)[i];}

  //! Direct access operator.
  template <class TN> const TN& refvector<TN>::operator[](long i) const {
#ifdef DEBUG
    if(i<0 || i>=(long) _P->size()) {
      stringstream ss;
      ss.str("");
      ss << " Access error refvector::operator[](const long " << i << " > " << _P->size() << " ) const";
      throw domain_error(ss.str());
    }
#endif
    return (*_P)[i];}

  //! Let refvector behave like any other pointer.
  //operator vector<TN>*() { return _P;}
  //operator const vector<TN>*() const { return _P;}
  //! Dereferencing operator.
  template <class TN> vector<TN>* refvector<TN>::operator->() { return _P;}
  //! Dereferencing operator.
  template <class TN> const vector<TN>* refvector<TN>::operator->() const { return _P;}

  //! Return the vector.
  template <class TN> vector<TN>& refvector<TN>::vec() { return *_P;}
  //! Return the vector.
  template <class TN> const vector<TN>& refvector<TN>::vec() const {return *_P;}

  //! Do a copy of depth j of i into a referenced counter.
  template <class TN> refvector<TN>& refvector<TN>::copy(const refvector<TN>& i, const long j)
  {
#ifdef DEBUG
    try {
#endif
    (*_refcount)--;
    if(*_refcount==0 && j==0)
      {
	if(_P != 0x0)
	  delete _P;
	delete _refcount;
	_P=i._P;
	_refcount=i._refcount;
	(*_refcount)++;
      }
    else
      if(j!=0)
	{
	  (*_refcount)=1;
	  _P->resize(i->size());
	  if(j>0 && i.size()>0) {
	    TN* PP=&((*_P)[0]);
	    const TN* iP=&i[0];
	    const long N=i.size();
	    for(long k=0;k<N;k++)
	      linear_algebra::copy(PP[k],iP[k],j-1);
	  }
	  else (*_P)=(*(i._P));
	}
      else
	{
	  _P=i._P;
	  _refcount=i._refcount;
	  (*_refcount)++;
	}
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by refvector<TN>::copy(const refvector<TN>& i, const long j) ");
    }
#endif
    return *this;
  }
  //! Copy i with full depth.
  template <class TN> refvector<TN>& refvector<TN>::copy(const refvector<TN>& i)
  {
#ifdef DEBUG
    try {
#endif
    (*_refcount)--;
    if(*_refcount==0)
      {
	(*_refcount)++;
	_P->resize(i->size());
      }
    else
      {
	_refcount=new unsigned long(1);
	(*_refcount)=1;
	_P=new vector<TN>(i->size());
      }
    const long N=i.size();
    if(N>0) {
      TN* PP=&((*_P)[0]);
      const TN* iP=&i[0];
      for(long k=0;k<N;k++)
	linear_algebra::copy(PP[k],iP[k]);
    }
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by refvector<TN>::copy(const refvector<TN>& i) ");
    }
#endif
    return *this;
  }

  //! Do a copy of depth j of i into a referenced counter.
  template <class TN> const refvector<TN>& refvector<TN>::copy(const refvector<TN>& i, const long j) const
  {
    (*_refcount)--;
    if(*_refcount==0 && j==0)
      {
	if(_P != 0x0)
	  delete _P;
	delete _refcount;
	_P=i._P;
	_refcount=i._refcount;
	(*_refcount)++;
      }
    else
      if(j!=0)
	{
	  (*_refcount)=1;
	  _P->resize(i->size());
	  if(j>0)
	    for(long k=0;k<i.size();k++)
	      linear_algebra::copy((*_P)[k],i[k],j-1);
	  else (*_P)=(*(i._P));
	}
      else
	{
	  _P=i._P;
	  _refcount=i._refcount;
	  (*_refcount)++;
	}
    return *this;
  }

  //! Copy i with depth 1.
  template <class TN> const refvector<TN>& refvector<TN>::copy(const refvector<TN>& i) const
  {
    return copy(i,1);
  }

  //! Returns the dimension of the vector.
  template <class TN> long refvector<TN>::dim() const { return _P->size(); }
  //! Returns the length of the vector.
  template <class TN> long refvector<TN>::size() const { return _P->size(); }

  template <class TN> refvector<TN>& refvector<TN>::push_back(const TN& a) { _P->push_back(a); return *this;}

  template <class TN> void refvector<TN>::resize(long n) { _P->resize(n); }
  template <class TN> void refvector<TN>::resize(long n, const TN& N) { _P->resize(n,N); }

  template <class TN> void refvector<TN>::insert(long n, const TN& a) {_P->insert(_P->begin()+n,a);}

  template <class TN> void refvector<TN>::erase(long a)
    {
#ifdef DEBUG
      if(a<0 || a+_P->begin()>_P->end())
	throw domain_error("refvector::erase() called with illegal index");
#endif
      _P->erase(_P->begin()+a);
    }

  //! Write a refvector to a stream.
  template <class TN> void refvector<TN>::write_to(ostream& OUTFILE)
    {
      TN a;
      long j=dim();
      OUTFILE << "Full vector:" << endl;
      OUTFILE.write((char*) &j,sizeof(long));
      for(j=0;j<dim();j++)
	{
	  a=(*_P)[j];
	  OUTFILE.write((char*) &a, sizeof(TN));
	}
    }
  //! Set the refvector to the zero vector of current dimension.
  template <class TN> refvector<TN>& refvector<TN>::zero()
  {
    const long N=dim();
    TN* PP=&((*_P)[0]);
    for(long i=0;i<N;i++) linear_algebra::zero((PP[i]));
    return (*this);
  }

  //! Determines whether the refvector contains a certain element and returns its first occurence position.
  template <class TN> long refvector<TN>::contains(const TN& a) const
  {
    long i;
    for(i=0;i<dim();i++)
      if((*_P)[i]==a)
	return i;
    return -1;
  }

  //! Appends another refvector to the end.
  template <class TN> refvector<TN>& refvector<TN>::concat(const refvector<TN>& a)
  {
    long oldsize=size();
    resize(oldsize+a.size());
    for(long i=0;i<a.size();i++)
      {
	(*this)[oldsize+i]=a[i];
      }
    return *this;
  }

} // end of linear_algebra::

#include "sparse_vector.h"
#endif
/*! @}*/
