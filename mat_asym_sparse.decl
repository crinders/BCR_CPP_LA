/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file mat_sym_sparse.decl
  \brief Provide declarations for antisymmetric sparse matrices.
*/
#ifndef _MAT_ASYM_SPARSE
#define _MAT_ASYM_SPARSE
#include <vector>
#include "linear_algebra.decl"
#include "asymmetric.decl"

namespace linear_algebra
{
  //! mat_asym_sparse packs sparse column matrices into a comfortable format.
  template<class TN>
  class mat_asym_sparse : public asymmetric<TN>
  /*!
    The sparse matrix is packed as a sparse vector of sparse vectors.
    Only the columns which are not empty are indexed and kept in memory.
    The sparse vector part takes care of the rows.
  */
  {
    friend class mat_sparse<TN>;
    friend class mat_full<TN>;
    using matrix<TN>::_rows;
    using matrix<TN>::_cols;
    using matrix<TN>::_size;
    
  public:
    // Constructors
    
    //! Default constructor
    mat_asym_sparse();
    
    //! Constructor of dimension n with uninitialised values.
    mat_asym_sparse(long n);
    
    //! Constructor of dimension n with initialisation of values.
    mat_asym_sparse(long n, const vector<long>& scols, const vector<sparse_vector<TN> >& vals);
    
    //! Constructor of dimension n with initialisation of values.
    mat_asym_sparse(long n, const vector<long>& scols, const refvector<sparse_vector<TN> >& vals);
    
    //! Constructor of dimension n with initialisation of values.
    mat_asym_sparse(long n, const refvector<long>& scols, const refvector<sparse_vector<TN> >& vals);
    
    //! Constructor of dimension n with initialisation of values.
    mat_asym_sparse(long n, const refvector<long>& scols, const vector<sparse_vector<TN> >& vals);
    
    //! Constructor to read a stored matrix from a stream. Aimed at retrieving information from a binary save with mat_sym_sparse::operator>>().
    mat_asym_sparse(istream& IN);
    
    // Operators  *******************************************************************************************************
    
    mat_asym_sparse<TN>& operator=(const mat_asym_sparse<TN>& b);
    
    //! Access operator.
    TN operator()(const long x, const long y) const;
    
    //! Direct access operator.
    const sparse_vector<TN> operator[](const long i) const;
    
    //! Direct access operator.
    sparse_vector<TN>& operator[](const long i);
    
    //! Set operation for safe assignments.
    void set(const long x, const long y, TN value);
    
    //! Multiplies a symmetric sparse matrix with an object.
    mat_asym_sparse<TN> operator*(const TN& a) const;
    
#ifdef SPARSE_SPARSE_FULL
    //! Multiply by a full matrix.\test
    mat_sparse<TN> operator*(mat_full<TN> A) const;
#else
    
    //! Multiply by a full matrix.\test
    mat_full<TN> operator*(mat_full<TN> A) const;

    //! Multiply by a full matrix.\test
    mat_full<TN>& multiply(mat_full<TN> A, mat_full<TN>& r) const;

#endif // SPARSE_SPARSE_FULL
    
#ifdef SPARSE_SPARSE_FULL
    //! Multiply a symmetric sparse matrix with a full vector into a sparse vector./test
    sparse_vector<TN> operator*(const refvector<TN> v) const;
#else
    //! Multiply a symmetric sparse matrix with a full vector into a full vector./test
    refvector<TN>& multiply(const refvector<TN>& v,refvector<TN>& r) const;
    
    //! Multiply a symmetric sparse matrix with a full vector into a full vector./test
    refvector<TN> operator*(const refvector<TN>& v) const;
#endif //SPARSE_SPARSE_FULL
    
    //! Multiply by an object of TN into left side.
    mat_asym_sparse<TN>& operator*=(const TN& x);
    
    //! Multiplies an element by a number
    mat_asym_sparse<TN>& multiply(long x, long y, TN a);

    //! Multiply an anti-symmetric sparse matrix with a full matrix into a full matrix./test
    mat_full<TN>& multiply(const mat_asym_full<TN>& B, mat_full<TN>& r) const;
    
    //! Multiply a symmetric sparse matrix with a full vector into a full vector./test
    mat_full<TN> operator*(const mat_asym_full<TN>& B) const;
    
    //! return a symmetric full matrix=\f$ g\cdot g^\ast\f$.
    mat_sym_full<TN> aa_t() const;

    //! Divide by an object of TN into left side. This is very slow and not advisable.
    mat_asym_sparse& operator/=(const TN& x);
    
    //! Multiply a sparse symmetric matrix with a sparse vector.
    sparse_vector<TN> operator*(const sparse_vector<TN>& v) const;
  
    //! Multiply a sparse symmetric matrix with a sparse matrix.
    mat_sparse<TN> operator*(const mat_sparse<TN>& B) const;
    
    //! Add a matrix into left object.
    mat_asym_sparse<TN>& operator+=(const mat_asym_sparse<TN>& B);
    
    //! Subtract a matrix into left object.
    mat_asym_sparse<TN>& operator-=(const mat_asym_sparse<TN>& B);
    
    // Normal member functions ***************************************************************************************
    
    bool is_col_nonzero(const long x, long &i) const;
    
    //! Checks whether an access is non zero.
    bool is_nonzero(long x, long y, long &j, long &k) const;
    
    //! Adds a value.
    bool add( long x, long y, const TN z);
    
    bool is_empty() const;
    
    //! Returns size of non-zero entries in _svals.
    long size() const;
    
    //! Output matrix in binary format into a stream. Aimed at file storage.
    void operator>>(ostream& OUT) const;
    
    //! Deletes all entries that are smaller in magnitude than x.
    mat_asym_sparse<TN>& prune(const TN x=0);
    
    //! Finds the maximum element.
    TN max_element() const;
    
    //! Simple display of matrix.
    bool display() const;
    
    //! Extracts a block from a column/row
    vector<TN> extract_full_col(const long column, const long xmin, const long xmax) const;
  
    //! Extracts a block from a column/row
    vector<TN> extract_full_col(const long column, const long xmin=0) const;
    
    //! Extracts a full antisymmetric block.
    mat_asym_full<TN> extract_full_asym_block(long xmin, long xmax) const;
    
    //! Extracts a full antisymmetric block.
    mat_asym_full<TN> extract_full_asym_block(long xmin=0) const;
    
    //! Extracts a block from anywhere in the matrix. \test should work, but ...
    mat_sparse<TN> extract_sparse_block(long xmin, long xmax, long ymin, long ymax) const;
    
    //! Extracts a block from a column/row
    sparse_vector<TN> extract_sparse_col(const long column, const long xmin=0) const;
    
    //! Extracts a sparse column up to the diagonal.
    sparse_vector<TN> extract_sparse_lower_col(const long column, const long xmin, const long xmax) const;
    
    //! Extracts a block from a column/row
    sparse_vector<TN> extract_sparse_lower_col(const long column, const long xmin=0) const;
    
    //! Tolerance for multiplication with a sparse vector.
    TN _prune_tol;
    
    //! Return the density of non-zero elements.
    double density() const;
    
//#include "mat_sym_sparse_eigen"
    
    //! Sets matrix to an empty matrix releasing all associated memory.
    void clear();
    
    //! Sets a matrix to zero retaining the dimensions.
    mat_asym_sparse<TN>& zero();

    //! Returns the ith nonzero column number.
    long index(long i) const;
    
    //! This is the unitary transform \f$U^\ast AU\f$.
    mat_asym_full<TN>& utu(const mat_full<TN>& U, mat_asym_full<TN>& R) const;
    
    //! This is the unitary transform U^tAU.
    mat_asym_full<TN> utu(const mat_full<TN>& U) const;
    
  private:
    
    //! Vector of column indices.
    refvector<long> _scols;
    
    //! Vector of sparse columns.
    refvector<sparse_vector<TN> > _svals;
    //! State of matrix.
    bool _empty;
  };//end mat_sym_sparse
  
} // linear_algebra::  
#endif
/*! @}*/
