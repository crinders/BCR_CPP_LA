/*! @{*/
/*! \file invert_sym.h \brief Inversion of symmetric matrices.
 */

#include "linear_algebra.h"
#include <vector>
#include <iostream>

using namespace linear_algebra;
using namespace std;

//! Invert a symmetric full matrix and multiply by a vector.
inline refvector<double>& invert_sym(const mat_sym_full<double>& S1,const refvector<double>& v, refvector<double>& r2)
  /*!
    A symmetric matrix can be transformed to a tridiagonal matrix via a householder transformation (\f$O(n^2)\f$). A following gaussian elimination of the tridiagonal matrix (O(n)). Can be used to solve for the inverted matrix.
  */
{
#ifdef DEBUG
  if(v->size() != S1.cols())
    throw domain_error("Incompatible dimensions in linear_algebra::invert_sym(mat_sym_full<double>& S,const refvector<double>& v,r): v/S");

  if(r2.dim() != S1.rows())
    throw domain_error("Incompatible dimensions in linear_algebra::invert_sym(mat_sym_full<double>& S,const refvector<double>& v,r): r2/S");
  try {
#endif
    mat_full<double> U;
    mat_sym_full<double> S;
    S.copy(S1);
    
    refvector<double> diagonal(S.cols());
    refvector<double> supdiagonal(S.cols());
    refvector<double> subdiagonal(S.cols());
    U=S.householder(diagonal,supdiagonal);
    
    // Gaussian elimination for tridiagonal matrix.
    refvector<double> r(r2.dim());
    U.multiply(v,r);
    
    // Go down.
    
    // We normalized the rows with respect to the diagonal.
    long i;
    
    for(i=0;i<S.cols()-1;i++)
      {
	diagonal[i]=(double) 1/diagonal[i];
	subdiagonal[i+1]=supdiagonal[i+1];
	supdiagonal[i+1]*=diagonal[i];
	diagonal[i+1]-=subdiagonal[i+1]*supdiagonal[i+1];
	r[i]*=diagonal[i];
	r[i+1]-=r[i]*subdiagonal[i+1];
      }
    
    diagonal[i]=(double) 1/diagonal[i];
    r[i]*=diagonal[i];
    
    // Go up. The diagonal is all 1s.
    
    for(i=S.cols()-1;i>0;i--)
      r[i-1]-=r[i]*supdiagonal[i];
    
    // Multiply with the transpose of U.
    
    for(i=0;i<U.cols();i++)
      r2[i]=U[i]*r;
    
    return r2;
    
#ifdef DEBUG
  } catch (domain_error e) {
    cerr << e.what() << endl;
    throw domain_error("called from invert_sym");
  }
#endif
}

//! Invert a symmetric full matrix and multiply by a vector.
inline refvector<double> invert_sym(const mat_sym_full<double>& S1,const refvector<double>& v)
{
  refvector<double> r(v.dim());
  return invert_sym(S1,v,r);
}


//! Invert a symmetric full matrix.
inline mat_sym_full<double> invert_sym(mat_sym_full<double>& S)
  /*!
    A symmetric matrix can be transformed to a tridiagonal matrix via a householder transformation (\f$O(n^2)\f$). A following gaussian elimination of the tridiagonal matrix (O(n)) can be used to solve for the inverted matrix.
  */
{
#ifdef DEBUG
  try {
#endif
  mat_full<double> U;
  refvector<double> diagonal(S.cols());
  refvector<double> supdiagonal(S.cols());
  refvector<double> subdiagonal(S.cols());
  U=S.householder(diagonal,supdiagonal);
  
  // Let's revert S.

  S.utu(U);
  
  mat_full<double> I2;
  
  // Gaussian elimination for tridiagonal matrix.

  I2.copy(U,2);
  
  // Go down.

  // We normalized the rows with respect to the diagonal.
  long i;
  
  for(i=0;i<S.cols()-1;i++)
    {
      diagonal[i]=(double) 1/diagonal[i];
      subdiagonal[i+1]=supdiagonal[i+1];
      supdiagonal[i+1]*=diagonal[i];
      diagonal[i+1]-=subdiagonal[i+1]*supdiagonal[i+1];
    }
  
  diagonal[i]=(double) 1/diagonal[i];

  long j;
  for(j=0;j<I2.cols();j++)
    {
      for(i=0;i<S.cols()-1;i++)
	{
	  I2[j][i]*=diagonal[i];
	  I2[j][i+1]-=I2[j][i]*subdiagonal[i+1];
	}
      I2[j][i]*=diagonal[i];
    }

  // Go up. The diagonal is all 1s.
  
  for(j=0;j<I2.cols();j++)
    for(i=S.cols()-1;i>0;i--)
      I2[j][i-1]-=I2[j][i]*supdiagonal[i];

  // Multiply with the transpose of U.

  for(j=0;j<I2.cols();j++)
    {
      refvector<double> r2(I2.rows());
      for(i=0;i<U.cols();i++)
	r2[i]=U[i]*I2[j];
      I2[j]=r2;
    }

  return I2.sym_upper_triangle();

#ifdef DEBUG
  } catch (domain_error e) {
    cerr << e.what() << endl;
    throw domain_error("called from invert_sym");
  }
#endif
}
/*! @}*/
