/*! \addtogroup LA Linear Algebra Package with Sparse Implementations */
/*! @{*/
/*! \file mat_sym_sparse.h
  \brief Provide functionality for symmetric sparse matrices.
  
*/
#ifndef _MAT_ASYM_SPARSE_H
#define _MAT_ASYM_SPARSE_H
#include <vector>
#include "linear_algebra.decl"
#include "mat_asym_sparse.decl"
#include "refcount.decl"
#include "sparse_vector.decl"
#include "mat_full.decl"
#include "mat_sparse.decl"
#include "mat_sym_full.decl"
#include "mat_asym_full.decl"

namespace linear_algebra
{
  // Constructors

  //! Default constructor
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse() : _prune_tol(0) { _rows=_cols=_size=0;}

  //! Constructor of dimension n with uninitialised values.
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse(long n): _prune_tol(0), _empty(true) {_size=0; _rows=_cols=n;}
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse(long n, const vector<long>& scols, const vector<sparse_vector<TN> >& vals) : _prune_tol(0)
    /*!
      \param n Dimension of matrix.
      \param srows Vector of row indices.
      \param scols Vector of column indices.
      \param vals Vector of values.

      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
      There is no integrity check.
    */
  {
    _rows=_cols=n;
    _svals=vals;
    _scols=scols;
    _empty=false;
    _size=vals.size();

#ifdef DEBUG
    if(_size !=scols.size() ) throw domain_error( " mat_asym_sparse::mat_asym_sparse(long n, const vector<long>& scols, const vector<sparse_vector<TN> >& vals): Inconsistent sizes in vals/scols. " );
#endif // debug
  }
  
  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse(long n, const vector<long>& scols, const refvector<sparse_vector<TN> >& vals) : _prune_tol(0)
    /*!
      \param n Dimension of matrix.
      \param srows Vector of row indices.
      \param scols Vector of column indices.
      \param vals reference counted Vector of values.

      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
      There is no integrity check.
    */
  {
    _rows=_cols=n;
    _svals=vals;
    _scols=scols;
    _empty=false;
    _size=vals->size();

#ifdef DEBUG
    if(_size !=scols.size() ) throw domain_error( " mat_asym_sparse::mat_asym_sparse(long n, const vector<long>& scols, const refvector<sparse_vector<TN> >& vals): Inconsistent sizes in vals/scols. " );
#endif // debug
  }

  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse(long n, const refvector<long>& scols, const refvector<sparse_vector<TN> >& vals) : _prune_tol(0)
    /*!
      \param n Dimension of matrix.
      \param srows Vector of row indices.
      \param scols Reference counted vector of column indices.
      \param vals Reference counted vector of values.

      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
      There is no integrity check.
    */
  {
    _rows=_cols=n;
    _svals=vals;
    _scols=scols;
    _empty=false;
    _size=vals->size();

#ifdef DEBUG
    if(_size !=scols->size() ) throw domain_error( " mat_asym_sparse::mat_asym_sparse(long n, const refvector<long>& scols, const refvector<sparse_vector<TN> >& vals): Inconsistent sizes in vals/scols. " );
#endif // debug
  }

  //! Constructor of dimension n with initialisation of values.
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse(long n, const refvector<long>& scols, const vector<sparse_vector<TN> >& vals) : _prune_tol(0)
    /*!
      \param n Dimension of matrix.
      \param srows Vector of row indices.
      \param scols Reference counted vector of column indices.
      \param vals Vector of values.

      Each value has an associated row and column index in srows and scols. 
      Therefore they all have the same size.
      There is no integrity check.
    */
  {
    _rows=_cols=n;
    _svals=vals;
    _scols=scols;
    _empty=false;
    _size=vals.size();

#ifdef DEBUG
    if(_size !=scols->size() ) throw domain_error( " mat_asym_sparse::mat_asym_sparse(long n, const refvector<long>& scols, const vector<sparse_vector<TN> >& vals): Inconsistent sizes in vals/scols. " );
#endif // debug
  }

  //! Constructor to read a stored matrix from a stream. Aimed at retrieving information from a binary save with mat_asym_sparse::operator>>().
  template <class TN> mat_asym_sparse<TN>::mat_asym_sparse(istream& IN) : _prune_tol(0)
    /*!
      The resultant vector is pruned to default pruning tolerance = 0.
      This should be completely superfluous but in case a different program not using this class
      produces the matrix as input we prune.
    */
  {
    long rs,cs,j;
    IN.seekg(25,ios::cur);
    IN.read((char*) &_rows,sizeof(long));
    _cols=_rows;
    IN.read((char*) &cs,sizeof(long));
    refvector<long> cols;
    long i[cs];

    IN.read((char*) i, sizeof(long)*cs);
    for(j=0;j<cs;j++) cols->push_back(i[j]);
    refvector<sparse_vector<TN> > sv(cs);
    long svi=0;
    while(svi<sv->size())
      {
	IN.read((char*) &rs,sizeof(long));
	long ci[rs];
	
	TN cv[rs];
	IN.read((char*) ci, sizeof(long)*rs);
	IN.read((char*) cv, sizeof(TN)*rs);
	refvector<long> civ;
	for(j=0;j<rs;j++) civ->push_back(ci[j]);
	refvector<TN> cvv;
	for(j=0;j<rs;j++) cvv->push_back(cv[j]);
	sparse_vector<TN> r(_rows,civ,cvv);
	//delete ci;
	//delete cv;
	sv[svi]=r;
	svi++;
      }
    _svals=sv;
    _size=_svals->size();
    _scols=cols;
    if( cs == 0) _empty=true;
    else _empty=false;
#ifdef DEBUG
    if( _size!= _scols->size()) throw domain_error(" mat_asym_sparse::mat_asym_sparse(istream& IN): inconsistent sizes for _scols and _svals");
#endif // debug
    prune();
  }
  
  // Operators  *******************************************************************************************************

  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::operator=(const mat_asym_sparse<TN>& b)
  {
    (*this)._scols=b._scols;
    (*this)._svals=b._svals;
    (*this)._empty=b._empty;
    (*this)._rows=b._rows;
    (*this)._cols=b._cols;
    (*this)._size=b._size;
    return *this;
  }
  
  //! Access operator.
  template <class TN> TN mat_asym_sparse<TN>::operator()(const long x, const long y) const
  {
    long i,j,k,l;
    TN sign=(TN) 1;
    if(x<y) { i=y; j=x; sign=(TN) -1;}
    else {i=x; j=y;}
    if( i<_cols)
      {
	if(is_nonzero(i,j,k,l)) { return sign*_svals[k][l];}
	else
	  return 0;
      }
    throw domain_error( " Illegal access outside of dimension in symmetric sparse " );
    exit(1);
  }

  //! Direct access operator.
  template <class TN> const sparse_vector<TN> mat_asym_sparse<TN>::operator[](const long i) const
  {
#ifdef DEBUG
    if(i<0 || i>= _scols.size())
      throw domain_error("Illegal access outside of dimension in mat_asym_sparse::operator[](const long i)");
#endif
    return _svals[i];
  }
  
  //! Direct access operator.
  template <class TN> sparse_vector<TN>& mat_asym_sparse<TN>::operator[](const long i)
  {
#ifdef DEBUG
    if(i<0 || i>= _scols.dim())
      throw domain_error("Illegal access outside of dimension in mat_asym_sparse::operator[](const long i)");
#endif
    return _svals[i];
  }
  
  //! Set operation for safe assignments.
  template <class TN> void mat_asym_sparse<TN>::set(const long x, const long y, TN value)
  {
    long i,j,k;
    TN sign=(TN) 1;
    if(x<y) { i=y; j=x; sign=(TN) -1;}
    else {i=x; j=y;}
    if( i<_cols)
      {
	if(is_col_nonzero(i,k)) 
	  {
	    _svals[k](j)=value*sign;
	  }
	else
	  {
	    sparse_vector<TN> v;
	    _svals->insert(_svals->begin()+k,v);
	    _scols->insert(_scols->begin()+k,i);
	    _svals[k]._size=_rows;
	    _svals[k](j)=value*sign;
	  }
	_empty=false;
      }
    else 
      throw domain_error( " Illegal access outside of dimension in mat_asym_sparse::set(const long x, const long y, TN value)" );
  }

  //! Multiplies an element by a number
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::multiply(long x, long y, TN z)
  {
#ifdef DEBUG
    try {
#endif
      long i;
      TN sign=(TN) 1;
      if(x<y) { i=x; x=y; y=i;sign=(TN) -1;}
      if(x<_cols)
	{
	  if(is_col_nonzero(x,i)) 
	    {
	      _svals[i](y)*=z;
	      _svals[i](y)*=sign;
	    }
	  return *this;
	}
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by  mat_asym_sparse::multiply(long,long,T) ");
    }
    throw domain_error( " Outside of dimension of mat_asym_sparse::multiply(long,long, T) " );
#endif
  }

  //! Multiplies an antisymmetric sparse matrix with an object.
  template <class TN> mat_asym_sparse<TN> mat_asym_sparse<TN>::operator*(const TN& a) const 
  {
    refvector<sparse_vector<TN> > r(_scols->size());
    long i;
    for(i=0;i<_scols->size();i++)
      r[i]=_svals[i]*a;
    mat_asym_sparse<TN> r2(_rows,_scols.vec(),r);
    return r2;
  }
  
#ifdef SPARSE_SPARSE_FULL
  //! Multiply by a full matrix.\test
  template <class TN> mat_sparse<TN> mat_asym_sparse<TN>::operator*(mat_full<TN> A) const
  {
#ifdef DEBUG
    if ( A.rows() != _cols) 
      throw domain_error("Incompatible dimensions in mat_asym_sparse::operator*(mat_full<TN> A) const");
#endif
    refvector<sparse_vector<TN> > sv(A.cols());
    refvector<long> si(A.cols());
    long i;
    
    for(i=0; i< A.cols(); i++) sv[i]=(*this)*A[i];
    for(i=0; i< A.cols(); i++) si[i]=i;
    
    mat_sparse<TN> r(A.cols(), _rows, sv, si);
    return r;
  }
#else
  //! Multiply by a full matrix.\test
  template <class TN> mat_full<TN>& mat_asym_sparse<TN>::multiply(mat_full<TN> A, mat_full<TN>& r) const
  {
#ifdef DEBUG
    if ( A.rows() != _cols) 
      throw domain_error("Incompatible dimensions in mat_asym_sparse::operator*(mat_full<TN> A) const");
    if ( r.rows() != _rows || r.cols() != A.cols()) 
      throw domain_error("Incompatible dimensions in mat_asym_sparse::operator*(mat_full<TN> A) const");
#endif
    long i;
    
    for(i=0; i< A.cols(); i++) multiply(A[i],r[i]);
    return r;
  }

  //! Multiply by a full matrix.\test
  template <class TN> mat_full<TN> mat_asym_sparse<TN>::operator*(mat_full<TN> A) const
  {
    mat_full<TN> r(A.cols(),_rows);
    return multiply(A,r);
  }
#endif // SPARSE_SPARSE_FULL
  
#ifdef SPARSE_SPARSE_FULL
  //! Multiply a symmetric sparse matrix with a full vector into a sparse vector./test
  template <class TN> sparse_vector<TN> mat_asym_sparse<TN>::operator*(const refvector<TN> v) const
  {
#ifdef DEBUG
    if(v->size() != _cols)
      throw domain_error("Incompatible dimensions in sparse_vector mat_asym_sparse::operator*(refvector<TN> a");
    try {
#endif
      
      sparse_vector<TN> r(_rows);
      sparse_vector<TN> a;
      TN b;
      // Upper triangle.
      for(long i=0; i<_svals->size(); i++)
	r+=_svals[i]*v[_scols[i]];
      
      // Lower triangle
      
      for(i=0;i<_scols->size();i++)
	{
	  a.copy(_svals[i]);
	  a(_scols[i])=(double) 0;
	  b=a*v;
	  if(b>_prune_tol || b<-_prune_tol) 
	    {
	      r(_scols[i])-=b;
	    }
	}
      return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by sparse_vector<TN> mat_asym_sparse::operator*(const refvector<TN>& v) const");
    }
#endif
  }
#else
  //! Multiply a symmetric sparse matrix with a full vector into a full vector./test
  template <class TN> refvector<TN>& mat_asym_sparse<TN>::multiply(const refvector<TN>& v,refvector<TN>& r) const
  {
#ifdef DEBUG
    if(v.size() != _cols)
      throw domain_error("Incompatible dimensions in refvector mat_asym_sparse::operator*(refvector<TN> a");
    if(r.dim() != _cols)
      throw domain_error("Incompatible dimensions for output in refvector mat_asym_sparse::operator*(refvector<TN> a");
    try {
#endif
      sparse_vector<TN> a;
      TN b;
      long i,j;
      for(i=0;i<r.dim();i++) r[i]=(TN) 0;
      
      // Upper triangle.
      for(i=0; i<_svals.size(); i++)
	for(j=0;j<_svals[i].size();j++)
	  r[_svals[i]._index[j]]+=_svals[i][j]*v[_scols[i]];
      
      // Lower triangle
      
      for(i=0;i<_scols.size();i++)
	{
	  a.copy(_svals[i]);
	  a(_scols[i])=(TN) 0;
	  b=a*v;
	  r[_scols[i]]-=b;
	}
      return r;
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by refvector<TN> mat_asym_sparse::operator*(const refvector<TN>& v) const");
    }
#endif
  }
  
  //! Multiply an antisymmetric sparse matrix with a full vector into a full vector./test
  template <class TN> refvector<TN> mat_asym_sparse<TN>::operator*(const refvector<TN>& v) const
  {
    refvector<TN> r(_rows);
    return multiply(v,r);
  }
#endif //SPARSE_SPARSE_FULL
  
  //! Multiply an antisymmetric sparse matrix with a full vector into a full vector./test
  template <class TN> mat_full<TN>& mat_asym_sparse<TN>::multiply(const mat_asym_full<TN>& B, mat_full<TN>& r) const
  {
    long i,j,l;
    for(i=0;i<r.cols();i++)
      for(j=0;j<r.rows();j++)
	r[i][j]=(TN) 0;
    
    // ut ut
    for(l=0;l<(*this).size();l++)
      {
	long ld=_scols[l];
	for(i=ld;i<B.cols();i++)
	  {
	    for(j=0;j<_svals[l].size();j++)
	      r[i][_svals[l].index(j)]=B[i*(i+1)/2+ld]*_svals[l][j];
	  }
      }
    // ut lt
    for(l=0;l<(*this).size();l++)
      {
	long ld=_scols[l];
	for(i=0;i<ld;i++)
	  for(j=0;j<_svals[l].size();j++)
	    r[i][_svals[l].index(j)]-=B[ld*(ld+1)/2+i]*_svals[l][j];
      }
    // lt ut
    for(j=0;j<_svals.size();j++)
      {
	long jd=_scols[j];
	for(l=0;l<_svals[j].size();l++)
	  for(i=_svals[j].index(l)+1;i<B.cols();i++)
	    r[i][_svals[j].index(l)]-=B[i*(i+1)/2+_svals[j].index(l)]*_svals[j][l];
      }
    // lt lt
    for(j=0;j<_svals.size();j++)
      {
	long jd=_scols[j];
	for(l=0;l<_svals[j].size();l++)
	  {
	    long ld=_svals[j].index(l);
	    for(i=ld+1;i<B.cols();i++)
	      r[i][jd]+=B[ld*(ld+1)/2+i]*_svals[j][l];
	  }
      }

    return r;
  }

  //! Multiply a symmetric sparse matrix with a full vector into a full vector./test
  template <class TN> mat_full<TN> mat_asym_sparse<TN>::operator*(const mat_asym_full<TN>& B) const
  {
    mat_full<TN> r(B.cols(),_rows);
    return multiply(B,r);
  }

  
  //! return a symmetric full matrix=\f$ g\cdot g^\ast\f$.
  template <class TN> mat_sym_full<TN> mat_asym_sparse<TN>::aa_t() const
  {
    mat_sym_full<TN> r(_cols);
    long i,j,k;
    for(i=0;i<_cols;i++)
      for(j=0;j<=i;j++)
	for(k=0;k<_cols;k++)
	  r[i*(i+1)/2+j]+=(*this)(i,k)*(*this)(j,k);
    return r;
  }


  //! Multiply by an object of TN into left side.
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::operator*=(const TN& x)
  {
    long i=0;
    if( !_empty)
      while(i<_svals.size())
	{
	  _svals[i]*=x;
	  i++;
	}
    return *this;
  }
  //! Divide by an object of TN into left side. This is very slow and not advisable.
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::operator/=(const TN& x)
  {
    long i=0;
    if( !_empty)
      while(i<_svals.size())
	{
	  _svals[i]/=x;
	  i++;
	}
    return *this;
  }

  //! Multiply a sparse symmetric matrix with a sparse vector.
  template <class TN> sparse_vector<TN> mat_asym_sparse<TN>::operator*(const sparse_vector<TN>& v) const
  {
    try {
      sparse_vector<TN> r(_rows);
      sparse_vector<TN> a;
      TN b;
      // Upper triangle.
      
      vector<long> thisindex; // We will produce one vector of mutual nonzeros. O(n) instead of O(n^2).
      vector<long> aindex;
      long j=0;
      long i=0;
      while( j<v._index->size())
	{
	  while(i<_scols->size() && _scols[i]<v._index[j]) i++;
	  if(i<_scols->size() && _scols[i]==v._index[j])
	    {
	      thisindex.push_back(i);
	      aindex.push_back(j);
	    }
	  j++;
	};
      for(long i=0; i<thisindex.size(); i++)
	{
	  r+=_svals[thisindex[i]]*v[aindex[i]];
	}
      
      // Lower triangle
      
      for(i=0;i<_scols->size();i++)
	{
	  a.copy(_svals[i]);
	  a(_scols[i])=(double) 0;
	  b=a*v;
	  if(b>_prune_tol || b<-_prune_tol) 
	    {
	      r(_scols[i])-=b;
	    }
	}
      return r;
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by sparse_vector<TN> mat_asym_sparse::operator*(const sparse_vector<TN>& v) const");
    }
  }
  
  //! Multiply a sparse symmetric matrix with a sparse matrix.
  template <class TN> mat_sparse<TN> mat_asym_sparse<TN>::operator*(const mat_sparse<TN>& B) const
  {
#ifdef DEBUG
    if(_cols != B._rows)
      {
	throw domain_error( "Incompatible dimensions in mat_sparse<TN> mat_asym_sparse::operator*(const mat_sparse<TN>& B) const" );
	exit(1);
      }
#endif //DEBUG
    try {
      long i;
      
      refvector<sparse_vector<TN> > sv;
      
      for(i=0; i<B._colindex->size(); i++)
	sv->push_back((*this)*B._scols[i]);
      mat_sparse<TN> r(B._cols,_rows,B._colindex.vec(),sv);
	
      r.prune();
      return r;
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by mat_sparse<TN> mat_asym_sparse::operator*(const mat_sparse<TN>& B) const");
    }
  }
  
  //! Add a matrix into left object.
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::operator+=(const mat_asym_sparse& B)
  {
#ifdef DEBUG
    if(_cols!=B._cols)
      throw domain_error( " Incompatible dimensions in mat_asym_sparse& operator+=(mat_asym_sparse& B)" );
#endif
  
    long ivals=0;
    long icols=0;
    long bvals=0;
    long bcols=0;
    while(ivals<_svals->size())
      {
	while(bcols <B._scols->size() && 
#ifdef DEBUG
	      icols <_scols->size()   &&
#endif
	      B._scols[bcols]<_scols[icols])
	  {
	    _scols->insert(_scols->begin()+icols,B._scols[bcols]);
	    icols++;
	    bcols++;
	    _svals->insert(_svals->begin()+ivals,B._svals[bvals]);
	    ivals++;
	    bvals++;
	  }
	if(bcols<B._scols->size() &&
#ifdef DEBUG
	   icols<_scols->size()   &&
#endif
	   B._scols[bcols]==_scols[icols])
	  {
	    _svals[ivals]+=B._svals[bvals];
	    bvals++;
	    bcols++;
	  }
	ivals++;
	icols++;
      }
    while(bcols<B._scols->size())
      {
	_svals->push_back(B._svals[bvals]);
	bvals++;
	_scols->push_back(B._scols[bcols]);
	bcols++;
      }
    prune();
    return *this;
  }
  
  //! Subtract a matrix into left object.
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::operator-=(const mat_asym_sparse& B)
  {
#ifdef DEBUG
    if(_cols!=B._cols)
      throw domain_error( " Incompatible dimensions in mat_asym_sparse& operator-=(mat_asym_sparse& B)" );
#endif
   
    long ivals=0;
    long icols=0;
    long bvals=0;
    long bcols=0; 
    while(ivals<_svals->size())
      {
	while(bcols <B._scols->size() &&
#ifdef DEBUG
	      icols <_scols->size()   &&
#endif
	      B._scols[bcols]<_scols[icols])
	  {
	    typename vector<long>::iterator icit=_scols->begin();
	    icit+=icols;
	    _scols->insert(icit,B._scols[bcols]);
	    icols++;
	    bcols++;
	    typename vector<sparse_vector<TN> >::iterator ivit=_svals->begin();
	    ivit+=ivals;
	    _svals->insert(ivit,-(B._svals[bvals]));
	    ivals++;
	    bvals++;
	  }
	if(bcols<B._scols->size() &&
#ifdef DEBUG
	   icols<_scols->size()   &&
#endif
	   B._scols[bcols]==_scols[icols])
	  {
	    _svals[ivals]-=B._svals[bvals];
	    bvals++;
	    bcols++;
	  }
	ivals++;
	icols++;
      }
    while(bcols<B._scols->size())
      {
	_svals->push_back(-(B._svals[bvals]));
	bvals++;
	_scols->push_back(B._scols[bcols]);
	bcols++;
      }
    prune();
    return *this;
  }
  
  // Normal member functions ***************************************************************************************

  template <class TN> bool mat_asym_sparse<TN>::is_col_nonzero(const long x, long &i) const
  {
    i=0;
    while(i<_scols.size() && _scols[i] <x) i++;
    if(i<_scols.size() && _scols[i]== x) return true;
    else return false;
  }
  
  //! Checks whether an access is non zero.
  template <class TN> bool mat_asym_sparse<TN>::is_nonzero(long x, long y, long &j, long &k) const
    /*!
      \param j holds the position of (x,y) in _vals. If (x,y) is 0. An insertion would happen here.
    */
  {
    j=0; k=0;
    if( _empty) return false;
    if ( y>x) { long interim=y; y=x; x=interim;}
    if(is_col_nonzero(x,j))
      return(_svals[j].is_nonzero(y,k));
    else return false;
  }
  
  //! Adds a value.
  template <class TN> bool mat_asym_sparse<TN>::add(long x, long y, const TN z)
    /*!
      \param z is added to (x,y).
      If the value is zero the associated vectors are expanded.
    */
  {
#ifdef DEBUG
    try {
#endif DEBUG
      long i;
      TN sign=(TN) 1;
      if(x<y) { i=x; x=y; y=i; sign=(TN) -1;}
      if (x==y) throw domain_error("mat_asym_sparse::add : the diagonal has no entries");
      if(x<_cols)
	{
	  if(is_col_nonzero(x,i)) 
	    {
	      _svals[i](y)+=z*sign;
	    }
	  else
	    {
	      typename vector<long>::iterator s=_scols->begin();
	      s+=i;
	      _scols->insert(s,x);
	      typename vector<sparse_vector<TN> >::iterator s2=_svals->begin();
	      s2+=i;
	      sparse_vector<TN> v(_rows);
	      v(y)=z*sign;
	      s2=_svals->insert(s2,v);
	      _size++;
	    }
	  _empty=false;
	  return true;
	}
#ifdef DEBUG
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by  mat_asym_sparse::add ");
    }
    throw domain_error( " Outside of dimension of mat_asym_sparse::add " );
#endif
  }

  template <class TN> bool mat_asym_sparse<TN>::is_empty() const { return _empty;};
  //! Returns size of non-zero entries in _svals.
  template <class TN> long mat_asym_sparse<TN>::size() const { return _svals->size();}
  
  //! Output matrix in binary format into a stream. Aimed at file storage.
  template <class TN> void mat_asym_sparse<TN>::operator>>(ostream& OUT) const
  {
    long i=_scols->size();
    
    OUT << "Antisymmetric sparse matrix:\n";
    OUT.write((char*) &_rows,sizeof(long));
    OUT.write((char*) &i, sizeof(long));
    vector<long>::const_iterator cc=_scols->begin();
    OUT.write((char*) &(*cc), sizeof(long)*i);
    typename vector<sparse_vector<TN> >::const_iterator cv=_svals->begin();
    
    while(cv!=_svals->end())
      {
	i=cv->_index->size();
	OUT.write((char*) &i, sizeof(long));
	OUT.write((char*) &(*cv->_index->begin()), sizeof(long)*i);
	OUT.write((char*) &(*cv->_vals->begin()), sizeof(TN)*i);
	cv++;
      }
    OUT << endl;
  }
  
  //! Deletes all entries that are smaller in magnitude than x.
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::prune(const TN x)
  {
    long iv=0;
    long ic=0;
    while(iv<_svals->size())
      {
	_svals[iv].prune(x);
	if(_svals[iv]._index->size()==0) 
	  {
	    _svals->erase(_svals->begin()+iv);
	    _scols->erase(_scols->begin()+ic);
	  }
	else
	  {
	    iv++;
	    ic++;
	  }
      }
    _size=_svals->size();
    if(_size == 0) _empty=true;
    return (*this);
  }
  
  //! Finds a maximum element.
  template <class TN> TN mat_asym_sparse<TN>::max_element() const
  {
    if(!_empty)
      {
	long i=0;
	if(_svals->size() ==0) return 0;
	TN max=_svals[i].max();i++;
	TN m;
	while(i<_svals->size())
	  {
	    m=_svals[i].max();
	    if(max<m) max=m;
	    i++;
	  }
	return max;
      }
    return 0;
  }
  
  //! Extracts a block from a column/row
  template <class TN> vector<TN> mat_asym_sparse<TN>::extract_full_col(const long column, const long xmin, const long xmax) const
  {
#ifdef DEBUG
    if(xmin>xmax) {
      throw domain_error( "xmin > xmax in mat_asym_sparse::extract_col(const long column, const long xmin, const long xmax) const" );
      exit(1);
    }
    if(xmin<0 || xmax>_rows) 
      {
	throw domain_error("Matrix dimensions exceeded in mat_asym_sparse::extract_col(const long column, const long xmin, const long xmax) const" );
	exit(1);
      }
#endif
    try {
      vector<TN> r(xmax-xmin);
      long i,j;
      if(is_col_nonzero(column,j))
	for(i=xmin; i<xmax; i++) r[i-xmin]=_svals[j](i);
      return r;
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by  vector<TN> mat_asym_sparse::extract_full_col(const long column, const long xmin, const long xmax) const");
    }
  }
  
  //! Extracts a block from a column/row
  template <class TN> vector<TN> mat_asym_sparse<TN>::extract_full_col(const long column, const long xmin) const
  {
    return extract_full_col(column, xmin,_rows);
  }
  
  //! Extracts a full symmetric block.
  template <class TN> mat_asym_full<TN> mat_asym_sparse<TN>::extract_full_asym_block(long xmin, long xmax) const
  {
    if(xmin>xmax) {
      throw domain_error( "xmin > xmax in mat_asym_sparse::extract_full_sym_block(const long column, const long xmin, const long xmax) const" );
      exit(1);
    }
    if(xmin<0 || xmax>_rows) 
      {
	throw domain_error("Matrix dimensions exceeded in mat_asym_sparse::extract_full_sym_block(const long column, const long xmin, const long xmax) const" );
	exit(1);
      }
    try {
      mat_asym_full<TN> r(xmax-xmin);
      long i,j;
      is_col_nonzero(xmin,i);
      while(i<_scols->size() && _scols[i]<xmax)
	{
	  _svals[i].is_nonzero(xmin,j);
	  while(j<_svals[i]._index->size() && _svals[i]._index[j]<xmax)
	    {
	      r(_scols[i]-xmin,_svals[i]._index[j]-xmin)=_svals[i][j];
	      j++;
	    }
	  i++;
	}
      return r;
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by mat_asym_full<TN> mat_asym_sparse::extract_full_asym_block(long xmin, long xmax) const");
    }
  }
  
  //! Extracts a full symmetric block.
  template <class TN> mat_asym_full<TN> mat_asym_sparse<TN>::extract_full_asym_block(long xmin) const
  {
    return extract_full_sym_block(xmin,_rows);
  }
  
  //! Extracts a block from anywhere in the matrix. \test should work, but ...
  template <class TN> mat_sparse<TN> mat_asym_sparse<TN>::extract_sparse_block(long xmin, long xmax, long ymin, long ymax) const
  {
    if(xmin > xmax || ymin > ymax ) {
      throw domain_error( "xmin > xmax || ymin > ymax in mat_asym_sparse::extract_sparse_block (const long column, const long xmin, const long xmax) const" );
      exit(1);
    }
    if(xmin<0 || xmax>_cols || ymin<0 || ymax >_rows)
      {
	throw domain_error("Matrix dimensions exceeded in mat_asym_sparse::extract_sparse_block(const long column, const long xmin, const long xmax) const" );
      }
    try {
      refvector<sparse_vector<TN> > sv;
      refvector<long> si;
      long i,j;
      j=0;
      for(i=0; i<_scols->size() && _scols[i]<xmin; i++);
      while(i<_scols->size() && _scols[i]<xmax)
	{
	  sparse_vector<TN> r(ymax-ymin);
	  if(_svals[i].is_nonzero(ymin,j))
	    while( j<_svals[i]._index->size() &&
		   _svals[i]._index[j]<ymax)
	      {
		r._index->push_back(_svals[i]._index[j]-ymin);
		r._vals->push_back(_svals[i][j]);
		j++;
	      }
	  sv->push_back(r);
	  si->push_back(_scols[i]-xmin);
	  i++;
	}
      mat_sparse<TN> r1(xmax-xmin, ymax-ymin,si, sv);
      return r1;
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called  by  mat_sparse<TN> mat_asym_sparse::extract_sparse_block(long xmin, long xmax, long ymin, long ymax) const");
    }
  }
  
  //! Extracts a block from a column/row
  template <class TN> sparse_vector<TN> mat_asym_sparse<TN>::extract_sparse_col(const long column, const long xmin) const
  {
    return extract_sparse_col(column, xmin,_rows);
  }
  
  //! Extracts a sparse column up to the diagonal.
  template <class TN> sparse_vector<TN> mat_asym_sparse<TN>::extract_sparse_lower_col(const long column, const long xmin, const long xmax) const
  {
    if(xmin>xmax) {
      throw domain_error( "xmin > xmax in mat_asym_sparse::extract_col(const long column, const long xmin, const long xmax) const" );
      exit(1);
    }
    if(xmin<0 || xmax>_rows) 
      {
	throw domain_error("Matrix dimensions exceeded in mat_asym_sparse::extract_col(const long column, const long xmin, const long xmax) const" );
	exit(1);
      }
    try {
      long i;
      if(is_col_nonzero(column,i))
	{
	  if(xmin ==0 && xmax == _rows) return _svals[i];
	  sparse_vector<TN> r;
	  long j;
	  _svals[i].is_nonzero(xmin,j);
	  while( j<_svals[i]._index->size() &&
		 _svals[i]._index[j]<xmax)
	    {
	      r._index->push_back(_svals[i]._index[j]-xmin);
	      r._vals->push_back(_svals[i][j]);
	      j++;
	    }
	  return r;
	}
      else
	{
	  sparse_vector<TN> r(xmax-xmin);
	  return r;
	}
    } catch(domain_error e) {
      cerr << e.what() << endl;
      throw domain_error(" called by sparse_vector<TN> extract_sparse_lower_col(const long column, const long xmin, const long xmax) const");
    }
  }
  
  //! Extracts a block from a column/row
  template <class TN> sparse_vector<TN> mat_asym_sparse<TN>::extract_sparse_lower_col(const long column, const long xmin) const
  {
    return extract_sparse_lower_col(column, xmin,_rows);
  }
  
  //! Return the density of non-zero elements.
  template <class TN> double mat_asym_sparse<TN>::density() const 
  {
    long i,j;
    j=0;
    for(i=0; i< _svals->size(); i++)
      j+=_svals[i]._vals->size();
    return (double) j/(double) (_rows*(_rows-1)/2);
  }
  
  //#include "mat_asym_sparse_eigen.h"
  
  //! Sets matrix to zero releasing all associated memory.
  template <class TN> void mat_asym_sparse<TN>::clear()
  {
    _rows=_cols=0;
    _size=0;
    _empty=true;
    refvector<long> s;
    refvector<sparse_vector<TN> > v;
    _scols=s;
    _svals=v;
  }

  //! Sets matrix to zero releasing all associated memory.
  template <class TN> mat_asym_sparse<TN>& mat_asym_sparse<TN>::zero()
  {
    _size=0;
    _empty=true;
    refvector<long> s;
    refvector<sparse_vector<TN> > v;
    _scols=s;
    _svals=v;
    return *this;
  }
  
  //! Returns the ith nonzero column number.
  template <class TN> long mat_asym_sparse<TN>::index(long i) const
  {
#ifdef DEBUG
    if(i<0 || i>= _scols.size())
      throw domain_error("Incompatble dimensions in mat_asym_full::index(long i) const");
#endif
    return _scols[i];
  }
  
  //! This is the unitary transform \f$U^\ast AU\f$.
  template <class TN> mat_asym_full<TN>& mat_asym_sparse<TN>::utu(const mat_full<TN>& U, mat_asym_full<TN>& R) const
  {
#ifdef DEBUG
    if(_cols!= U.rows() || _rows != U.rows() || _cols!=R.cols())
      throw domain_error("Incompatble dimensions in mat_asym_sparse::utu(const mat_full<TN>& U, mat_asym_full<TN>& R) const");
#endif
    const long n=U.cols();
    
    /* This is also order n^3*/
    long i,j;
    refvector<TN> r(_rows);
    for(i=0;i<n;i++)
      {
	multiply(U[i],r);
	for(j=0;j<=i;j++) R[i*(i+1)/2+j]=U[j]*r;
      }
    return R;
  }
  
  //! This is the unitary transform U^tAU.
  template <class TN> mat_asym_full<TN> mat_asym_sparse<TN>::utu(const mat_full<TN>& U) const
  {
    mat_asym_full<TN> R(_cols);
    utu(U,R);
    return R;
  }
  
  template<class TN> bool mat_asym_sparse<TN>::display() const
  {
    if (_empty )
      {
	cout << "empty" << endl;
	return true;
      }
    long col=0;
    long val=0;
    long vn;
    long in;
    while(val < _svals.size())
      {
	vn=0;
	in=0;
	while(in<_svals[val]._index.size())
	  cout << " (" << _scols[col] << "," << _svals[val]._index[in++] <<") = " << _svals[val]._vals[vn++];
	cout << endl;
	val++;
	col++;
      }
    return true;
  }
} // linear_algebra::  

#include "refcount.h"
#include "sparse_vector.h"
#include "mat_full.h"
#include "mat_sparse.h"
#include "mat_sym_full.h"
#include "mat_asym_full.h"
#endif
/*! @}*/
